
#include <gtest/gtest.h>
#include <downow/search/KickassTorrentsScraper.hpp>
#include <downow/search/YifySource.hpp>
#include <QtCore/QFile>
#include <downow/common/Utils.hpp>

using namespace downow;

struct Kickass : ::testing::Test {
    void SetUp() override {
        QFile binaryData(DOWNOW_TEST_KICKASS_SEARCH_DATA_PATH);
        binaryData.open(QIODevice::ReadOnly);
        html = binaryData.readAll();
    }

    QByteArray html;
};

TEST_F(Kickass, scrape) {
    using namespace search::kickass::detail;

    boost::optional<std::vector<KickassTorrent>> torrents = scrape(html);

    ASSERT_TRUE(torrents.is_initialized());
    ASSERT_EQ(torrents->size(), 25);

    for (KickassTorrent const& t : torrents.get()) {
        ASSERT_GT(t.name.size(), 0);
        ASSERT_GT(t.magnetUrl.size(), 0);
        ASSERT_GT(t.size.value, 0);
        ASSERT_NE(t.size.unit, ByteUnit::Unknown);
        ASSERT_GT(t.seeders, 0);
    }
}
