
project(downow C CXX)

cmake_minimum_required(VERSION 2.8.9)

set(UBUNTU_MANIFEST_PATH "manifest.json.in" CACHE INTERNAL "Tells QtCreator location and name of the manifest file")
#set (UBUNTU_PROJECT_TYPE "ClickApp" CACHE INTERNAL "Tells QtCreator this is a Click application project")
set (CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

include(CTest)
enable_testing()

set(APP_NAME DowNow)
set(DOWNOW_DESCRIPTION "Torrent downloading app")
set(DOWNOW_REVERSE_DOMAIN "com.nogzatalz.downow")
set(DOWNOW_CLICK_PACKAGE_NAME ${DOWNOW_REVERSE_DOMAIN})

set(DOWNOW_SEARCH_KICKASS_TORRENTS_HOST "kat.cr")
set(DOWNOW_SEARCH_YIFY_TORRENTS_HOST "yts.to")

set(Boost_USE_SYSTEM ON)

set(CMAKE_CXX_FLAGS -Wall)
set(CMAKE_CXX_FLAGS_RELEASE "-Os -DNDEBUG=true")
set(CMAKE_CXX_FLAGS_DEBUG "-g -DDEBUG=true")

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

# make relevant files visible in QtCreator
file(GLOB_RECURSE CMAKE_FILES *.cmake)
add_custom_target(${PROJECT_NAME}-extra-files SOURCES apparmor.json downow.desktop.in manifest.json.in urldispatcher.json icon.png icon-square.svg ${CMAKE_FILES})

# get target information
execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
    OUTPUT_VARIABLE DOWNOW_TARGET_TRIPLET
    OUTPUT_STRIP_TRAILING_WHITESPACE)

execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_ARCH
    OUTPUT_VARIABLE DOWNOW_ARCHITECTURE
    OUTPUT_STRIP_TRAILING_WHITESPACE)

set(DOWNOW_VERSION_MAJOR 0)
set(DOWNOW_VERSION_MINOR 7)

if(NOT DOWNOW_VERSION_REVISION)
    # get the bazaar revision number
    execute_process(
        COMMAND bzr revno
        OUTPUT_VARIABLE DOWNOW_VERSION_REVISION
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

#if(DOWNOW_VERSION_REVISION)
#    message(STATUS "Revision: ${DOWNOW_VERSION_REVISION}")
#else()
#    message(SEND_ERROR "Couldn't get bazaar revision number. Bazaar must be 
#installed on the building machine.")
#endif()

#if(NOT TARGET_MODE)
#    set(TARGET_MODE "Click")
#endif()

#if(TARGET_MODE STREQUAL "Click")
#    include(Click)
#elseif(TARGET_MODE STREQUAL "Desktop")
#    include(Desktop)
#else()
#    message(FATAL_ERROR "No such target mode: ${TARGET_MODE}")
#endif()

configure_file(downow.desktop.in ${CMAKE_CURRENT_BINARY_DIR}/downow.desktop)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
#add_subdirectory(third-party)

#link_directories(${downow-third-party-libtorrent_LIB_DIRS} 
#${downow-third-party-boost_LIB_DIRS})

add_subdirectory(downow)
add_subdirectory(test)

# install desktop file
install(
    FILES ${CMAKE_CURRENT_BINARY_DIR}/downow.desktop
    DESTINATION ${DESKTOP_INSTALL_DIR})
