[![pipeline status](https://gitlab.com/StefWe/downow/badges/master/pipeline.svg)](https://gitlab.com/StefWe/downow/commits/master)

<img width="200px" src="assets/icon.png" />

DowNow
=============

This is a fork of the former [DowNow](https://launchpad.net/downow) app made by Tal Zion and Noga Dikerman.


Is a torrent downloading app for Ubuntu Touch devices.


## Building the app

### Using clickable
To build and run the app on the desktop run:

```
clickable desktop
```
To build and install a click package on the device run:

```
clickable
```

See [clickable documentation](http://clickable.bhdouglass.com/en/latest/) for details.

## License

Copyright (C) 2019 Stefan Weng  
Copyright (C) 2014-2018 Tal Zion and Noga Dikerman

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.