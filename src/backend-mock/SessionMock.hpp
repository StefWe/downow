#ifndef DOWNOW_BACKEND_MOCK_SESSION_MOCK
#define DOWNOW_BACKEND_MOCK_SESSION_MOCK

#include "../torrent-frontend/Session.hpp"

namespace downow { namespace backend_mock {

struct TorrentMock;

struct SessionMock : torrent_frontend::Session {
private:
    Q_OBJECT

public:
    using base_type = torrent_frontend::Session;
    SessionMock(QObject * parent = nullptr);

    void addTorrent(TorrentMock *);
    void addTorrentByMagnetLink(QUrl magnetLink) override;
    void addTorrentByFile(QString torrentFilePath) override;

    void removeTorrent(torrent_frontend::Torrent * pTorrent) override;
    void removeTorrentAndDeleteFiles(torrent_frontend::Torrent *) override;

    TorrentRandomRange
    getTorrents() const override;

    int getMaxDownloadSpeedBPS() const override {
        return -1;
    }
    int getMaxUploadSpeedBPS() const override {
        return 100;
    }
    void setMaxDownloadSpeedBPS(int) override {}
    void setMaxUploadSpeedBPS(int) override {}

    QDir getTorrentsSaveDirectory() const override;
    int getDownloadSpeedBPS() const override {
        return 100;
    }
    int getUploadSpeedBPS() const override {
        return 50;
    }
    void pause() override {}
    void resume() override {}
    bool isPaused() const override {
        return false;
    }

private:
    QVector<TorrentMock *> _torrents;
};

} }

#endif
