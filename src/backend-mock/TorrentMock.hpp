#ifndef DOWNOW_BACKEND_MOCK_TORRENT_MOCK
#define DOWNOW_BACKEND_MOCK_TORRENT_MOCK

#include "../torrent-frontend/Torrent.hpp"

namespace downow { namespace backend_mock {

struct SessionMock;

struct TorrentMock : torrent_frontend::Torrent {
private:
    Q_OBJECT

public:
    using base_type = torrent_frontend::Torrent;
    TorrentMock(SessionMock * sessionMock = nullptr, QString name = "", int totalBytes = 0, int downSpeed = 1234847, QObject * parent = nullptr);

    void timerEvent(QTimerEvent *) override;

    QString getName() const override {
        return _name;
    }

    void resume() override {
        setState(State::Downloading);
    }
    void pause() override {
        setState(State::Paused);
    }

    Bytes getDownloadedBytes() const override {
        return _downloadedBytes;
    }

    Bytes getUploadedBytes() const override {
        return _uploadedBytes;
    }

    Bytes getTotalBytes() const override {
        return _totalBytes;
    }

    int getDownloadLimitBPS() const override {
        return _downloadLimitBPS;
    }

    void setDownloadLimitBPS(int downloadLimit) override {
        if (_downloadLimitBPS == downloadLimit) {
            return;
        }

        _downloadLimitBPS = downloadLimit;
        downloadLimitBPSChanged();
    }

    int getUploadLimitBPS() const override {
        return _uploadLimitBPS;
    }

    void setUploadLimitBPS(int uploadLimit) override {
        if (_uploadLimitBPS == uploadLimit) {
            return;
        }

        _uploadLimitBPS = uploadLimit;
        uploadLimitBPSChanged();
    }

    int getDownloadSpeedBPS() const override {
        return _downloadSpeed;
    }

    int getUploadSpeedBPS() const override {
        return _uploadSpeed;
    }

    Priority getPriority() const override {
        return _priority;
    }

    void setPriority(Priority priorty) override {
        if (_priority == priorty) {
            return;
        }

        _priority = priorty;
        priorityChanged();
    }

    QDateTime getDateAdded() const override {
        return _dateAdded;
    }

    State getState() const override {
        return _state;
    }

    void setState(State state);

    QString getSavePath() const override {
        return ".";
    }

    torrent_frontend::Session * getSession() const override;

    FileEntriesRandomAccessRange getFileEntries() const {
        return _fileEntries;
    }

private:
    QString _name;
    int _downloadedBytes = 0;
    int _uploadedBytes = 0;
    int _totalBytes;
    int _downloadLimitBPS;
    int _uploadLimitBPS;
    int _downloadSpeed;
    int _uploadSpeed = 100;
    Priority _priority;
    State _state;
    int _time = 0;
    QDateTime _dateAdded;

    std::vector<torrent_frontend::FileEntry *> _fileEntries;

    SessionMock * _pSession;
};

} }

#endif
