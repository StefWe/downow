#include "SessionMock.hpp"

#include "TorrentMock.hpp"
#include <boost/range/adaptor/transformed.hpp>
#include <boost/log/trivial.hpp>
#include "../common/Utils.hpp"
#include "../config.hpp.in"

namespace downow { namespace backend_mock {

SessionMock::SessionMock(QObject * parent) :
    base_type(parent) {
    addTorrent(new TorrentMock(this, "How to train your dragon 1080p", 12000000, 1231230, this));
//    addTorrent(new TorrentMock(this, "Game of thrones", 10000000, 12300, this));
}

void
SessionMock::addTorrent(TorrentMock * torrent) {
    willAddTorrent(torrent);
    _torrents.append(torrent);
    torrentAdded(torrent);
}

void
SessionMock::addTorrentByFile(QString torrentFilePath) {
    BOOST_LOG_TRIVIAL(debug) << "SessionMock: addTorrentByFile: " << torrentFilePath;
}

void
SessionMock::addTorrentByMagnetLink(QUrl magnetLink) {
    BOOST_LOG_TRIVIAL(debug) << "SessionMock: addTorrentByMagnetLink: " << magnetLink.toString();
}

void SessionMock::removeTorrent(torrent_frontend::Torrent * pTorrent) {
    BOOST_LOG_TRIVIAL(debug) << "Remove torrent: " << pTorrent->getName();
}

void SessionMock::removeTorrentAndDeleteFiles(torrent_frontend::Torrent * pTorrent) {
    BOOST_LOG_TRIVIAL(debug) << "Remove torrent and delete files: " << pTorrent->getName();
}

static downow::torrent_frontend::Torrent * torrentUpcast(TorrentMock * pMock) {
    return static_cast<downow::torrent_frontend::Torrent *>(pMock);
}

torrent_frontend::Session::TorrentRandomRange
SessionMock::getTorrents() const {
    return _torrents | boost::adaptors::transformed(torrentUpcast);

    // for some weird reason transformed with a lambda doesn't seem to work under GCC
#if 0
    [] (TorrentMock * pMock) {
        return static_cast<Torrent *>(pMock);
    });
#endif
}

QDir SessionMock::getTorrentsSaveDirectory() const {
    return QDir("/home/user/.local/share/" DOWNOW_REVERSE_DOMAIN "/download");
}

} }
