#include "TorrentMock.hpp"
#include "SessionMock.hpp"

namespace downow { namespace backend_mock {

TorrentMock::TorrentMock(SessionMock * session, QString name, int totalBytes, int downSpeed, QObject *parent) :
    base_type(session, parent),
    _name(name),
    _totalBytes(totalBytes),
    _downloadSpeed(downSpeed),
    _state(State::Preparing),
    _dateAdded(QDateTime::currentDateTime()),
    _pSession(session) {
    startTimer(1000);
}

void TorrentMock::timerEvent(QTimerEvent *) {
    _time++;
    if (_time < 2) {

    }
    else if (_time == 2) {
        setState(State::Preparing);
    }
    else if (_time >= 3) {
        if (getState() == State::Paused) {
            return;
        }

        _downloadedBytes = std::min(_downloadedBytes + _downloadSpeed, _totalBytes);
        _uploadedBytes += _uploadSpeed;

        downloadedBytesChanged();
        uploadedBytesChanged();

        if (getDownloadedBytes() < getTotalBytes()) {
            setState(State::Downloading);
        }
        else {
            setState(State::Seeding);
        }
    }
}

void TorrentMock::setState(Torrent::State state) {
    if (_state == state) {
        return;
    }

    State old = _state;
    _state = state;
    stateChanged(old, _state);

    if (_state == State::Seeding) {
        _downloadSpeed = 0;
        downloadSpeedBPSChanged();
    }
}

torrent_frontend::Session *TorrentMock::getSession() const {
    return _pSession;
}

} }
