#ifndef DOWNOW_MODEL_UTILS
#define DOWNOW_MODEL_UTILS

#include <QString>
#include <QSharedPointer>
#include <QObject>
#include <QFuture>
#include <functional>
#include <ostream>
#include <memory>
#include <chrono>
#include <boost/exception/all.hpp>
#include <boost/optional.hpp>

std::ostream & operator<< (std::ostream &, QString const&);

namespace downow {

template <typename T, typename... Arg>
std::shared_ptr<T> shared_qobject(Arg... arg) {
    return {
        new T(std::forward<Arg>(arg)...),
        [] (T * t) {
            t->deleteLater();
        }
    };
}

struct QObjectDeleter {
    void operator() (QObject * t) const {
        t->deleteLater();
    }
};

template <typename T>
using unique_qobject_ptr = std::unique_ptr<T, QObjectDeleter>;

template <typename T, typename... Arg>
unique_qobject_ptr<T> make_unique_qobject(Arg... arg) {
    return unique_qobject_ptr<T> (
        new T(std::forward<Arg>(arg)...)
    );
}

template <typename Func>
auto
on_scope_exit(Func func) {
    struct scope_t {
        scope_t(Func func) : _func(std::move(func)) {}
        scope_t(scope_t const& other) = delete;
        scope_t(scope_t && other) : _func(std::move(other._func)) {
            other._func.reset();
        }

        ~scope_t() {
            if (_func) {
                _func.get()();
            }
        }

    private:
        boost::optional<Func> _func;
    };

    return scope_t { std::move(func) };
}

template <typename T, typename Func>
void onFutureFinished(QFuture<T> future, QObject * object, Func func) {
    auto futureWatcher = new QFutureWatcher<T>();
    QObject::connect(futureWatcher, &QFutureWatcher<T>::finished, object, [
            func,
            futureWatcher
        ]() mutable {
        auto s = on_scope_exit([&futureWatcher]() mutable {
            futureWatcher->deleteLater();
        });
        func(futureWatcher->future());
    });
    futureWatcher->setFuture(future);
}

template <typename F>
std::chrono::duration<float> measureSec(F && f) {
    namespace chrono = std::chrono;
    using Clock = chrono::steady_clock;
    auto startTime = Clock::now();
    f();
    auto endTime = Clock::now();
    return chrono::duration_cast<chrono::duration<float>>(endTime - startTime);
}

struct InstanceAlreadyExistsException : virtual boost::exception, virtual std::exception {};
struct InstanceDoesntExistException : virtual boost::exception, virtual std::exception {};

}

#endif
