#include "FileEntry.hpp"
#include "Torrent.hpp"
#include <qt5/QtCore/QDebug>

namespace downow { namespace backend_libtorrent {

FileEntry::FileEntry(Torrent * torrent, QFileInfo fileInfo, Bytes totalSizeBytes, QObject * parent) :
    base_type(torrent, parent),
    _fileInfo(std::move(fileInfo)),
    _totalBytes(totalSizeBytes),
    _torrent(torrent) {}

downow::torrent_frontend::Torrent *
FileEntry::getTorrent() const {
    return _torrent.data();
}

void
FileEntry::setDownloadedBytes(Bytes downloadedBytes) {
    if (getDownloadedBytes() == downloadedBytes) {
        return;
    }

    _downloadedBytes = downloadedBytes;
    Q_EMIT downloadedBytesChanged();
}

void
FileEntry::setTotalBytes(Bytes totalBytes) {
    if (getTotalBytes() == totalBytes) {
        return;
    }

    _totalBytes = totalBytes;
    Q_EMIT totalBytesChanged();
}

} }
