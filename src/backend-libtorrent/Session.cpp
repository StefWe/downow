#include "Session.hpp"
#include "Torrent.hpp"

#include <libtorrent/session.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/create_torrent.hpp>

#include "Event.hpp"

#include <boost/log/trivial.hpp>

#include <qt5/QtCore/QCoreApplication>
#include <qt5/QtCore/QTimer>
#include <qt5/QtConcurrent/QtConcurrent>

#include <boost/range/adaptor/transformed.hpp>
#include <boost/format.hpp>

#include <libtorrent/bencode.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>

#include <boost/range/adaptor/filtered.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/shared_ptr.hpp>
#include <chrono>

#include <boost/log/trivial.hpp>

namespace downow { namespace backend_libtorrent {

namespace chrono = std::chrono;
namespace fs = boost::filesystem;

static chrono::seconds constexpr timeToSaveFastResumeData { 4 };
using Clock = chrono::steady_clock;

Session::Session(
        fs::path downloadDir,
        fs::path dataDir,
        boost::optional<int> lastRevision,
        QObject *parent) :
    base_type(parent),
    _downloadDir(std::move(downloadDir)),
    _dataDir(dataDir) {

    auto startTime = Clock::now();

    fs::create_directories(_downloadDir);
    fs::create_directories(_dataDir);

    // Before revision 54 .torrent and .fast-resume were in the download dir.
    // They have since been moved to the data directory.
    // Make sure they're in the data dir.
    // Also, last revision launched was not tracked before revision 55 because of a bug so
    // if the last revision doesn't exist, we don't know if it's because this is
    // the first launch or the last launched revision was <= 54
    if (!lastRevision || lastRevision.get() <= 54) {
        BOOST_LOG_TRIVIAL(debug)
                << "libtorrent: Upgrading from revision <= 54: "
                << "Moving .torrent and .fast-resume files from the download directory to the data directory";

        auto metaDataFilesInDownloadDir =
                boost::make_iterator_range(
                    fs::directory_iterator(_downloadDir),
                    fs::directory_iterator()
                ) |
                // check that it is a regular file
                boost::adaptors::filtered([] (fs::directory_entry const& e) {
                    return e.status().type() == fs::file_type::regular_file;
                }) |
                // check that it ends with .torrent or .fast-resume
                boost::adaptors::filtered([] (fs::directory_entry const& e) {
                    return
                        boost::algorithm::ends_with(e.path().filename().native(), ".torrent") ||
                        boost::algorithm::ends_with(e.path().filename().native(), ".fast-resume");
                });
        for (fs::directory_entry const& e : metaDataFilesInDownloadDir) {
            fs::rename(e.path(), _dataDir / e.path().filename());
        }
    }

    _pSession.reset(new libtorrent::session());

    {
        using namespace libtorrent;

        _pSession->set_alert_mask(
                    alert::error_notification |
                    alert::storage_notification |
                    alert::status_notification |
                    alert::progress_notification |
                    alert::stats_notification);
    }

    _pSession->set_alert_dispatch([this] (std::auto_ptr<libtorrent::alert> pAlert) {
        std::unique_ptr<libtorrent::alert> uniquePtr { pAlert.release() };
        QCoreApplication::postEvent(this, new backend_libtorrent::Event(std::move(uniquePtr)));
    });

    resumePreviousSessionTorrents();

    QTimer * saveFastResumeTimer = new QTimer(this);
    connect(saveFastResumeTimer, &QTimer::timeout, this, &Session::saveFastResumeData);
    saveFastResumeTimer->start(chrono::duration_cast<chrono::milliseconds>(timeToSaveFastResumeData).count());

    QTimer * updateSessionStatusTimer = new QTimer(this);
    connect(updateSessionStatusTimer, &QTimer::timeout, this, &Session::updateSessionStatus);
    updateSessionStatusTimer->setInterval(1000);
    updateSessionStatusTimer->start();

    connect(this, &Session::preparingToExit, [this, saveFastResumeTimer, updateSessionStatusTimer] {
        updateSessionStatusTimer->stop();
        saveFastResumeTimer->stop();
    });

    connect(this, &Session::isPausedChanged, [this, updateSessionStatusTimer] {
         if (isPaused()) {
            _lastDownloadSpeedBPS = 0;
            _lastUploadSpeedBPS = 0;
            Q_EMIT downloadSpeedBPSChanged();
            Q_EMIT uploadSpeedBPSChanged();
            updateSessionStatusTimer->stop();
         }
         else {
             updateSessionStatusTimer->start();
         }
    });

    auto endTime = Clock::now();
    auto durationSec = chrono::duration_cast<chrono::duration<float>>(endTime - startTime);
    BOOST_LOG_TRIVIAL(debug) << "libtorrent: Session initialized in " << durationSec.count() << " seconds";
}

Session::~Session() {}

bool
Session::event(QEvent * pEvent) {
    assert(QThread::currentThread() == thread());
    if (pEvent->type() == backend_libtorrent::Event::getType()) {
        backend_libtorrent::Event & libtorrentEvent = dynamic_cast<backend_libtorrent::Event &>(*pEvent);
        libtorrent::alert const& alert = libtorrentEvent.getAlert();

        switch (alert.type()) {
        case libtorrent::torrent_added_alert::alert_type:
            onTorrentAdded((libtorrent::torrent_added_alert const&)alert);
            break;
        case libtorrent::metadata_received_alert::alert_type:
            onTorrentMetadataReceived((libtorrent::metadata_received_alert const&)alert);
            break;
        case libtorrent::state_changed_alert::alert_type:
            onTorrentStateChanged((libtorrent::state_changed_alert const&)alert);
            break;
        case libtorrent::torrent_paused_alert::alert_type:
            onTorrentPaused((libtorrent::torrent_paused_alert const&)alert);
            break;
        case libtorrent::torrent_resumed_alert::alert_type:
            onTorrentResumed((libtorrent::torrent_resumed_alert const&)alert);
            break;
        case libtorrent::stats_alert::alert_type:
            onStats((libtorrent::stats_alert const&)alert);
            break;
        case libtorrent::torrent_removed_alert::alert_type:
            onTorrentRemoved((libtorrent::torrent_removed_alert const&)alert);
            break;
        case libtorrent::save_resume_data_alert::alert_type:
            onResumeDataGenerated((libtorrent::save_resume_data_alert const&)alert);
        default: break;
        }

#ifdef DOWNOW_MODEL_LIBTORRENT_LOG_ALERTS
        BOOST_LOG_TRIVIAL(debug) << alert.what();
        if (alert.type() != libtorrent::add_torrent_alert::alert_type) {
            BOOST_LOG_TRIVIAL(debug) << ": " << alert.message();
        }
#endif

        return true;
    }
    else {
        return base_type::event(pEvent);
    }
}

void
Session::onTorrentAdded(libtorrent::torrent_added_alert const& alert) {
    Torrent * torrent = new Torrent(*this, alert.handle, this);
    willAddTorrent(torrent);
    _torrents.push_back(torrent);
    _handleToTorrentMap.insert({alert.handle, torrent});
    torrentAdded(torrent);

    // save torrent file
    auto startTime = Clock::now();
    if (alert.handle.is_valid() && alert.handle.status().has_metadata) {
        saveTorrentFile(alert.handle);
    }
    auto endTime = Clock::now();
    auto durationSecs = chrono::duration_cast<chrono::duration<float>>(endTime - startTime);

    BOOST_LOG_TRIVIAL(debug)
            << "Saved torrent file for torrent: " << alert.handle.name() << " in "
            << durationSecs.count() << " seconds";
}

void
Session::onStats(libtorrent::stats_alert const& alert) {
    Torrent * torrent = findTorrentByHandle(alert.handle);
    if (torrent) {
        torrentStatsUpdate(torrent, alert);
    }
}

void
Session::saveFastResumeData() {
    using namespace libtorrent;

    if (_fastResumedWaitingCount > 0) {
        BOOST_LOG_TRIVIAL(debug)
                << "libtorrent: Called saveFastResume while waiting for "
                << _fastResumedWaitingCount
                << " items to be saved. Doing nothing.";
    }
    else {
        BOOST_LOG_TRIVIAL(debug) << "libtorrent: Saving resume data";

        _fastResumedWaitingCount = 0;
        _fastResumeTimedOut = false;

        // tell all of the torrents to generate fast resume data asynchronously
        for (Torrent * torrent : _torrents) {
            auto handle = torrent->getHandle();
            if (handle.is_valid()) {
                auto status = handle.status();
                if (status.has_metadata && status.need_save_resume) {
                    _fastResumedWaitingCount++;
                    handle.save_resume_data();
                }
            }
        }

        if (_fastResumedWaitingCount == 0) {
            saveFastResumeDataDone();
        }
        else {
            QTimer::singleShot(3000, this, SLOT(saveFastResumeDataTimeOut()));
        }
    }
}

void
Session::onResumeDataGenerated(libtorrent::save_resume_data_alert const& alert) {
    using namespace libtorrent;

    torrent_handle handle = alert.handle;

    // write the resume data asynchronously
    auto future = QtConcurrent::run([
        savePath = getFastResumeFilePath(handle.name()),
        resumeData = alert.resume_data] {
        fs::ofstream out {
            savePath,
            std::ios_base::binary
        };

        bencode(std::ostreambuf_iterator<char>(out), *resumeData);
    });

    onFutureFinished(future, this, [this] (auto future) {
        assert(QThread::currentThread() == this->thread());

        _fastResumedWaitingCount--;
        if (_fastResumedWaitingCount == 0 && !_fastResumeTimedOut) {
            this->saveFastResumeDataDone();
        }
    });
}

void
Session::onTorrentRemoved(libtorrent::torrent_removed_alert const& alert) {
    auto iter = std::find_if(_torrents.begin(), _torrents.end(), [&] (Torrent * torrent) {
        return alert.handle == torrent->getHandle();
    });

    Torrent * pTorrent = *iter;

    willRemoveTorrent(pTorrent);
    _torrents.erase(iter);
    torrentRemoved(pTorrent);

    pTorrent->deleteLater();
}

boost::filesystem::path
Session::getTorrentFilePath(std::string const& torrentName) const {
    static boost::format const globalFormat {"%s.torrent"};
    return _dataDir / (boost::format(globalFormat) % torrentName).str();
}

boost::filesystem::path
Session::getFastResumeFilePath(std::string const& torrentName) const {
    static boost::format const globalFormat {"%s.fast-resume"};
    return _dataDir / (boost::format(globalFormat) % torrentName).str();
}

void
Session::resumePreviousSessionTorrents() {
    if (fs::exists(_dataDir)) {
        auto torrentFiles =
                boost::make_iterator_range(
                    fs::directory_iterator(_dataDir),
                    fs::directory_iterator()
                ) |
                // check that it is a regular file
                boost::adaptors::filtered([] (fs::directory_entry const& e) {
                    return e.status().type() == fs::file_type::regular_file;
                }) |
                // check that it ends with .torrent
                boost::adaptors::filtered([] (fs::directory_entry const& e) {
                    return boost::algorithm::ends_with(e.path().filename().native(), ".torrent");
                });

        for (fs::directory_entry const& entry : torrentFiles) {
            addTorrentByFile(QString::fromStdString(entry.path().native()));
        }
    }
}

QFuture<void>
Session::saveTorrentFile(libtorrent::torrent_handle handle) const {
    auto pTorrentFile = handle.torrent_file();
    auto savePath = getTorrentFilePath(handle.name());

    return QtConcurrent::run([pTorrentFile, savePath] {
        libtorrent::create_torrent createTorrent {
            *pTorrentFile
        };
        libtorrent::entry entry = createTorrent.generate();

        fs::ofstream outStream {
            savePath,
            std::ios_base::binary
        };
        libtorrent::bencode(std::ostreambuf_iterator<char>(outStream), entry);
    });
}

void
Session::onTorrentMetadataReceived(libtorrent::metadata_received_alert const& alert) {
    Torrent * torrent = findTorrentByHandle(alert.handle);
    if (torrent) {
        torrentMetadataReceived(torrent);
    }

    saveTorrentFile(alert.handle);
}

void
Session::onTorrentPaused(libtorrent::torrent_paused_alert const& alert) {
    Torrent * torrent = findTorrentByHandle(alert.handle);
    if (torrent) {
        torrentPaused(torrent);
    }
}

void
Session::onTorrentResumed(libtorrent::torrent_resumed_alert const& alert) {
    Torrent * torrent = findTorrentByHandle(alert.handle);
    if (torrent) {
        torrentResumed(torrent);
    }
}

void
Session::onTorrentStateChanged(libtorrent::state_changed_alert const& alert) {
    Torrent * torrent = findTorrentByHandle(alert.handle);
    if (torrent) {
        torrentStateChanged(torrent, alert.state);
    }
}

void
Session::addTorrentByMagnetLink(QUrl magnetLink) {
    libtorrent::add_torrent_params p;
    boost::system::error_code errCode;
    libtorrent::parse_magnet_uri(
                magnetLink.url().toStdString(),
                p,
                errCode);

    addTorrentByParams(p, p.name);
}

void
Session::addTorrentByFile(QString torrentFilePath) {
    libtorrent::add_torrent_params p;
    p.ti = new libtorrent::torrent_info(torrentFilePath.toStdString());

    addTorrentByParams(p, p.ti->name());
}

QDir Session::getTorrentsSaveDirectory() const {
    return QDir(QString::fromStdString(_downloadDir.native()));
}

QFuture<void>
Session::addTorrentByParams(libtorrent::add_torrent_params p, std::string const& name) {
    QFuture<void> doneFuture;
    QFutureInterface<void> doneFutureInterface;

    p.auto_managed = false;
    p.paused = false;
    p.duplicate_is_error = true;

    fs::path const fastResumeFilePath = getFastResumeFilePath(name);
    if (fs::exists(fastResumeFilePath)) {
        BOOST_LOG_TRIVIAL(debug) << "libtorrent: Using fast resume data for torrent: " << name;
        QFuture<std::vector<char>> resumeDataFuture = QtConcurrent::run([fastResumeFilePath] {
            fs::ifstream inFastResumeFile {
                fastResumeFilePath,
                std::ios_base::binary
            };

            return std::vector<char> {
                std::istreambuf_iterator<char> {inFastResumeFile},
                std::istreambuf_iterator<char> {}
            };
        });

        onFutureFinished(resumeDataFuture, this, [
                this,
                p = std::move(p),
                doneFutureInterface
            ](QFuture<std::vector<char>> resumeDataFuture) mutable {
            assert(QThread::currentThread() == thread());

#if LIBTORRENT_VERSION_MAJOR >= 1
            p.resume_data = std::move(resumeDataFuture.result());
#else
            p.resume_data = new std::vector<char>(std::move(resumeDataFuture.result()));
#endif

            QtConcurrent::run([this, p, doneFutureInterface]() mutable {
                try {
                    _pSession->add_torrent(p);
                    doneFutureInterface.reportFinished();
                }
                catch (libtorrent::duplicate_torrent const&) {
                    BOOST_LOG_TRIVIAL(debug) << "Torrent already exists: " << p.name;
                    doneFutureInterface.reportCanceled();
                    Q_EMIT duplicateTorrentError(QString::fromStdString(p.name));
                }
            });
        });
    }
    else {
        p.save_path = fs::absolute(_downloadDir).native();
        BOOST_LOG_TRIVIAL(debug) << "Adding new torrent: " << name;
        QtConcurrent::run([this, p, doneFutureInterface]() mutable {
            try {
                _pSession->add_torrent(p);
                doneFutureInterface.reportFinished();
            }
            catch (libtorrent::duplicate_torrent const&) {
                BOOST_LOG_TRIVIAL(debug) << "Torrent already exists: " << p.name;
                doneFutureInterface.reportCanceled();
                Q_EMIT duplicateTorrentError(QString::fromStdString(p.name));
            }
        });
    }

    return doneFuture;
}

static
downow::torrent_frontend::Torrent * torrentUpcast(Torrent * pTorrent) {
    return static_cast<downow::torrent_frontend::Torrent *>(pTorrent);
}

Session::TorrentRandomRange
Session::getTorrents() const {
    using namespace boost::adaptors;
    return _torrents | transformed(torrentUpcast);

    // for some weird reason transformed with a lambda doesn't seem to work under GCC
#if 0
    [] (LibtorrentTorrent * pTorrent) {
        return static_cast< ::downow::Torrent *>(pTorrent);
    });
#endif
}

void
Session::removeTorrent(downow::torrent_frontend::Torrent * pTorrent, bool deleteFiles) {
    if (Torrent * libtorrentTorrent = dynamic_cast<Torrent *>(pTorrent)) {
        BOOST_LOG_TRIVIAL(debug) << "libtorrent: Removing torrent: " << pTorrent->getName();

        libtorrent::torrent_handle handle = libtorrentTorrent->getHandle();

        // this operation is asynchronous
        if (deleteFiles) {
            _pSession->remove_torrent(handle, libtorrent::session::delete_files);
        }
        else {
            _pSession->remove_torrent(handle);
        }

        QtConcurrent::run([
            resumeFilePath = getFastResumeFilePath(handle.name()),
            torrentFilePath = getTorrentFilePath(handle.name())
        ] {
            fs::remove(resumeFilePath);
            fs::remove(torrentFilePath);
        });
    }
    else {
        BOOST_LOG_TRIVIAL(error) << "libtorrent: Not a LibtorrentTorrent." << std::endl;
    }
}

void
Session::removeTorrent(downow::torrent_frontend::Torrent * pTorrent) {
    removeTorrent(pTorrent, /*delete files*/ false);
}

void
Session::removeTorrentAndDeleteFiles(downow::torrent_frontend::Torrent * pTorrent) {
    removeTorrent(pTorrent, /*delete files*/ true);
}

void
Session::prepareToExit() {
    BOOST_LOG_TRIVIAL(debug) << "libtorrent: Preparing to exit";
    _preparingToExit = true;
    Q_EMIT preparingToExit();

    _pSession->pause();
    saveFastResumeData();
}

int Session::getMaxDownloadSpeedBPS() const {
    return _pSession->download_rate_limit();
}

int Session::getMaxUploadSpeedBPS() const {
    return _pSession->upload_rate_limit();
}

void Session::setMaxDownloadSpeedBPS(int downloadLimitBPS) {
    if (getMaxDownloadSpeedBPS() == downloadLimitBPS) {
        return;
    }

    BOOST_LOG_TRIVIAL(debug) << "Session: set max download speed to " << downloadLimitBPS << " B/s";

    _pSession->set_download_rate_limit(downloadLimitBPS);
    Q_EMIT maxDownloadSpeedBPSChanged();
}

void Session::setMaxUploadSpeedBPS(int uploadLimitBPS) {
    if (getMaxUploadSpeedBPS() == uploadLimitBPS) {
        return;
    }

    BOOST_LOG_TRIVIAL(debug) << "Session: set max upload speed to " << uploadLimitBPS << " B/s";

    _pSession->set_upload_rate_limit(uploadLimitBPS);
    Q_EMIT maxUploadSpeedBPSChanged();
}

int Session::getDownloadSpeedBPS() const {
    return _lastDownloadSpeedBPS;
}

int Session::getUploadSpeedBPS() const {
    return _lastUploadSpeedBPS;
}

void
Session::saveFastResumeDataDone() {
    BOOST_LOG_TRIVIAL(debug) << "libtorrent: Saving resume data done";
    if (_preparingToExit) {
        QtConcurrent::run([this] {
            BOOST_LOG_TRIVIAL(debug) << "libtorrent: Closing session";
            _pSession.reset();
            BOOST_LOG_TRIVIAL(debug) << "libtorrent: Closed session";

            Q_EMIT allowedToExit();
        });
    }
}

void Session::updateSessionStatus() {
    libtorrent::session_status status = _pSession->status();
    if (getDownloadSpeedBPS() != status.payload_download_rate) {
        _lastDownloadSpeedBPS = status.payload_download_rate;
        Q_EMIT downloadSpeedBPSChanged();
    }
    if (getUploadSpeedBPS() != status.payload_upload_rate) {
        _lastUploadSpeedBPS = status.payload_upload_rate;
        Q_EMIT uploadSpeedBPSChanged();
    }
}

void
Session::saveFastResumeDataTimeOut() {
    _fastResumeTimedOut = true;

    // this will be called a certain amount of seconds
    // after we started saving fast resume data regardless
    // of the operation's success.
    // We need to check if the fast resume data has already finished.
    if (_fastResumedWaitingCount > 0) {
        BOOST_LOG_TRIVIAL(debug) << "libtorrent: Fast resume timed out";
        saveFastResumeDataDone();
    }
}

Torrent *
Session::findTorrentByHandle(libtorrent::torrent_handle handle) {
    auto iter = std::find_if(_torrents.begin(), _torrents.end(), [&] (Torrent * pTorrent) {
        return pTorrent-> getHandle() == handle;
    });

    if (iter != _torrents.end()) {
        return *iter;
    }
    else {
        return nullptr;
    }
    //    return _handleToTorrentMap[handle];
}

void Session::pause() {
    if (isPaused()) {
        return;
    }

    _pSession->pause();
    assert(isPaused());
    Q_EMIT isPausedChanged();
}

void Session::resume() {
    if (!isPaused()) {
        return;
    }

    _pSession->resume();
    assert(!isPaused());
    Q_EMIT isPausedChanged();
}

bool Session::isPaused() const {
    return _pSession->is_paused();
}

} }
