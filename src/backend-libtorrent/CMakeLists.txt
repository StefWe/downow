
project(${PROJECT_NAME}-backend-libtorrent)

find_package(Qt5Core)

set(CMAKE_AUTOMOC ON)
file(GLOB SOURCES *.cpp *.hpp)

set(TARGET_NAME ${PROJECT_NAME})
add_library(${TARGET_NAME} STATIC ${SOURCES})
target_link_libraries(
    ${TARGET_NAME}
    Qt5::Core
    libtorrent-rasterbar
    downow-common
    downow-torrent-frontend
    boost-system
    boost-filesystem
    boost-log
    boost-thread
    -lpthread)
