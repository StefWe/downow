#ifndef DOWNOW_BACKEND_LIBTORRENT_TORRENT
#define DOWNOW_BACKEND_LIBTORRENT_TORRENT

#include "../torrent-frontend/Torrent.hpp"

#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/torrent.hpp>
#include <libtorrent/alert_types.hpp>

#include "FileEntry.hpp"

#include <vector>
#include <memory>

namespace downow { namespace backend_libtorrent {

struct Session;

struct Torrent : torrent_frontend::Torrent {
private:
    Q_OBJECT

public:
    using base_type = torrent_frontend::Torrent;

    Torrent(Session & session, libtorrent::torrent_handle handle, QObject * parent = nullptr);

    QString getName() const override;
    void resume() override;
    void pause() override;

    torrent_frontend::Session * getSession() const override;

    Bytes getDownloadedBytes() const override;
    Bytes getUploadedBytes() const override;
    Bytes getTotalBytes() const override;
    int getDownloadLimitBPS() const override;
    void setDownloadLimitBPS(int downloadLimit) override;
    int getUploadLimitBPS() const override;
    void setUploadLimitBPS(int uploadLimit) override;
    int getDownloadSpeedBPS() const override;
    int getUploadSpeedBPS() const override;
    Priority getPriority() const override;
    void setPriority(Priority priorty) override;
    State getState() const override;
    QString getSavePath() const override;
    QDateTime getDateAdded() const override;
    FileEntriesRandomAccessRange getFileEntries() const override;

    libtorrent::torrent_handle getHandle() const {
        return _handle;
    }

private:
    Q_SLOT void onMetadataReceived(downow::torrent_frontend::Torrent *);
    Q_SLOT void onStateChanged(Torrent *, const libtorrent::torrent_status::state_t &current);
    Q_SLOT void onStatsUpdated();
    Q_SLOT void onTorrentResumed(Torrent *);
    Q_SLOT void onTorrentPaused(Torrent *);

    void update();

    void updateStatus() const;
    void updateFileEntries();

private:
    libtorrent::torrent_status const&
    getStatus() const;

    void checkIfStateChanged();

    State _lastState = State::Preparing;

    Bytes _downloadedBytes = 0;
    Bytes _uploadedBytes = 0;
    unsigned int _uploadSpeedBPS = 0;
    unsigned int _downloadSpeedBPS = 0;

    std::vector<std::unique_ptr<FileEntry>> _fileEntries;

    mutable boost::optional<libtorrent::torrent_status> _cachedStatus;
    libtorrent::torrent_handle _handle;

    Session * _pSession;
};

} }

#endif
