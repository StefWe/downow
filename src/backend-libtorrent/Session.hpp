#ifndef DOWNOW_BACKEND_LIBTORRENT_SESSION
#define DOWNOW_BACKEND_LIBTORRENT_SESSION

#include "../torrent-frontend/Session.hpp"

#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/session_status.hpp>

#include <unordered_map>
#include <boost/filesystem/path.hpp>
#include <qt5/QtCore/QFuture>

#include <boost/optional.hpp>

#include "Torrent.hpp"

namespace std {

template <>
struct hash<libtorrent::torrent_handle> {
    std::size_t operator() (libtorrent::torrent_handle const& h) const {
        return 1;
//        return hash<libtorrent::torrent *>()(h.native_handle().get());
    }
};

}

namespace libtorrent {
    class session;
    class entry;
    struct torrent_added_alert;
    struct metadata_received_alert;
    struct torrent_paused_alert;
    struct torrent_resumed_alert;
    struct state_changed_alert;
    struct stats_alert;
    struct add_torrent_params;
    struct save_resume_data_alert;
    struct torrent_removed_alert;
}

namespace downow { namespace backend_libtorrent {

struct Torrent;

struct Session : torrent_frontend::Session {
private:
    Q_OBJECT

public:
    using base_type = torrent_frontend::Session;

    Session(
            boost::filesystem::path downloadDir = {"downloads"},
            boost::filesystem::path dataDir = {"data"},
            boost::optional<int> lastRevision = {},
            QObject * parent = nullptr);
    ~Session() override;

    Torrent * findTorrentByHandle(libtorrent::torrent_handle);

    void pause() override;
    void resume() override;
    bool isPaused() const override;

    bool event(QEvent *) override;

    void addTorrentByMagnetLink(QUrl magnetLink) override;
    void addTorrentByFile(QString torrentFilePath) override;

    QDir getTorrentsSaveDirectory() const override;

    TorrentRandomRange
    getTorrents() const override;

    Q_SIGNAL void torrentStatsUpdate(Torrent *, libtorrent::stats_alert const&);
    Q_SIGNAL void torrentPaused(Torrent *);
    Q_SIGNAL void torrentResumed(Torrent *);
    Q_SIGNAL void torrentStateChanged(Torrent *, libtorrent::torrent_status::state_t const& current);
    Q_SIGNAL void preparingToExit();

    void removeTorrent(downow::torrent_frontend::Torrent *, bool deleteFiles);
    void removeTorrent(downow::torrent_frontend::Torrent *) override;
    void removeTorrentAndDeleteFiles(downow::torrent_frontend::Torrent *) override;

    void prepareToExit() override;

    int getMaxDownloadSpeedBPS() const override;
    int getMaxUploadSpeedBPS() const override;
    void setMaxDownloadSpeedBPS(int) override;
    void setMaxUploadSpeedBPS(int) override;

    int getDownloadSpeedBPS() const override;
    int getUploadSpeedBPS() const override;

private:
    void onTorrentAdded(libtorrent::torrent_added_alert const&);
    void onTorrentMetadataReceived(libtorrent::metadata_received_alert const&);
    void onTorrentPaused(libtorrent::torrent_paused_alert const&);
    void onTorrentResumed(libtorrent::torrent_resumed_alert const&);
    void onTorrentStateChanged(libtorrent::state_changed_alert const&);
    void onStats(libtorrent::stats_alert const&);
    void onResumeDataGenerated(libtorrent::save_resume_data_alert const&);
    void onTorrentRemoved(libtorrent::torrent_removed_alert const&);

    boost::filesystem::path
    getTorrentFilePath(std::string const& torrentName) const;

    boost::filesystem::path
    getFastResumeFilePath(std::string const& torrentName) const;

    QFuture<void> addTorrentByParams(libtorrent::add_torrent_params, std::string const& name);

    void saveFastResumeData();
    void resumePreviousSessionTorrents();

    QFuture<void>
    saveTorrentFile(libtorrent::torrent_handle) const;

    Q_SLOT void saveFastResumeDataTimeOut();
    void saveFastResumeDataDone();

    Q_SLOT void updateSessionStatus();

private:
    std::unique_ptr<libtorrent::session> _pSession;
    std::vector<Torrent *> _torrents;
    std::unordered_map<libtorrent::torrent_handle, Torrent *> _handleToTorrentMap;

    boost::filesystem::path _downloadDir;
    boost::filesystem::path _dataDir;

    int _lastDownloadSpeedBPS = 0;
    int _lastUploadSpeedBPS = 0;

    int _fastResumedWaitingCount = 0;
    bool _fastResumeTimedOut = false;
    bool _preparingToExit = false;
};

} }

#endif
