#include "Torrent.hpp"
#include "Session.hpp"

#include <boost/log/trivial.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/filesystem/path.hpp>
#include <vector>
#include <chrono>
#include <memory>
#include <qt5/QtConcurrent/QtConcurrent>

#include "../common/Utils.hpp"

namespace downow { namespace backend_libtorrent {

namespace chrono = std::chrono;
using Clock = chrono::steady_clock;

static_assert(std::numeric_limits<Torrent::Bytes>::max() >= std::numeric_limits<libtorrent::size_type>::max(), "Torrent::Bytes type is not big enough to hold all values that libtorrent::size_type can hold");

Torrent::Torrent(Session & session, libtorrent::torrent_handle handle, QObject * parent) :
    base_type(&session, parent),
    _handle(handle),
    _pSession(&session) {
    connect(&session, &Session::torrentResumed, this, &Torrent::onTorrentResumed);
    connect(&session, &Session::torrentPaused, this, &Torrent::onTorrentPaused);
    connect(&session, &Session::torrentMetadataReceived, this, &Torrent::onMetadataReceived);
    connect(&session, &Session::torrentStateChanged, this, &Torrent::onStateChanged);
    connect(&session, &Session::isPausedChanged, this, [this] {
        _downloadSpeedBPS = 0;
        _uploadSpeedBPS = 0;

        Q_EMIT downloadSpeedBPSChanged();
        Q_EMIT uploadSpeedBPSChanged();

        checkIfStateChanged();
    });

    connect(&session, &Session::torrentStatsUpdate, this, [this] (Torrent * pTorrent) {
        if (pTorrent == this) {
            onStatsUpdated();
        }
    });

    connect(this, &Torrent::stateChanged, [this] {
        _downloadSpeedBPS = 0;
        _uploadSpeedBPS = 0;
        downloadSpeedBPSChanged();
        uploadSpeedBPSChanged();
    });

    if (_handle.has_metadata()) {
        checkIfStateChanged();
        updateFileEntries();
        onStatsUpdated();
    }
}

QString
Torrent::getName() const {
    try {
        return QString::fromStdString(_handle.name());
    }
    catch (libtorrent::invalid_handle const&) {
        return {};
    }
}

Torrent::State
Torrent::getState() const {
    return _lastState;
}

QString
Torrent::getSavePath() const {
    try {
        return QString::fromStdString(_handle.save_path());
    }
    catch (libtorrent::invalid_handle const&) {
        return {};
    }
}

QDateTime
Torrent::getDateAdded() const {
    try {
        return QDateTime::fromTime_t(getStatus().added_time);
    }
    catch (libtorrent::invalid_handle const&) {
        return {};
    }
}

Torrent::FileEntriesRandomAccessRange
Torrent::getFileEntries() const {
    struct transformer {
        FileEntry * operator() (std::unique_ptr<FileEntry> const& ptr) const {
            return ptr.get();
        }
    };

    return _fileEntries | boost::adaptors::transformed(transformer());
}

int
Torrent::getDownloadLimitBPS() const {
    try {
        return _handle.download_limit();
    }
    catch (libtorrent::invalid_handle const&) {
        return 0;
    }
}

int
Torrent::getUploadLimitBPS() const {
    try {
        return _handle.upload_limit();
    }
    catch (libtorrent::invalid_handle const&) {
        return 0;
    }
}

Torrent::Priority
Torrent::getPriority() const {
    return Priority::Normal;
}

void
Torrent::setDownloadLimitBPS(int downloadLimit) {
    if (getDownloadLimitBPS() == downloadLimit) {
        return;
    }

    _handle.set_download_limit(downloadLimit);
    downloadLimitBPSChanged();
}

void
Torrent::setUploadLimitBPS(int uploadLimit) {
    if (getUploadLimitBPS() == uploadLimit) {
        return;
    }

    _handle.set_upload_limit(uploadLimit);
    uploadLimitBPSChanged();
}

void
Torrent::setPriority(Priority priority) {
    if (getPriority() == priority) {
        return;
    }

    _handle.set_priority((int)priority);
    priorityChanged();
}

void
Torrent::resume() {
    if (getSession()->isPaused()) {
        getSession()->resume();
    }

    if (_handle.is_paused()) {
        BOOST_LOG_TRIVIAL(debug) << "Resume torrent: " << getName();
        _handle.resume();
    }
}

void
Torrent::pause() {
    if (!_handle.is_paused()) {
        BOOST_LOG_TRIVIAL(debug) << "Pause torrent: " << getName();
        _handle.pause();
    }
}

downow::torrent_frontend::Session *
Torrent::getSession() const {
    return _pSession;
}

void
Torrent::onMetadataReceived(downow::torrent_frontend::Torrent * pTorrent) {
    if (pTorrent != this) {
        return;
    }

    BOOST_LOG_TRIVIAL(debug) << "Metadata received, torrent: " << getName();
    nameChanged();
    totalBytesChanged();

    updateFileEntries();
}

void
Torrent::onStateChanged(Torrent * pTorrent, libtorrent::torrent_status::state_t const& current) {
    if (pTorrent != this) {
        return;
    }

    // sometimes libtorrent enters an intermediate Downloading state
    // although it's finished.
    // if this is the case, disregard.
    if (current == libtorrent::torrent_status::state_t::downloading && getStatus().is_finished) {
        return;
    }

    checkIfStateChanged();
}

void
Torrent::onStatsUpdated() {
    updateStatus();

    if (getStatus().has_metadata) {
        auto downloadedBytesForFileEntryByIndexFuture = QtConcurrent::run([handle = _handle] {
            std::vector<libtorrent::size_type> res;
            handle.file_progress(res);
            return res;
        });

        onFutureFinished(downloadedBytesForFileEntryByIndexFuture, this, [this] (auto downloadedBytesForFileEntryByIndexFuture) {
            assert(QThread::currentThread() == this->thread());

            auto downloadedBytesForFileEntryByIndex = downloadedBytesForFileEntryByIndexFuture.result();
            assert(_fileEntries.size() == downloadedBytesForFileEntryByIndex.size());

            for (std::size_t i = 0; i < _fileEntries.size(); i++) {
                _fileEntries[i]->setDownloadedBytes(downloadedBytesForFileEntryByIndex[i]);
            }
        });
    }

    decltype(_downloadedBytes) downloadedBytes = getStatus().progress * getTotalBytes();
    if (_downloadedBytes != downloadedBytes) {
        _downloadedBytes = downloadedBytes;
        downloadedBytesChanged();
    }

    assert(getStatus().all_time_upload >= 0);
    Bytes allTimeUpload = getStatus().all_time_upload;
    if (_uploadedBytes != allTimeUpload) {
        _uploadedBytes = allTimeUpload;
        uploadedBytesChanged();
    }

    assert(getStatus().download_payload_rate >= 0);
    decltype(_downloadSpeedBPS) downloadSpeedBPS = getStatus().download_payload_rate;
    if (_downloadSpeedBPS != downloadSpeedBPS) {
        _downloadSpeedBPS = downloadSpeedBPS;
        downloadSpeedBPSChanged();
    }

    assert(getStatus().upload_payload_rate >= 0);
    decltype(_uploadSpeedBPS) uploadSpeedBPS = getStatus().upload_payload_rate;
    if (_uploadSpeedBPS != uploadSpeedBPS) {
        _uploadSpeedBPS = uploadSpeedBPS;
        uploadSpeedBPSChanged();
    }
}

Torrent::Bytes
Torrent::getDownloadedBytes() const {
    return _downloadedBytes;
}

Torrent::Bytes
Torrent::getUploadedBytes() const {
    return _uploadedBytes;
}

int
Torrent::getDownloadSpeedBPS() const {
    return _downloadSpeedBPS;
}

int
Torrent::getUploadSpeedBPS() const {
    return _uploadSpeedBPS;
}

void
Torrent::onTorrentResumed(Torrent * torrent) {
    if (torrent != this) {
        return;
    }

    checkIfStateChanged();
}

void
Torrent::onTorrentPaused(Torrent * torrent) {
    if (torrent != this) {
        return;
    }

    checkIfStateChanged();
}

Torrent::Bytes
Torrent::getTotalBytes() const {
    try {
        return _handle.get_torrent_info().total_size();
    }
    catch (libtorrent::invalid_handle const&) {
        return 0;
    }
}

void
Torrent::updateStatus() const {
    _cachedStatus = _handle.status();
}

void
Torrent::updateFileEntries() {
    auto pTorrentFile = _handle.torrent_file();

    Q_EMIT fileEntriesWillChange();

    auto startTime = Clock::now();

    _fileEntries.clear();
    for (int i = 0; i < pTorrentFile->num_files(); i++) {
        libtorrent::file_entry const& fileEntry = pTorrentFile->file_at(i);

        using boost::filesystem::path;
        path pathAbsolute = path { _handle.save_path() } / path { fileEntry.path };
        _fileEntries.push_back(std::make_unique<FileEntry>(
            this,
            QFileInfo(QString::fromStdString(pathAbsolute.native())),
            fileEntry.size
        ));
    }

    auto endTime = Clock::now();
    auto durationSec = chrono::duration_cast<chrono::duration<float>>(endTime - startTime);
    BOOST_LOG_TRIVIAL(debug)
            << "Updated file entries synchronously for torrent: " << getName()
            << " in " << durationSec.count() << " seconds";

    Q_EMIT fileEntriesChanged();
}

libtorrent::torrent_status const&
Torrent::getStatus() const {
    if (!_cachedStatus) {
        updateStatus();
    }
    return *_cachedStatus;
}

void
Torrent::checkIfStateChanged() {
    updateStatus();
    State newState = [this] {
        try {
            auto const& status = getStatus();
            using state_t = libtorrent::torrent_status::state_t;
            switch (status.state) {
            case state_t::queued_for_checking:
            case state_t::checking_files:
            case state_t::allocating:
            case state_t::checking_resume_data:
            case state_t::downloading_metadata:
                return Torrent::State::Preparing;

            case state_t::downloading:
                if (status.paused || getSession()->isPaused()) {
                    return State::Paused;
                }
                else {
                    return Torrent::State::Downloading;
                }

            case state_t::finished:
            case state_t::seeding:
                return Torrent::State::Seeding;

            default:
                return Torrent::State::Invalid;
            }
        }
        catch (libtorrent::invalid_handle const&) {
            return State::Preparing;
        }
    }();

    if (_lastState == newState) {
        return;
    }

    State oldState = _lastState;
    _lastState = newState;

    Q_EMIT stateChanged(oldState, newState);
}

} }
