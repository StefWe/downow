#ifndef DOWNOW_BACKEND_LIBTORRENT_FILE_ENTRY
#define DOWNOW_BACKEND_LIBTORRENT_FILE_ENTRY

#include "../torrent-frontend/FileEntry.hpp"
#include <qt5/QtCore/QPointer>
#include <qt5/QtCore/QFileInfo>

namespace downow { namespace backend_libtorrent {

struct Torrent;

struct FileEntry : downow::torrent_frontend::FileEntry {
private:
    Q_OBJECT

    // let Torrent access the setter methods
    friend struct downow::backend_libtorrent::Torrent;

public:
    using base_type = downow::torrent_frontend::FileEntry;
    FileEntry(Torrent *, QFileInfo, Bytes totalSizeBytes, QObject * parent = nullptr);

    Bytes getDownloadedBytes() const override {
        return _downloadedBytes;
    }
    Bytes getUploadedBytes() const override {
        return 0;
    }
    Bytes getTotalBytes() const override {
        return _totalBytes;
    }
    int getDownloadSpeedBPS() const override {
        return 0;
    }
    int getUploadSpeedBPS() const override {
        return 0;
    }

    downow::torrent_frontend::Torrent * getTorrent() const override;

protected:
    QFileInfo getFileInfo() const override {
        return _fileInfo;
    }

private:
    void setDownloadedBytes(Bytes downloadedBytes);
    void setTotalBytes(Bytes totalBytes);

private:
    QFileInfo _fileInfo;
    Bytes _downloadedBytes = 0;
    Bytes _totalBytes = 0;
    QPointer<Torrent> _torrent;
};

} }

#endif
