#include "Event.hpp"

namespace downow { namespace backend_libtorrent {

QEvent::Type
Event::getType() {
    static QEvent::Type type = QEvent::Type::None;
    if (type == QEvent::Type::None) {
        type = (QEvent::Type)QEvent::registerEventType();
    }

    return type;
}

} }
