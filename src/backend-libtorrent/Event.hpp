#ifndef DOWNOW_BACKEND_LIBTORRENT_EVENT
#define DOWNOW_BACKEND_LIBTORRENT_EVENT

#include <QEvent>
#include <libtorrent/alert.hpp>
#include <memory>
#include <boost/exception/all.hpp>
#include <exception>

namespace downow { namespace backend_libtorrent {

struct Event : QEvent {
    Event();

    Event(std::unique_ptr<libtorrent::alert> pAlert) :
        QEvent(getType()),
        _pAlert(std::move(pAlert)) {}

    libtorrent::alert const&
    getAlert() const {
        if (_pAlert) {
            return *_pAlert;
        }
        else {
            BOOST_THROW_EXCEPTION(std::runtime_error("Alert is null"));
        }
    }

    std::unique_ptr<libtorrent::alert>
    stealAlert() {
        return std::move(_pAlert);
    }

    static QEvent::Type getType();

private:
    std::unique_ptr<libtorrent::alert> _pAlert;
};

} }

#endif
