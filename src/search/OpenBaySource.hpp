#ifndef DOWNOW_MODEL_SEARCH_OPEN_BAY_SOURCE
#define DOWNOW_MODEL_SEARCH_OPEN_BAY_SOURCE

#include "HttpSource.hpp"

namespace downow {
namespace search {

struct OpenBaySource : HttpSource {
private:
    Q_OBJECT

    using base_type = HttpSource;
public:
    OpenBaySource(QNetworkAccessManager * pnam = nullptr, QObject * parent = nullptr);

    static QString getSourceType();
    QString getType() const override {
        return getSourceType();
    }

protected:
    void parse(QByteArray) override;
    QUrl createUrlForQuery(QString query) override;
    QString getHost() override;
    QString getQueryFromUrl(const QUrl &);
};

} }

#endif
