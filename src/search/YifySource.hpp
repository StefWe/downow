#ifndef DOWNOW_MODEL_SEARCH_YIFY_SOURCE
#define DOWNOW_MODEL_SEARCH_YIFY_SOURCE

#include "HttpSource.hpp"

#include <memory>

namespace downow {
namespace search {

struct YifySource : HttpSource {
private:
    Q_OBJECT

    using base_type = HttpSource;

public:
    YifySource(QNetworkAccessManager * nam = nullptr, QObject * parent = nullptr);

    static QString getSourceType();
    QString getType() const override {
        return getSourceType();
    }

protected:
    void parse(QByteArray) override;
    QUrl createUrlForQuery(QString query) override;
    QString getHost() override;
    QString getQueryFromUrl(const QUrl &) override;
};

struct VideoResult : Result {
private:
    Q_OBJECT
    Q_PROPERTY(QString quality READ getQuality WRITE setQuality NOTIFY qualityChanged)
    Q_PROPERTY(QUrl coverImageUrl READ getCoverImageUrl WRITE setCoverImageUrl NOTIFY coverImageUrlChanged)
    Q_PROPERTY(QString imdbID READ getImdbID WRITE setImdbID NOTIFY imdbIDChanged)
    Q_PROPERTY(QStringList genres READ getGenres WRITE setGenres NOTIFY genresChanged)

    using base_type = Result;

public:
    VideoResult(QObject * parent = nullptr) :
        base_type(parent) {}

    QString getQuality() const {
        return _quality;
    }

    void setQuality(QString quality) {
        if (_quality == quality) {
            return;
        }

        _quality = quality;
        Q_EMIT qualityChanged();
    }

    QUrl getCoverImageUrl() const {
        return _coverImageUrl;
    }

    void setCoverImageUrl(QUrl url) {
        if (_coverImageUrl == url) {
            return;
        }

        _coverImageUrl = url;
        Q_EMIT coverImageUrlChanged();
    }

    QString getImdbID() const {
        return _imdbID;
    }

    void setImdbID(QString imdbID) {
        if (_imdbID == imdbID) {
            return;
        }

        _imdbID = imdbID;
        Q_EMIT imdbIDChanged();
    }

    QStringList getGenres() const {
        return _genres;
    }

    void setGenres(QStringList genres) {
        if (_genres == genres) {
            return;
        }

        _genres = genres;
        Q_EMIT genresChanged();
    }

Q_SIGNALS:
    void qualityChanged();
    void coverImageUrlChanged();
    void imdbIDChanged();
    void genresChanged();

private:
    QString _quality;
    QUrl _coverImageUrl;
    QString _imdbID;
    QStringList _genres;
};

} }

#endif
