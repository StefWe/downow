#ifndef DOWNOW_MODEL_SEARCH_SOURCE
#define DOWNOW_MODEL_SEARCH_SOURCE

#include <QObject>
#include <QString>

#include <boost/range/any_range.hpp>

namespace downow {
namespace search {

struct Result;

struct Source : QObject {
private:
    Q_OBJECT
    Q_PROPERTY(bool isLoading READ isLoading NOTIFY isLoadingChanged)
    Q_PROPERTY(QString query READ getQuery WRITE setQuery NOTIFY queryChanged)
    Q_PROPERTY(QString lastLoadedQuery READ getLastLoadedQuery NOTIFY lastLoadedQueryChanged)
    Q_PROPERTY(QString type READ getType NOTIFY typeChanged)
    Q_PROPERTY(Error error READ getError NOTIFY errorChanged)
    Q_ENUMS(Error)

public:
    Source(QObject * parent = nullptr) : QObject(parent) {}

    enum class Error {
        Internet,
        Parsing,
        None
    };

    virtual
    void
    setQuery(QString const&) = 0;

    virtual
    QString
    getQuery() const = 0;

    virtual
    Error
    getError() const = 0;

    virtual
    QString
    getLastLoadedQuery() const = 0;

    virtual
    bool
    isLoading() const = 0;

    virtual
    QString
    getType() const = 0;

    using ResultsRandomAccessRange = boost::any_range<
        Result *,
        boost::random_access_traversal_tag,
        Result *,
        std::ptrdiff_t
    >;

    virtual
    ResultsRandomAccessRange
    getResults() const = 0;

    virtual ~Source() {}

Q_SIGNALS:
    void queryChanged();
    void lastLoadedQueryChanged();
    void resultsWillChange();
    void resultsChanged();
    void isLoadingChanged();
    void typeChanged();
    void errorChanged();
};

} }

#endif
