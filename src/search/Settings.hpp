#ifndef SEARCHSETTINGS_HPP
#define SEARCHSETTINGS_HPP

#include <qt5/QtCore/QObject>
#include <memory>

struct QSettings;

namespace downow { namespace search {

struct Settings : QObject {
private:
    Q_OBJECT

public:
    Settings(std::shared_ptr<QSettings>);
    static Settings & instance();

private:
    std::shared_ptr<QSettings> _settings;
};

} }

#endif
