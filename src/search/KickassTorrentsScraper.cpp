#include "KickassTorrentsScraper.hpp"
#include "Tidy.hpp"

#include "../common/Utils.hpp"
#include <QMap>

namespace downow {
namespace search {
namespace kickass {
namespace detail {

boost::optional<std::vector<KickassTorrent>>
scrape(QByteArray html) {
    QDomDocument doc = parseHtml(html.data());
    if (doc.isNull()) {
        return {};
    }

    QDomNodeList tableNodes = doc.elementsByTagName("table");
    QDomElement resultsTable;
    for (int i = 0; i < tableNodes.size(); i++) {
        QDomElement tableElem = tableNodes.at(i).toElement();
        if (tableElem.attribute("class").contains("data")) {
            resultsTable = tableElem;
            break;
        }
    }

    if (resultsTable.isNull()) {
        return {};
    }

    /*
       <tr class="odd" id="torrent_hunger10218554">
            <td>
                <div class="iaconbox center floatright">
                    <a rel="10218554,0" class="icommentjs icon16" href="/the-hunger-games-mockingjay-p1-2014-ts-xvid-ac3-eng-srt-murd3r-t10218554.html#comment"><em style="font-size: 12px; margin: 0 4px 0 4px;" class="iconvalue">7</em><i class="ka ka-comment"></i></a>				<a class="iverify icon16" href="/the-hunger-games-mockingjay-p1-2014-ts-xvid-ac3-eng-srt-murd3r-t10218554.html" title="Verified Torrent"><i class="ka ka16 ka-verify ka-green"></i></a>                                <a href="#" onclick="_scq.push(['redirect', '_b91ea3142d712c64815b8c569ca90f34', { 'name': 'The%20Hunger%20Games%20Mockingjay%20P1%202014%20TS%20XviD%20AC3%20ENG%20SRT%20MURD3R', 'magnet': 'magnet%3A%3Fxt%3Durn%3Abtih%3AB66596074A9503C5FC7C31262825F08544975F04%26dn%3Dthe%2Bhunger%2Bgames%2Bmockingjay%2Bp1%2B2014%2Bts%2Bxvid%2Bac3%2Beng%2Bsrt%2Bmurd3r%26tr%3Dudp%253A%252F%252Fglotorrents.pw%253A6969%252Fannounce%26tr%3Dudp%253A%252F%252Fopen.demonii.com%253A1337' }]); return false;" class="partner1Button idownload icon16"><i class="ka ka16 ka-arrow-down partner1Button"></i></a>

                    <a title="Torrent magnet link" href="magnet:?xt=urn:btih:B66596074A9503C5FC7C31262825F08544975F04&amp;dn=the+hunger+games+mockingjay+p1+2014+ts+xvid+ac3+eng+srt+murd3r&amp;tr=udp%3A%2F%2Fglotorrents.pw%3A6969%2Fannounce&amp;tr=udp%3A%2F%2Fopen.demonii.com%3A1337" class="imagnet icon16"><i class="ka ka16 ka-magnet"></i></a>
                    <a title="Download torrent file" href="http://torcache.net/torrent/B66596074A9503C5FC7C31262825F08544975F04.torrent?title=[kickass.to]the.hunger.games.mockingjay.p1.2014.ts.xvid.ac3.eng.srt.murd3r" class="idownload icon16"><i class="ka ka16 ka-arrow-down"></i></a>
                </div>
                <div class="torrentname">
                    <a href="/the-hunger-games-mockingjay-p1-2014-ts-xvid-ac3-eng-srt-murd3r-t10218554.html" class="torType filmType"></a>
                    <a href="/the-hunger-games-mockingjay-p1-2014-ts-xvid-ac3-eng-srt-murd3r-t10218554.html" class="normalgrey font12px plain bold"></a>
                <div class="markeredBlock torType filmType">

                    <a href="/the-hunger-games-mockingjay-p1-2014-ts-xvid-ac3-eng-srt-murd3r-t10218554.html" class="cellMainLink">The <strong class="red">Hunger</strong> Games Mockingjay P1 2014 TS XviD AC3 ENG SRT MURD3R</a>

                                                    <span class="font11px lightgrey block">
                                    Posted by <i class="ka ka-verify" style="font-size: 16px;color:orange;"></i> <a class="plain" href="/user/DOCMURDER82/">DOCMURDER82</a> in <span id="cat_10218554"><strong><a href="/movies/">Movies</a></strong></span>                	                </span>
                                </div>
                </div>
            </td>
            <td class="nobr center">1.52 <span>GB</span></td>
            <td class="center">18</td>
            <td class="center">16&nbsp;hours</td>
            <td class="green center">831</td>
            <td class="red lasttd center">1732</td>
        </tr>
     */

    std::vector<KickassTorrent> torrents;
    QDomNodeList rows = resultsTable.elementsByTagName("tr");
    for (int i = 0; i < rows.size(); i++) {
        QDomElement row = rows.at(i).toElement();
        if (row.attribute("class").contains("odd") || row.attribute("class").contains("even")) {
            KickassTorrent torrent;
            {
                QDomElement nameColumnElem = row.childNodes().at(0).toElement();

                {
                    QDomNodeList magnetElems = nameColumnElem.firstChildElement().elementsByTagName("a");
                    QDomElement magnetLinkElem;
                    for (int j = 0; j < magnetElems.size(); j++) {
                        if (magnetElems.at(j).toElement().attribute("class").contains("imagnet")) {
                            magnetLinkElem = magnetElems.at(j).toElement();
                            break;
                        }
                    }
                    torrent.magnetUrl = magnetLinkElem.attribute("href").toStdString();
                }

                {
                    QDomElement torrentNameElem = nameColumnElem.lastChildElement();
                    torrent.name = torrentNameElem
                            .lastChildElement()
                            .firstChildElement("a")
                            .text()
                            .replace('\n', ' ')
                            .toStdString();
                }
            }
            {
                QString sizeString = row.childNodes().at(1).toElement().text();
                QStringList components = sizeString.split(' ');
                torrent.size.value = components[0].toFloat();
                static QMap<QString, ByteUnit> const strToByteUnit {
                    {"GB", ByteUnit::GB},
                    {"MB", ByteUnit::MB},
                    {"KB", ByteUnit::KB},
                    {"TB", ByteUnit::TB}
                };
                auto unitIter = strToByteUnit.find(components[1]);
                if (unitIter != strToByteUnit.end()) {
                    torrent.size.unit = unitIter.value();
                }
                else {
                    // Unknown unit
                }
            }

            torrent.seeders = row.childNodes().at(4).toElement().text().toUInt();

            torrents.push_back(std::move(torrent));
        }
    }

    return std::move(torrents);
}

} } } }
