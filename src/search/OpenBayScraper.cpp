
#include "OpenBayScraper.hpp"

#define BOOST_SPIRIT_UNICODE // We'll use unicode (UTF8) all throughout

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted/struct.hpp>

#include <boost/log/trivial.hpp>
#include <sstream>

BOOST_FUSION_ADAPT_STRUCT(
    downow::search::openbay::detail::Size,
    (float, value)
    (downow::search::openbay::detail::ByteUnit, unit)
)

BOOST_FUSION_ADAPT_STRUCT(
    downow::search::openbay::detail::TitleRow,
    (std::string, magnetLink)
    (std::string, name)
)

BOOST_FUSION_ADAPT_STRUCT(
    downow::search::openbay::detail::OpenBayTorrent,
    (downow::search::openbay::detail::TitleRow, titleRow)
    (downow::search::openbay::detail::Size, size)
    (int, seeders)
)

namespace downow {
namespace search {
namespace openbay {
namespace detail {

namespace qi = boost::spirit::qi;

struct SizeUnit_ : qi::symbols<char, ByteUnit> {
    SizeUnit_()
    {
        add
            ("B",   ByteUnit::B)
            ("KiB", ByteUnit::KB)
            ("MiB", ByteUnit::MB)
            ("GiB", ByteUnit::GB)
            ("TiB", ByteUnit::TB)
        ;
    }
} SizeUnit;

namespace charset = qi::unicode;

template <typename Iterator>
struct TorrentsGrammer : qi::grammar<Iterator, std::vector<OpenBayTorrent>(), charset::space_type> {
    TorrentsGrammer() : TorrentsGrammer::base_type(start, "torrents") {
        using namespace qi;
        using charset::space;
        using namespace qi::labels;

        /*
            <tr data-key="6281558">
                <td>
                    <div style="float: right; height: 16px;">
                        <a
                            href="magnet:?xt=urn:btih:3cde13156eddb456473b36935c955fd9acd87c07&amp;amp;dn=heyzo0103&amp;amp;xl=2763615392&amp;amp;dl=2763615392"
                            title="MAGNET LINK">
                            <img src="/img/icons/magnet.png">
                        </a>
                    </div>
                    <a href="/torrent/6281558/heyzo0103">
                        heyzo0103
                    </a>
                    <br/>
                    <em>
                        <small>
                            Download from <a href="/search?iht=7" title="Browse other torrents">other</a>
                        </small>
                    </em>
                </td>
                <td>
                    a year ago
                </td>
                <td>
                    2.57 GiB
                </td>
                <td>
                    2
                </td>
                <td>
                    19
                </td>
            </tr>
        */

        start %=
            omit[
                *(char_ - tpbIndicator) > tpbIndicator
            ]
            > -(
                omit[
                    *(char_ - searchResultTableIndicator) >> searchResultTableIndicator
                 >> lit("<thead") >> *(char_ - lit("</thead>")) >> lit("</thead>")
                 >> *(char_ - torrentRowIndicator)
                ]
              > *torrent
              > omit[*char_]
            );

        torrentRowIndicator =
            lit("<tr") >> *(char_ - ('>' >> lit("<td"))) >> '>';

        torrent %=
            torrentRowIndicator
          > titleRow
          > skipTableRow // date row, not interested
          > sizeRow
          > seedersRow
          > skipUntilEndOfTorrent;

        titleRow %=
            omit[
                lit("<td") >> *(char_ - '>') >> lit('>')
             >> lit("<div") >> *(char_ - ('>' >> lit("<a"))) >> lit('>')
            ]
          > magnetLink
          > omit[
                *(char_ - "</div>") >> "</div>"
             >> lit("<a") >> *(char_ - '>') >> lit('>')
            ]
          > lexeme[
                *(char_ - "</a>")
            ]
          > omit[
            *(char_ - lit("</td>")) >> lit("</td>")
         ];

        sizeRowIndicator =
            lit("<td") >> *(char_ - '>') >> lit('>');

        sizeRow %=
            sizeRowIndicator
          > size
          > lit("</td>");

        seedersRow %=
            omit[
                lit("<td") >> *(char_ - '>') >> lit(">")
            ]
          > int_
          > lit("</td>");

        magnetLink %=
            omit[
                lexeme[lit("<a") >> space] >> lit("href=\"")
            ]
         >> lexeme[
                +(char_ - lit('"'))
            ]
         >> lit('"')
         >> omit[
                *(char_ - lit("</a>")) >> lit("</a>")
            ];

        tpbIndicator =
            no_case[
                lit("<title>") > *(char_ - (lit("Open") > "Pirate" > "Bay")) > (lit("Open") > "Pirate" > "Bay") >
                *(char_ - lit("</title>")) > lit("</title>")
            ];

        searchResultTableIndicator =
            lit("<table") > lit("class=\"table-torrents") > *(char_ - '>') > lit('>');

        size %=
            float_
          > omit[
                *(char_ - SizeUnit)
            ]
          > SizeUnit;

        skipUntilEndOfTorrent =
            *(char_ - lit("</tr>")) >> lit("</tr>");

        skipTableRow =
            lexeme[
                lit("<td") >> (space | '>')
            ]
         >> *(char_ - "</td>") >> "</td>";

#define ASSIGN_NAME(RULE) RULE.name(#RULE)
        ASSIGN_NAME(start);
        ASSIGN_NAME(tpbIndicator);
        ASSIGN_NAME(torrent);
        ASSIGN_NAME(magnetLink);
        ASSIGN_NAME(sizeRow);
        ASSIGN_NAME(size);
        ASSIGN_NAME(titleRow);
#undef ASSIGN_NAME

        qi::on_error<qi::fail>(start, [] (auto params, auto context, auto noidea) {
            using namespace boost::fusion;
            std::ostringstream s;
            s
                    << "Error! Expecting "
                    << at_c<3>(params)
                    << " here: \""
                    << std::string(at_c<2>(params), at_c<1>(params))
                    << "\"";
            BOOST_LOG_TRIVIAL(debug) << s.str();
        });
    }

    template <typename T, typename Skipper=charset::space_type>
    using rule = qi::rule<Iterator, T, Skipper>;

    rule<std::vector<OpenBayTorrent>()> start;
    rule<OpenBayTorrent()> torrent;
    rule<TitleRow()> titleRow;
    rule<std::string()> magnetLink;
    rule<Size()> sizeRow;
    rule<Size()> size;
    rule<int()> seedersRow;

    rule<void()> tpbIndicator;
    rule<void()> torrentRowIndicator;
    rule<void()> searchResultTableIndicator;
    rule<void()> sizeRowIndicator;
    rule<void()> skipUntilEndOfTorrent;
    rule<void()> skipTableRow;
};

boost::optional<std::vector<OpenBayTorrent>>
scrape(QByteArray html) {
    std::vector<OpenBayTorrent> torrents;
    bool success =
            qi::phrase_parse(
                html.begin(),
                html.end(),
                TorrentsGrammer<QByteArray::iterator>(),
                charset::space,
                torrents);
    if (success) {
        return torrents;
    }
    else {
        return {};
    }
}

} } } }
