#include "SwitchSource.hpp"
#include "OpenBaySource.hpp"
#include "YifySource.hpp"
#include "KickassSource.hpp"
#include "Settings.hpp"
#include <boost/log/trivial.hpp>
#include <boost/range/adaptor/map.hpp>
#include "../common/Utils.hpp"

namespace downow {
namespace search {

SwitchSource::SwitchSource(QString type, QObject *parent) :
    base_type(parent) {

    _pnam = new QNetworkAccessManager(this);

    auto openBaySourceFactory = [this] {
        return new OpenBaySource(_pnam, this);
    };

    auto yifySourceFactory = [this] {
        return new YifySource(_pnam, this);
    };

    auto kickassSourceFactory = [this] {
        return new KickassSource(_pnam, this);
    };

    _typeToSource = {
        { OpenBaySource::getSourceType().toStdString(), openBaySourceFactory },
        { YifySource::getSourceType().toStdString(), yifySourceFactory },
        { KickassSource::getSourceType().toStdString(), kickassSourceFactory }
    };

    setType(type);
}

SwitchSource::SwitchSource(QObject * parent) :
    SwitchSource(YifySource::getSourceType(), parent) {}

void SwitchSource::setQuery(QString const& query) {
    _currentSource->setQuery(query);
}

QString SwitchSource::getQuery() const {
    return _currentSource->getQuery();
}

QString SwitchSource::getLastLoadedQuery() const {
    return _currentSource->getLastLoadedQuery();
}

bool SwitchSource::isLoading() const {
    return _currentSource->isLoading();
}

Source::ResultsRandomAccessRange SwitchSource::getResults() const {
    return _currentSource->getResults();
}

Source::Error SwitchSource::getError() const {
    return _currentSource->getError();
}

QString SwitchSource::getType() const {
    if (_currentSource) {
        return _currentSource->getType();
    }
    else {
        return QString::null;
    }
}

void SwitchSource::setType(QString type) {
    if (getType() == type) {
        return;
    }

    if (_currentSource) {
        _currentSource->disconnect(this);
    }

    auto iter = _typeToSource.find(type.toStdString());
    if (iter != _typeToSource.end()) {
        std::function<Source*()> const& factory = iter->second;
        _currentSource.reset(factory());
        onSourceChanged();
    }
    else {
        BOOST_THROW_EXCEPTION(std::runtime_error("No search source for type '" + type.toStdString() + "'."));
    }
}

void SwitchSource::onSourceChanged() {
    connect(_currentSource.get(), &Source::queryChanged, this, &Source::queryChanged);
    connect(_currentSource.get(), &Source::resultsWillChange, this, &Source::resultsWillChange);
    connect(_currentSource.get(), &Source::resultsChanged, this, &Source::resultsChanged);
    connect(_currentSource.get(), &Source::isLoadingChanged, this, &Source::isLoadingChanged);
    connect(_currentSource.get(), &Source::errorChanged, this, &Source::errorChanged);
    connect(_currentSource.get(), &Source::lastLoadedQueryChanged, this, &Source::lastLoadedQueryChanged);

    _currentSource->setQuery(getQuery());

    Q_EMIT typeChanged();
}

} }
