#include "KickassSource.hpp"

#include "KickassTorrentsScraper.hpp"

#include "../config.hpp.in"
#include "../common/Utils.hpp"

#include <qt5/QtConcurrent/QtConcurrent>
//#include <QNetworkAccessManager>
#include <boost/log/trivial.hpp>

namespace downow {
namespace search {

using namespace kickass::detail;

KickassSource::KickassSource(QNetworkAccessManager * pnam, QObject * parent) :
    base_type(pnam, parent) {}

QString KickassSource::getSourceType() {
    return "KickassTorrents";
}

void KickassSource::parse(QByteArray replyData) {
    QtConcurrent::run([this, replyData] {
        boost::optional<std::vector<KickassTorrent>> torrentStructs = scrape(replyData);

        std::vector<std::unique_ptr<Result>> results;
        if (torrentStructs) {
            BOOST_LOG_TRIVIAL(debug) << "KickassSource: found " << torrentStructs->size() << " torrents";
            for (KickassTorrent const& t : *torrentStructs) {
                auto result = std::make_unique<Result>();
                result->moveToThread(thread());
                result->setParent(this);
                result->setName(QString::fromStdString(t.name));
                result->setMagnetLink(QUrl(QString::fromStdString(t.magnetUrl)));

                int exponent = (int)t.size.unit;
                result->setSizeBytes(t.size.value * pow(1024, exponent));
                result->setSeeders(t.seeders);
                results.push_back(std::move(result));
            }
        }
        else {
            BOOST_LOG_TRIVIAL(debug) << "KickassSource: Couldn't parse data";
            Q_EMIT errorWhileContactingServer(Error::Parsing);
        }

        Q_EMIT doneParsingResults(std::make_shared<SharedResultsList::element_type>(std::move(results)));
    });
}

QUrl KickassSource::createUrlForQuery(QString query) {
    QUrl url;
    url.setScheme("https");
    url.setHost(getHost());
    url.setPort(443);
    url.setPath("/usearch/" + query + '/');

    QUrlQuery urlQuery;
    urlQuery.addQueryItem("field", "seeders");
    urlQuery.addQueryItem("sorder", "desc");
    url.setQuery(urlQuery);

    return url;
}

QString KickassSource::getHost() {
    return DOWNOW_SEARCH_KICKASS_TORRENTS_HOST;
}

QString KickassSource::getQueryFromUrl(QUrl const& url) {
    QStringList components = url.path().split('/');
    if (components.size() >= 2) {
        return components[components.size() - 2];
    }
    else {
        return QString::null;
    }
}

} }
