#include "SearchResultsListModel.hpp"

#include "SwitchSource.hpp"
#include "MockSource.hpp"

namespace downow {
namespace search {

SearchResultsListModel::SearchResultsListModel() :
    _source { new SwitchSource() } {

    connect(_source.data(), &Source::resultsChanged, this, [this] {
        onSourceResultsChanged();
    });
    connect(_source.data(), &Source::resultsWillChange, this, [this] {
        onSourceResultsWillChange();
    });
    connect(_source.data(), &Source::isLoadingChanged, this, &SearchResultsListModel::isLoadingChanged);
    connect(_source.data(), &Source::errorChanged, this, &SearchResultsListModel::errorChanged);
    connect(_source.data(), &Source::typeChanged, this, &SearchResultsListModel::typeChanged);
    connect(_source.data(), &Source::queryChanged, this, &SearchResultsListModel::queryChanged);
    connect(_source.data(), &Source::lastLoadedQueryChanged, this, &SearchResultsListModel::lastLoadedQueryChanged);
}

SearchResultsListModel::~SearchResultsListModel() {}

QString SearchResultsListModel::getQuery() const {
    return _source->getQuery();
}

bool SearchResultsListModel::isLoading() const {
    return _source->isLoading();
}

void SearchResultsListModel::setQuery(QString query) {
    _source->setQuery(query);
}

QString SearchResultsListModel::getLastLoadedQuery() const {
    return _source->getLastLoadedQuery();
}

Source *SearchResultsListModel::getSource() const {
    return _source.data();
}

QString SearchResultsListModel::getType() const {
    return _source->getType();
}

void SearchResultsListModel::setType(QString type) {
    _source->setType(type);
}

QVariant SearchResultsListModel::data(const QModelIndex &index, int role) const {
    switch((Role)role) {
    case Role::Result:
        return QVariant::fromValue(getResults()[index.row()]);

    default:
        return QVariant {};
    }
}

QHash<int, QByteArray> SearchResultsListModel::roleNames() const {
    return {
        {(int)Role::Result, "result"}
    };
}

void SearchResultsListModel::onSourceResultsWillChange() {
    beginResetModel();
}

void SearchResultsListModel::onSourceResultsChanged() {
    endResetModel();
    Q_EMIT countChanged();
}

Source::ResultsRandomAccessRange
SearchResultsListModel::getResults() const {
    return _source->getResults();
}

} }
