#ifndef DOWNOW_MODEL_SEARCH_OPEN_BAY_SCRAPER
#define DOWNOW_MODEL_SEARCH_OPEN_BAY_SCRAPER

#include <string>
#include <vector>
#include <boost/optional.hpp>
#include <qt5/QtCore/QByteArray>

namespace downow {
namespace search {
namespace openbay {
namespace detail {

enum class ByteUnit {
    B = 0, KB, MB, GB, TB
};

struct Size {
    ByteUnit unit;
    float value;
};

struct TitleRow {
    std::string name;
    std::string magnetLink;
};

struct OpenBayTorrent {
    TitleRow titleRow;
    Size size;
    int seeders;
};

boost::optional<std::vector<OpenBayTorrent>>
scrape(QByteArray html);

} } } }

#endif
