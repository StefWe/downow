#ifndef DOWNOW_MODEL_SEARCH_RESULT
#define DOWNOW_MODEL_SEARCH_RESULT

#include <QString>
#include <QUrl>
#include <QObject>

#include <boost/integer.hpp>

namespace downow {
namespace search {

using Bytes = boost::uint64_t;

struct Result : QObject {
private:
    Q_OBJECT
    Q_PROPERTY(QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY(QUrl magnetLink READ getMagnetLink NOTIFY magnetLinkChanged)
    Q_PROPERTY(unsigned int sizeKB READ getSizeKB NOTIFY sizeKBChanged)
    Q_PROPERTY(QString category READ getCategory NOTIFY categoryChanged)
    Q_PROPERTY(int seeders READ getSeeders NOTIFY seedersChanged)

public:
    Result(QObject * parent = nullptr) : QObject(parent) {
        connect(this, &Result::sizeBytesChanged, this, &Result::sizeKBChanged);
    }

    QString getName() const {
        return _name;
    }

    QUrl getMagnetLink() const {
        return _magnetLink;
    }

    Bytes getSizeBytes() const {
        return _sizeBytes;
    }

    unsigned int getSizeKB() const {
        return getSizeBytes() / 1024;
    }

    QString getCategory() const {
        return _category;
    }

    int getSeeders() const {
        return _seeders;
    }

    void setName(QString const& name) {
        if (_name == name) {
            return;
        }

        _name = name;
        nameChanged();
    }

    void setMagnetLink(QUrl const& magnetLink) {
        if (_magnetLink == magnetLink) {
            return;
        }

        _magnetLink = magnetLink;
        magnetLinkChanged();
    }

    void setSizeBytes(Bytes sizeBytes) {
        if (_sizeBytes == sizeBytes) {
            return;
        }

        _sizeBytes = sizeBytes;
        sizeBytesChanged();
    }

    void setCategory(QString const& category) {
        if (_category == category) {
            return;
        }

        _category = category;
        categoryChanged();
    }

    void setSeeders(int seeders) {
        if (_seeders == seeders) {
            return;
        }

        _seeders = seeders;
        seedersChanged();
    }

Q_SIGNALS:
    void nameChanged();
    void magnetLinkChanged();
    void sizeBytesChanged();
    void sizeKBChanged();
    void categoryChanged();
    void seedersChanged();

private:
    QString _name;
    QUrl _magnetLink;
    Bytes _sizeBytes = 0;
    QString _category;
    int _seeders = 0;
};

} }

Q_DECLARE_METATYPE(downow::search::Result *)

#endif
