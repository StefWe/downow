#ifndef DOWNOW_SEARCH_TIDY
#define DOWNOW_SEARCH_TIDY

#include <QtCore/QByteArray>
#include <QtXml/QDomDocument>
#include <boost/optional.hpp>

namespace downow { namespace search {

std::string tidyHtml(char const* inputHtml, boost::optional<std::string &> errors = {});
QDomDocument parseHtml(char const* inputHtml);

} }

#endif
