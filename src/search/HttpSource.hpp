#ifndef DOWNOW_MODEL_SEARCH_HTTP_SOURCE
#define DOWNOW_MODEL_SEARCH_HTTP_SOURCE

#include "Source.hpp"
#include "RequestManager.hpp"
#include "Result.hpp"

#include <memory>
#include <vector>

#include <QtCore/QString>
#include <QtCore/QUrl>

struct QNetworkAccessManager;
struct QNetworkReply;

namespace downow {
namespace search {

struct HttpSource : Source {
private:
    Q_OBJECT

    using base_type = Source;

public:
    using ResultsList = std::vector<std::unique_ptr<Result>>;
    using SharedResultsList = std::shared_ptr<ResultsList>;

    HttpSource(QNetworkAccessManager * pnam = nullptr, QObject * parent = nullptr);

    void setQuery(const QString &) override;
    QString getQuery() const override;
    QString getLastLoadedQuery() const override;
    bool isLoading() const override;
    ResultsRandomAccessRange getResults() const override;
    Error getError() const override;

Q_SIGNALS:
    void errorWhileContactingServer(Error);
    void doneParsingResults(SharedResultsList);

protected:
    virtual void parse(QByteArray) = 0;
    virtual QUrl createUrlForQuery(QString query) = 0;
    virtual QString getHost() = 0;
    virtual QString getQueryFromUrl(QUrl const&) = 0;

    void setError(Error);

private Q_SLOTS:
    void onReplyFinished(QNetworkReply * reply);
    void onDoneParsingResults(SharedResultsList);

private:
    void
    search(QString const& query);

    void setLastLoadedQuery(QString);

private:
    QNetworkAccessManager * _pnam = nullptr;
    ResultsList _results;
    unsigned int _loadingCount = 0;
    RequestManager _requestManager;
    QString _query;
    QString _lastLoadedQuery;
    Error _error;
};

} }

#endif
