#ifndef DOWNOW_MODEL_SEARCH_KICKASS_TORRENTS_SCRAPER
#define DOWNOW_MODEL_SEARCH_KICKASS_TORRENTS_SCRAPER

#include <string>
#include <vector>
#include <boost/optional.hpp>
#include <qt5/QtCore/QByteArray>

namespace downow {
namespace search {
namespace kickass {
namespace detail {

enum class ByteUnit {
    B = 0, KB, MB, GB, TB, Unknown = -1
};

struct Size {
    float value;
    ByteUnit unit;
};

struct KickassTorrent {
    std::string name;
    std::string magnetUrl;
    Size size;
    unsigned int seeders;
};

boost::optional<std::vector<KickassTorrent>>
scrape(QByteArray html);

} } } }

#endif
