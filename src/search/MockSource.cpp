#include "MockSource.hpp"
#include "Result.hpp"

#include <boost/range/adaptor/transformed.hpp>

namespace downow {
namespace search {

MockSource::MockSource(QObject *parent) :
    Source(parent) {
    {
        std::unique_ptr<Result> result { new Result() };
        result->setName("Game of Thrones");
        _results.push_back(std::move(result));
    }

    {
        std::unique_ptr<Result> result { new Result() };
        result->setName("Song of Ice and Fire");
        _results.push_back(std::move(result));
    }
}

Source::ResultsRandomAccessRange
MockSource::getResults() const {
    struct transformer {
        Result * operator() (std::unique_ptr<Result> const& result) const {
            return result.get();
        }
    };

    return _results | boost::adaptors::transformed(transformer {});
}

} }
