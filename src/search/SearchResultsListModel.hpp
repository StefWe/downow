#ifndef DOWNOW_MODEL_SEARCH_SEARCH_RESULTS_LIST_MODEL
#define DOWNOW_MODEL_SEARCH_SEARCH_RESULTS_LIST_MODEL

#include <QString>
#include <QAbstractListModel>
#include <boost/range/any_range.hpp>
#include <qt5/QtCore/QScopedPointer>

#include "Result.hpp"
#include "Source.hpp"

namespace downow {
namespace search {

struct SwitchSource;

struct SearchResultsListModel : QAbstractListModel {
private:
    Q_OBJECT
    Q_PROPERTY(QString query READ getQuery WRITE setQuery NOTIFY queryChanged)
    Q_PROPERTY(QString lastLoadedQuery READ getLastLoadedQuery NOTIFY lastLoadedQueryChanged)
    Q_PROPERTY(bool isLoading READ isLoading NOTIFY isLoadingChanged)
    Q_PROPERTY(downow::search::Source * source READ getSource NOTIFY sourceChanged)
    Q_PROPERTY(int count READ getCount NOTIFY countChanged)
    Q_PROPERTY(QString type READ getType WRITE setType NOTIFY typeChanged)

public:
    SearchResultsListModel();
    ~SearchResultsListModel();

    QString getQuery() const;
    bool isLoading() const;

    void setQuery(QString query);

    QString getLastLoadedQuery() const;

    Source * getSource() const;

    int getCount() const {
        return getResults().size();
    }

    QString getType() const;
    void setType(QString type);

    enum class Role {
        Result
    };

    int rowCount(QModelIndex const& parent) const override {
        return getCount();
    }
    QVariant data(QModelIndex const& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void queryChanged();
    void lastLoadedQueryChanged();
    void isLoadingChanged();
    void errorChanged();
    void sourceChanged();
    void countChanged();
    void typeChanged();

private:
    void onQueryChanged();
    void onSourceResultsWillChange();
    void onSourceResultsChanged();

    Source::ResultsRandomAccessRange getResults() const;

private:
    QScopedPointer<SwitchSource> _source;
};

} }

#endif
