#ifndef DOWNOW_MODEL_SEARCH_KICKASS_SOURCE
#define DOWNOW_MODEL_SEARCH_KICKASS_SOURCE

#include "HttpSource.hpp"

namespace downow {
namespace search {

struct KickassSource : HttpSource {
private:
    Q_OBJECT

public:
    using base_type = HttpSource;
    KickassSource(QNetworkAccessManager * pnam = nullptr, QObject * parent = nullptr);

    static QString getSourceType();
    QString getType() const override {
        return getSourceType();
    }

protected:
    void parse(QByteArray) override;
    QUrl createUrlForQuery(QString query) override;
    QString getHost() override;
    QString getQueryFromUrl(const QUrl &) override;
};

} }

#endif // KICKASSSOURCE_HPP
