#ifndef DOWNOW_MODEL_SEARCH_MOCK_SOURCE
#define DOWNOW_MODEL_SEARCH_MOCK_SOURCE

#include "Source.hpp"
#include <memory>

namespace downow {
namespace search {

struct MockSource : Source {
private:
    Q_OBJECT

public:
    MockSource(QObject * parent = nullptr);

    void
    search(const QString &searchTerm) {}

    ResultsRandomAccessRange
    getResults() const;

private:
    std::vector<std::unique_ptr<Result>> _results;
};

} }

#endif
