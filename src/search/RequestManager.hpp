#ifndef DOWNOW_MODEL_SEARCH_REQUEST_MANAGER
#define DOWNOW_MODEL_SEARCH_REQUEST_MANAGER

#include <functional>
#include <chrono>

#include <qt5/QtCore/QObject>
#include <qt5/QtCore/QFuture>
#include <qt5/QtCore/QTimer>

namespace downow {
namespace search {

struct RequestManager : QObject {
private:
    Q_OBJECT
    using Clock = std::chrono::steady_clock;

public:
    RequestManager(QObject * parent = nullptr);
    void request(std::function<void()> request);
    void cancel();

private:
    Q_SLOT void timerTimeout();

private:
    std::function<void()> _lastRequest;
    QTimer _timer;
};

} }

#endif
