
#include "../config.hpp.in"

#if DOWNOW_SEARCH_ENABLE_PIRATE_BAY

#include "PirateBayScraper.hpp"

#define BOOST_SPIRIT_UNICODE // We'll use unicode (UTF8) all throughout

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted/struct.hpp>

#include <boost/log/trivial.hpp>
#include <sstream>

BOOST_FUSION_ADAPT_STRUCT(
    downow::search::tpb::detail::DetDesc,
    (float, size)
    (downow::search::tpb::detail::ByteUnit, unit)
)

BOOST_FUSION_ADAPT_STRUCT(
    downow::search::tpb::detail::PirateBayTorrent,
    (std::string, category)
    (std::string, name)
    (std::string, magnetLink)
    (downow::search::tpb::detail::DetDesc, desc)
    (int, seeders)
    (int, leechers)
)

namespace downow {
namespace search {
namespace tpb {
namespace detail {

namespace qi = boost::spirit::qi;

struct SizeUnit_ : qi::symbols<char, ByteUnit> {
    SizeUnit_()
    {
        add
            ("B",   ByteUnit::B)
            ("KiB", ByteUnit::KB)
            ("MiB", ByteUnit::MB)
            ("GiB", ByteUnit::GB)
            ("TiB", ByteUnit::TB)
        ;
    }
} SizeUnit;

namespace charset = qi::unicode;

template <typename Iterator>
struct TorrentsGrammer : qi::grammar<Iterator, std::vector<PirateBayTorrent>(), charset::space_type> {
    TorrentsGrammer() : TorrentsGrammer::base_type(start, "torrents") {
        using namespace qi;
        using charset::space;
        using namespace qi::labels;

        /*
        <tr>
            <td class="vertTh">
                <center>
                    <a href="/browse/200" title="More from this category">Video</a><br />
                    (<a href="/browse/599" title="More from this category">Other</a>)
                </center>
            </td>
            <td>
                <div class="detName">
                    <a href="some-url" class="detLink" title="some-title">some name</a>
                </div>
                <a href="magnet link" title="Download this torrent using magnet">
                    <img src="/static/img/icon-magnet.gif" alt="Magnet link" />
                </a>
                <a href="/user/FluxXxu">
                    <img src="/static/img/vip.gif" alt="VIP" title="VIP" style="width:11px;" border='0' />
                </a>
                <img src="/static/img/11x11p.png" />
                <font class="detDesc">Uploaded Y-day&nbsp;18:20, Size 1.75&nbsp;GiB, ULed by <a class="detDesc" href="/user/FluxXxu/" title="Browse FluxXxu">FluxXxu</a></font>
            </td>
            <td align="right">342</td>
            <td align="right">381</td>
        </tr>
        */

        start %=
            omit[
                *(char_ - tpbIndicator) > tpbIndicator
            ]
            > -(
                omit[
                    *(char_ - searchResultTableIndicator) >> searchResultTableIndicator
                 >> lit("<thead") >> *(char_ - lit("</thead>")) >> lit("</thead>")
                 >> *(char_ - lit("<tr>"))
                ]
             >> *torrent
             >> omit[*char_]
            );

        torrent %=
            lit("<tr>")
          > vertTh
          > lit("<td>")
          > detName
          > magnetLink
          > omit[
                *(char_ - detDescIndicator)
            ]
          > detDesc
          > lit("</td>")
          > lit("<td") > lit("align=\"right\"") > lit(">") > int_ > lit("</td>")
          > lit("<td") > lit("align=\"right\"") > lit(">") > int_ > lit("</td>")
          > skipUntilEndOfTorrent;

        vertTh =
            lit("<td") >> lit("class=\"vertTh\"") >> lit(">")
         >> lit("<center>")
         >> elementText
         >> *(char_ - lit("</td>")) >> lit("</td>");

        detName %=
            lit("<div") >> lit("class=\"detName\"") >> lit(">")
         >> elementText
         >> lit("</div>");

        magnetLink %=
            omit[
                lexeme[lit("<a") >> space] >> lit("href=\"")
            ]
         >> lexeme[
                +(char_ - lit('"'))
            ]
         >> lit('"')
         >> omit[
                *(char_ - lit("</a>")) >> lit("</a>")
            ];

        detDescIndicator =
            lit("<font") >> lit("class=\"detDesc\"") >> lit(">");

        tpbIndicator =
            no_case[
                lit("<title>") > "The" > "Pirate" > "Bay" >
                *(char_ - lit("</title>")) > lit("</title>")
            ];

        searchResultTableIndicator =
            lit("<table") > lit("id=\"searchResult\"") > lit(">");

        elementText %=
            omit[
                lexeme[
                    lit("<a")
                 >> space
                ]
             >> *(char_ - (lit(">") >> +(char_ - lit("</a>")) >> lit("</a>")))
            ]
         >> lit(">")
         >> lexeme[
                +(char_ - lit("</a>"))
            ]
         >> lit("</a>");

        detDesc %=
            omit[
                detDescIndicator
            >> *(char_ - lit("Size")) >> lit("Size")
            ]
         >> float_
         >> omit[
                *(char_ - SizeUnit)
            ]
         >> SizeUnit
         >> *(char_ - lit("</font>")) >> lit("</font>");

        skipUntilEndOfTorrent =
            *(char_ - lit("</tr>")) >> lit("</tr>");

#define ASSIGN_NAME(RULE) RULE.name(#RULE)
        ASSIGN_NAME(start);
        ASSIGN_NAME(tpbIndicator);
        ASSIGN_NAME(torrent);
        ASSIGN_NAME(detName);
        ASSIGN_NAME(magnetLink);
        ASSIGN_NAME(detDesc);
        ASSIGN_NAME(vertTh);
#undef ASSIGN_NAME

        qi::on_error<qi::fail>(start, [] (auto params, auto context, auto noidea) {
            using namespace boost::fusion;
            std::ostringstream s;
            s
                    << "Error! Expecting "
                    << at_c<3>(params)
                    << " here: \""
                    << std::string(at_c<2>(params), at_c<1>(params))
                    << "\"";
            BOOST_LOG_TRIVIAL(debug) << s.str();
        });
    }

    template <typename T, typename Skipper=charset::space_type>
    using rule = qi::rule<Iterator, T, Skipper>;

    rule<std::vector<PirateBayTorrent>()> start;
    rule<PirateBayTorrent()> torrent;
    rule<std::string()> detName;
    rule<std::string()> magnetLink;
    rule<DetDesc()> detDesc;
    rule<std::string()> vertTh;

    rule<void()> tpbIndicator;
    rule<void()> searchResultTableIndicator;
    rule<void()> detDescIndicator;
    rule<void()> skipUntilEndOfTorrent;
    rule<std::string()> elementText;
};

boost::optional<std::vector<PirateBayTorrent>>
scrape(QByteArray html) {
    std::vector<PirateBayTorrent> torrents;
    bool success =
            qi::phrase_parse(
                html.begin(),
                html.end(),
                TorrentsGrammer<QByteArray::iterator>(),
                charset::space,
                torrents);
    if (success) {
        return torrents;
    }
    else {
        return {};
    }
}

} } } }

#endif
