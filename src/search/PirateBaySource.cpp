
#include "../config.hpp.in"

#if DOWNOW_SEARCH_ENABLE_PIRATE_BAY

#include "PirateBaySource.hpp"

#include "Result.hpp"
#include "PirateBayScraper.hpp"

#include "Settings.hpp"

#include <qt5/QtNetwork/QNetworkRequest>
#include <qt5/QtNetwork/QNetworkAccessManager>
#include <qt5/QtNetwork/QNetworkReply>
#include <qt5/QtConcurrent/QtConcurrent>
#include <qt5/QtCore/QCoreApplication>
#include <qt5/QtCore/QFuture>
#include <qt5/QtCore/QSettings>

#include <boost/log/trivial.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "../common/Utils.hpp"

namespace downow { namespace search {

static QStringList const availableDomains {
    "thepiratebay.se",
    "pirateproxy.in",
    "thebootlegbay.com",
    "thepiratebay.si",
    "bayproxy.uk",
    "tpb.eduardofortes.com.ar",
    "piratebayguru.com"
};

QStringList PirateBaySource::getAvailableDomains() {
    return availableDomains;
}

PirateBaySource::PirateBaySource(QObject * parent) :
    base_type(parent) {

    int defaultHostIndex = Settings::instance().getTpbProxyDomainIndex();
    _proxyDomainsIterator = availableDomains.begin() + defaultHostIndex;

    BOOST_LOG_TRIVIAL(debug) << "PirateBaySource: Host: " << getProxyDomain();

    _pnam = new QNetworkAccessManager(this);
    qRegisterMetaType<ResultsList>("ResultsList");
    connect(_pnam, &QNetworkAccessManager::finished, this, &PirateBaySource::onReplyFinished);
    connect(this, &PirateBaySource::doneParsingResults, this, &PirateBaySource::onDoneParsingResults);
    connect(this, &PirateBaySource::queryChanged, [this] {
        if (_query.isEmpty()) {
            Q_EMIT doneParsingResults({});
            _requestManager.cancel();
        }
        else {
            _requestManager.request([this, query=_query] {
                search(query);
            });
        }
    });
    connect(this, &PirateBaySource::proxyDomainChanged, [this] {
        if (_blockNextSearchOnProxyChanged) {
            _blockNextSearchOnProxyChanged = false;
            return;
        }

        if (!getProxyDomain().isEmpty() && !_query.isEmpty()) {
            search(_query);
        }
    });
    connect(this, &PirateBaySource::foundWorkingDomainProxy, [this] (QString domainProxy) {
        Settings & settings = Settings::instance();
        if (!settings.tpbProxyDomainWasSetExplicity()) {
            settings.setTpbProxyDomainIndex(availableDomains.indexOf(domainProxy));
        }
    });
    connect(&Settings::instance(), &Settings::tpbProxyDomainIndexChanged, this, [this] {
         _proxyDomainsIterator = availableDomains.begin() + Settings::instance().getTpbProxyDomainIndex();
         Q_EMIT proxyDomainChanged();
    });
    connect(this, &PirateBaySource::errorWhileContactingServer, this, &PirateBaySource::onErrorWhileContactingServer);
}

void PirateBaySource::setQuery(QString const& query) {
    if (_query == query) {
        return;
    }

    _query = query;
    Q_EMIT queryChanged();
}

QString PirateBaySource::getQuery() const {
    return _query;
}

void
PirateBaySource::onDoneParsingResults(ResultsList results) {
    resultsWillChange();
    if (results) {
        _results = std::move(*results);
    }
    else {
        _results.clear();
    }
    resultsChanged();
}

QString PirateBaySource::getProxyDomain() const {
    if (_proxyDomainsIterator != availableDomains.end()) {
        return *_proxyDomainsIterator;
    }
    else {
        return {};
    }
}

bool
PirateBaySource::tryNextProxyDomain() {
    if (_proxyDomainsIterator != availableDomains.end()) {
        bool nextIsEnd = (_proxyDomainsIterator + 1) == availableDomains.end();
        if (nextIsEnd) {
            _proxyDomainsIterator = availableDomains.begin();
            BOOST_LOG_TRIVIAL(debug) << "PirateBaySource: Reached end of domain proxy options";
        }
        else {
            _proxyDomainsIterator++;
        }

        bool returnedToBeginning = nextIsEnd;
        _blockNextSearchOnProxyChanged = returnedToBeginning;
        Q_EMIT proxyDomainChanged();

        return !returnedToBeginning;
    }
    else {
        return false;
    }
}

void
PirateBaySource::search(QString const& query) {
    QString urlString = "http://";
    urlString.append(getProxyDomain());
    urlString.append("/search/");
    urlString.append(query);

    // sort by seeders
    urlString.append("/0/7/0");

    QNetworkRequest request {
        QUrl {urlString}
    };

    _pnam->get(request);

    _loadingCount++;
    if (_loadingCount == 1) {
        isLoadingChanged();
    }
}

Source::ResultsRandomAccessRange
PirateBaySource::getResults() const {
    struct transformer {
        Result * operator() (std::unique_ptr<Result> const& result) const {
            return result.get();
        }
    };

    return _results | boost::adaptors::transformed(transformer {});
}

void PirateBaySource::onReplyFinished(QNetworkReply * reply) {
    _loadingCount--;
    if (_loadingCount == 0) {
        isLoadingChanged();
    }

    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    BOOST_LOG_TRIVIAL(debug) << "PirateBaySource: Received response: " << statusCode << ", url: " << reply->url().toString();

    if (statusCode == 200) {
        QtConcurrent::run([this, reply] {
            QByteArray replyData = reply->readAll();
            reply->close();

            using namespace tpb::detail;

            boost::optional<std::vector<PirateBayTorrent>> torrentStructs = scrape(replyData);

            std::vector<std::unique_ptr<Result>> results;
            if (torrentStructs) {
                BOOST_LOG_TRIVIAL(debug) << "PirateBaySource: found " << torrentStructs->size() << " torrents";
                for (PirateBayTorrent const& t : *torrentStructs) {
                    auto result = std::make_unique<Result>();
                    result->moveToThread(thread());
                    result->setParent(this);
                    result->setName(QString::fromStdString(t.name));
                    result->setMagnetLink(QUrl(QString::fromStdString(t.magnetLink)));

                    int exponent = (int)t.desc.unit;
                    result->setSizeBytes(t.desc.size * pow(1024, exponent));
                    result->setCategory(QString::fromStdString(t.category));
                    result->setSeeders(t.seeders);
                    results.push_back(std::move(result));
                }

                Q_EMIT foundWorkingDomainProxy(reply->url().host());
            }
            else {
                BOOST_LOG_TRIVIAL(debug) << "PirateBaySource: Couldn't parse data from url: " << reply->url().toString();
                Q_EMIT errorWhileContactingServer(Error::Parsing);
            }

            Q_EMIT doneParsingResults(std::make_shared<std::vector<std::unique_ptr<Result>>>(std::move(results)));
        });
    }
    else {
        reply->close();
        if (reply->error() != QNetworkReply::NoError) {
            BOOST_LOG_TRIVIAL(error) << "Error while contacting thepiratebay. Url: " << reply->url().toString() << ", error: " << reply->errorString();
        }

        Q_EMIT errorWhileContactingServer(Error::Internet);
    }
}

void PirateBaySource::onErrorWhileContactingServer(Error error) {
    bool keepsTrying = false;
    if (!Settings::instance().tpbProxyDomainWasSetExplicity()) {
        keepsTrying = tryNextProxyDomain();
    }

    if (!keepsTrying) {
        Q_EMIT networkErrorOccured();
    }
}

} }

#endif
