#include "HttpSource.hpp"

#include "Result.hpp"

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

#include <boost/range/adaptor/transformed.hpp>
#include <boost/log/trivial.hpp>
#include "../common/Utils.hpp"

namespace downow {
namespace search {

static bool hasRegisteredResultsList = false;

HttpSource::HttpSource(QNetworkAccessManager * pnam, QObject * parent) :
    base_type(parent),
    _pnam(pnam),
    _error(Error::None) {
    if (!_pnam) {
        _pnam = new QNetworkAccessManager(this);
    }

    // not thread safe, I assume that HttpSource objects will only be created on the main thread
    if (!hasRegisteredResultsList) {
        qRegisterMetaType<SharedResultsList>("SharedResultsList");
        hasRegisteredResultsList = true;
    }

    connect(_pnam, &QNetworkAccessManager::finished, this, &HttpSource::onReplyFinished);
    connect(this, &HttpSource::doneParsingResults, this, &HttpSource::onDoneParsingResults);
    connect(this, &HttpSource::queryChanged, [this] {
        if (_query.isEmpty()) {
            setLastLoadedQuery("");
            Q_EMIT doneParsingResults({});
            _requestManager.cancel();
        }
        else {
            _requestManager.request([this, query=_query] {
                search(query);
            });
        }
    });
}

void
HttpSource::setQuery(QString const& query) {
    if (_query == query) {
        return;
    }

    _query = query;
    Q_EMIT queryChanged();
}

QString
HttpSource::getQuery() const {
    return _query;
}

QString HttpSource::getLastLoadedQuery() const {
    return _lastLoadedQuery;
}

bool HttpSource::isLoading() const {
    return _loadingCount > 0;
}

void
HttpSource::onDoneParsingResults(SharedResultsList results) {
    Q_EMIT resultsWillChange();
    if (results) {
        _results = std::move(*results);
    }
    else {
        _results.clear();
    }

    Q_EMIT resultsChanged();
}

void
HttpSource::search(QString const& query) {
    QUrl url = createUrlForQuery(query);

    BOOST_LOG_TRIVIAL(debug) << "Getting URL: " << url.toString();

    QNetworkRequest request { url };

    _pnam->get(request);

    _loadingCount++;
    if (_loadingCount == 1) {
        isLoadingChanged();
    }
}

void HttpSource::setLastLoadedQuery(QString lastLoadedQuery) {
    if (_lastLoadedQuery == lastLoadedQuery) {
        return;
    }

    _lastLoadedQuery = lastLoadedQuery;
    Q_EMIT lastLoadedQueryChanged();
}

Source::ResultsRandomAccessRange
HttpSource::getResults() const {
    struct transformer {
        Result * operator() (std::unique_ptr<Result> const& result) const {
            return result.get();
        }
    };

    return _results | boost::adaptors::transformed(transformer {});
}

Source::Error HttpSource::getError() const {
    return _error;
}

void HttpSource::setError(Source::Error error) {
    if (_error == error) {
        return;
    }

    _error = error;
    Q_EMIT errorChanged();
}

void
HttpSource::onReplyFinished(QNetworkReply * reply) {
    // in case the QNetworkAccessManager is shared, check that the reply is meant for this source
    if (reply->url().host() != getHost()) {
        return;
    }

    _loadingCount--;
    if (_loadingCount == 0) {
        isLoadingChanged();
    }

    setLastLoadedQuery(getQueryFromUrl(reply->url()));

    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    BOOST_LOG_TRIVIAL(debug) << "HttpSource: Received response: " << statusCode << ", url: " << reply->url().toString();

    if (statusCode == 200) {
        setError(Error::None);
        parse(reply->readAll());
    }
    else {
        reply->close();
        if (reply->error() != QNetworkReply::NoError) {
            BOOST_LOG_TRIVIAL(error) << "Error while contacting thepiratebay. Url: " << reply->url().toString() << ", error: " << reply->errorString();
        }

        setError(Error::Internet);
        Q_EMIT doneParsingResults(std::make_shared<ResultsList>(ResultsList {}));
    }
}

} }
