#ifndef DOWNOW_MODEL_SEARCH_PIRATE_BAY_SOURCE
#define DOWNOW_MODEL_SEARCH_PIRATE_BAY_SOURCE

#include <downow/config.hpp>

#if DOWNOW_SEARCH_ENABLE_PIRATE_BAY

#include <qt5/QtNetwork/QNetworkAccessManager>
#include <qt5/QtNetwork/QNetworkReply>
#include <QList>
#include <QMetaType>
#include <qt5/QtQml/QQmlListProperty>

#include <memory>

#include "Source.hpp"
#include "RequestManager.hpp"

namespace downow {
namespace search {

using ResultsList = std::shared_ptr<std::vector<std::unique_ptr<Result>>>;

struct PirateBaySource : Source {
private:
    Q_OBJECT
    Q_PROPERTY(QString proxyDomain READ getProxyDomain NOTIFY proxyDomainChanged)

    using base_type = Source;

public:
    static QStringList getAvailableDomains();
    PirateBaySource(QObject * parent = nullptr);

    void
    setQuery(const QString &) override;

    QString
    getQuery() const override;

    ResultsRandomAccessRange
    getResults() const override;

    bool isLoading() const override {
        return _loadingCount > 0;
    }

    QString getProxyDomain() const;

    Q_SIGNAL void proxyDomainChanged();

private:
    enum class Error {
        Internet,
        Parsing
    };

    Q_SLOT void onReplyFinished(QNetworkReply * reply);
    Q_SLOT void onDoneParsingResults(ResultsList);
    Q_SLOT void onErrorWhileContactingServer(Error);
    Q_SIGNAL void errorWhileContactingServer(Error);
    Q_SIGNAL void foundWorkingDomainProxy(QString domainProxy);
    Q_SIGNAL void doneParsingResults(ResultsList);

    void
    search(QString const& query);

    /**
     * @return Whether or not it cycled back to the start
     */
    bool tryNextProxyDomain();

private:
    QNetworkAccessManager * _pnam;
    std::vector<std::unique_ptr<Result>> _results;
    int _loadingCount = 0;
    RequestManager _requestManager;

    QList<QString>::const_iterator _proxyDomainsIterator;
    QString _query;
    bool _blockNextSearchOnProxyChanged = false;
};

} }

#else
#   error Not in use until the pirate bay is back
#endif
#endif
