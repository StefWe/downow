#include "Settings.hpp"

#include <qt5/QtCore/QSettings>
#include <qt5/QtCore/QPointer>

#include <boost/log/trivial.hpp>
#include "../common/Utils.hpp"

namespace downow { namespace search {

static QPointer<Settings> _instance = nullptr;

Settings::Settings(std::shared_ptr<QSettings> settings) :
    _settings(settings) {
    if (_instance) {
        BOOST_THROW_EXCEPTION(InstanceAlreadyExistsException());
    }

    _instance = this;
}

Settings &Settings::instance() {
    if (!_instance) {
        BOOST_THROW_EXCEPTION(InstanceDoesntExistException());
    }

    return *_instance;
}

} }
