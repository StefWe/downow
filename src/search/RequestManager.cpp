#include "RequestManager.hpp"

namespace downow {
namespace search {

static std::chrono::seconds const durationBetweenRequests {1};

RequestManager::RequestManager(QObject *parent) : QObject(parent) {
    _timer.setInterval(std::chrono::milliseconds {durationBetweenRequests}.count());
    _timer.setSingleShot(true);
    connect(&_timer, &QTimer::timeout, this, &RequestManager::timerTimeout);
}

void RequestManager::request(std::function<void()> request) {
    _lastRequest = std::move(request);
    _timer.start();
}

void RequestManager::cancel() {
    _timer.stop();
}

void RequestManager::timerTimeout() {
    _lastRequest();
}

} }
