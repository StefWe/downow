#include "YifySource.hpp"

#include "../config.hpp.in"
#include "../common/Utils.hpp"

#include <QtCore/QUrlQuery>
#include <boost/log/trivial.hpp>

#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonValue>
#include <QtCore/QJsonArray>

namespace downow {
namespace search {

YifySource::YifySource(QNetworkAccessManager * pnam, QObject * parent) :
    base_type(pnam, parent) {}

QString YifySource::getSourceType() {
    return "Yify";
}

void YifySource::parse(QByteArray data) {
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(data, &parseError);

    if (parseError.error == QJsonParseError::NoError) {
        if (doc.object()["status"].toString() == "ok") {
            SharedResultsList results = std::make_shared<SharedResultsList::element_type>();
            QJsonArray movieListJson = doc.object()["data"].toObject()["movies"].toArray();
            for (QJsonValue const& movieJson : movieListJson) {
                QJsonObject movieObject = movieJson.toObject();

                for (QJsonValue const& torrentJson : movieObject["torrents"].toArray()) {
                    auto result = std::make_unique<VideoResult>();
                    result->setName(movieObject["title"].toString());
                    result->setImdbID(movieObject["imdb_code"].toString());
                    result->setCoverImageUrl(QUrl(movieObject["medium_cover_image"].toString()));

                    result->setGenres([&] {
                        QStringList genres;
                        for (auto const& genre : movieObject["genres"].toArray()) {
                            genres.append(genre.toString());
                        }
                        return genres;
                    }());

                    QJsonObject torrentObject = torrentJson.toObject();

                    result->setSeeders(torrentObject["seeds"].toInt());
                    result->setSizeBytes(torrentObject.toVariantMap()["size_bytes"].toULongLong());
                    result->setQuality(torrentObject["quality"].toString());

                    result->setMagnetLink([&] {
                        QUrl magnetUrl;
                        magnetUrl.setScheme("magnet");
                        {
                            QUrlQuery urlQuery;
                            urlQuery.addQueryItem("xt", "urn:btih:" + torrentObject["hash"].toString());
                            urlQuery.addQueryItem("dn", movieObject["title_long"].toString());

                            char const* trackers[] = {
                                "udp://open.demonii.com:1337",
                                "udp://tracker.istole.it:80",
                                "http://tracker.yify-torrents.com/announce",
                                "udp://tracker.publicbt.com:80",
                                "udp://tracker.openbittorrent.com:80",
                                "udp://tracker.coppersurfer.tk:6969",
                                "udp://exodus.desync.com:6969",
                                "http://exodus.desync.com:6969/announce",
                            };
                            for (auto tracker : trackers) {
                                urlQuery.addQueryItem("tr", tracker);
                            }

                            magnetUrl.setQuery(urlQuery);
                        }
                        return magnetUrl;
                    }());

                    results->push_back(std::move(result));
                }
            }

            Q_EMIT doneParsingResults(std::move(results));
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "YifySource: Couldn't parse json. Error: " << parseError.errorString();
            Q_EMIT errorWhileContactingServer(Error::Internet);
        }
    }
    else {
        BOOST_LOG_TRIVIAL(error) << "YifySource: Couldn't parse json. Error: " << parseError.errorString();
        Q_EMIT errorWhileContactingServer(Error::Parsing);
    }
}

QUrl YifySource::createUrlForQuery(QString query) {
    QUrl url;
    url.setScheme("https");
    url.setHost(getHost());
    url.setPath("/api/v2/list_movies.json");
    url.setPort(443);

    QUrlQuery urlQuery;
    urlQuery.addQueryItem("query_term", query);
    urlQuery.addQueryItem("sort_by", "seeds");
    urlQuery.addQueryItem("order_by", "desc");

    url.setQuery(urlQuery);

    return url;
}

QString YifySource::getHost() {
    return DOWNOW_SEARCH_YIFY_TORRENTS_HOST;
}

QString YifySource::getQueryFromUrl(QUrl const& url) {
    return QUrlQuery(url).queryItemValue("query_term");
}

} }
