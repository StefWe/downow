#ifndef DOWNOW_MODEL_SEARCH_SWITCH_SOURCE
#define DOWNOW_MODEL_SEARCH_SWITCH_SOURCE

#include "Source.hpp"

#include "../common/Utils.hpp"

#include <string>
#include <unordered_map>
#include <type_traits>
#include <boost/optional.hpp>
#include <functional>

#include <QNetworkAccessManager>

namespace downow {
namespace search {

struct SwitchSource : Source {
private:
    Q_OBJECT

    using base_type = Source;

public:
    SwitchSource(QString type, QObject * parent = nullptr);
    SwitchSource(QObject * parent = nullptr);

    void setQuery(const QString &) override;
    QString getQuery() const override;
    QString getLastLoadedQuery() const override;
    bool isLoading() const override;
    ResultsRandomAccessRange getResults() const override;
    Error getError() const override;

    QString getType() const override;
    void setType(QString type);

private:
    void onSourceChanged();

private:
    QNetworkAccessManager * _pnam;
    std::unordered_map<std::string, std::function<Source *()>> _typeToSource;
    unique_qobject_ptr<Source> _currentSource = nullptr;
    QString _type;
};

} }

#endif
