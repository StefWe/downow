
#include "Tidy.hpp"

#include <tidy/tidy.h>
#include <tidy/buffio.h>

#include <boost/exception/all.hpp>

namespace downow { namespace search {

std::string tidyHtml(char const* input, boost::optional<std::string &> errors) {
    TidyBuffer output;
    TidyBuffer errbuf;
    int rc = -1;
    Bool ok;

    TidyDoc tdoc = tidyCreate();                     // Initialize "document"
    tidyBufInit( &output );
    tidyBufInit( &errbuf );

    ok = tidyOptSetBool( tdoc, TidyXhtmlOut, yes );  // Convert to XHTML
    if ( ok )
        rc = tidySetErrorBuffer( tdoc, &errbuf );      // Capture diagnostics
    if ( rc >= 0 )
        rc = tidyParseString( tdoc, input );           // Parse the input
    if ( rc >= 0 )
        rc = tidyCleanAndRepair( tdoc );               // Tidy it up!
    if ( rc >= 0 )
        rc = tidyRunDiagnostics( tdoc );               // Kvetch
    if ( rc > 1 )                                    // If error, force output.
        rc = ( tidyOptSetBool(tdoc, TidyForceOutput, yes) ? rc : -1 );
    if ( rc >= 0 )
        rc = tidySaveBuffer( tdoc, &output );          // Pretty Print

    if ( rc >= 0 ) {
        if ( rc > 0 ) {
            if (errors) {
                errors.get() = std::string((char const*)errbuf.bp, errbuf.size);
            }
        }
    }
    else {
        printf( "A severe error (\%d) occurred.\\n", rc );
    }

    std::string result { (char const*)output.bp, output.size };

    tidyBufFree( &output );
    tidyBufFree( &errbuf );
    tidyRelease( tdoc );

    return result;
}

QDomDocument parseHtml(const char *inputHtml) {
    std::string tidy = tidyHtml(inputHtml);

    QDomDocument doc;
    doc.setContent(QByteArray(tidy.data(), tidy.size()));

    return doc;
}

} }
