#include "OpenBaySource.hpp"

#include "OpenBayScraper.hpp"
#include "Result.hpp"

#include "../common/Utils.hpp"

#include <qt5/QtConcurrent/QtConcurrent>
#include <boost/log/trivial.hpp>

namespace downow { namespace search {

OpenBaySource::OpenBaySource(QNetworkAccessManager * pnam, QObject * parent) :
    base_type(pnam, parent) {}

QString OpenBaySource::getSourceType() {
    return "OpenPirateBay";
}

void OpenBaySource::parse(QByteArray replyData) {
    QtConcurrent::run([this, replyData] {
        using namespace openbay::detail;

        boost::optional<std::vector<OpenBayTorrent>> torrentStructs = scrape(replyData);

        std::vector<std::unique_ptr<Result>> results;
        if (torrentStructs) {
            BOOST_LOG_TRIVIAL(debug) << "OpenBaySource: found " << torrentStructs->size() << " torrents";
            for (OpenBayTorrent const& t : *torrentStructs) {
                auto result = std::make_unique<Result>();
                result->moveToThread(thread());
                result->setParent(this);
                result->setName(QString::fromStdString(t.titleRow.name));
                result->setMagnetLink(QUrl(QString::fromStdString(t.titleRow.magnetLink)));

                int exponent = (int)t.size.unit;
                result->setSizeBytes(t.size.value * pow(1024, exponent));
                result->setSeeders(t.seeders);
                results.push_back(std::move(result));
            }
        }
        else {
            BOOST_LOG_TRIVIAL(debug) << "OpenBaySource: Couldn't parse data";
            Q_EMIT errorWhileContactingServer(Error::Parsing);
        }

        Q_EMIT doneParsingResults(std::make_shared<SharedResultsList::element_type>(std::move(results)));
    });
}

QUrl OpenBaySource::createUrlForQuery(QString query) {
    QUrl url;
    url.setScheme("https");
    url.setHost(getHost());
    url.setPort(443);
    url.setPath("/search.php");

    QUrlQuery urlQuery;
    urlQuery.addQueryItem("q", query.trimmed().replace(' ', '+'));
    urlQuery.addQueryItem("Torrent_sort", "seeders.desc");
    url.setQuery(urlQuery);

    return url;
}

QString OpenBaySource::getHost() {
    return "oldpiratebay.org";
}

QString OpenBaySource::getQueryFromUrl(QUrl const& url) {
    return QUrlQuery(url).queryItemValue("q");
}

} }
