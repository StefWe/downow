#ifndef DOWNOW_MODEL_SEARCH_PIRATE_BAY_SCRAPER
#define DOWNOW_MODEL_SEARCH_PIRATE_BAY_SCRAPER

#include <downow/config.hpp>

#if DOWNOW_SEARCH_ENABLE_PIRATE_BAY

#include <string>
#include <vector>
#include <boost/optional.hpp>
#include <qt5/QtCore/QByteArray>

namespace downow {
namespace search {
namespace tpb {
namespace detail {

enum class ByteUnit {
    B = 0, KB, MB, GB, TB
};

struct DetDesc {
    ByteUnit unit;
    float size;
};

struct PirateBayTorrent {
    std::string category;
    std::string name;
    std::string magnetLink;
    DetDesc desc;
    int seeders;
    int leechers;
};

boost::optional<std::vector<PirateBayTorrent>>
scrape(QByteArray html);

} } } }

#else
#   error Not in use until the pirate bay is back
#endif
#endif
