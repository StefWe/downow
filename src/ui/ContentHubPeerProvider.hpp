#ifndef DOWNOW_UI_CONTENT_HUB_PEER_PROVIDER
#define DOWNOW_UI_CONTENT_HUB_PEER_PROVIDER

#include <qt5/QtCore/QFuture>
#include <com/ubuntu/content/type.h>
#include <com/ubuntu/content/peer.h>

namespace downow { namespace ui {

struct ContentHubPeerProvider {
    virtual
    QFuture<com::ubuntu::content::Peer>
    getPeerForType(com::ubuntu::content::Type const&) = 0;
};

} }

#endif
