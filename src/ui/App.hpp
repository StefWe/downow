#ifndef DOWNOW_APP
#define DOWNOW_APP

#include "../config.hpp.in"
#include "../common/Utils.hpp"

#include <QtGui/QGuiApplication>
#include <QtCore/QFutureInterface>
#include <QtCore/QUrl>
#include <QtQml/QQmlListProperty>

#include <boost/filesystem/path.hpp>
#include <boost/optional.hpp>
#include <memory>

#include <qt5/QtCore/QMimeType>

#if DOWNOW_FILE_OPENER_CONTENT_HUB
#include <com/ubuntu/content/peer.h>
#include <com/ubuntu/content/type.h>
#include "ContentHubPeerProvider.hpp"
#endif

#if DOWNOW_SINGLE_APPLICATION
#include "SingleApplicationManager.hpp"
#endif

class QQuickView;
class QQmlEngine;

namespace downow {

namespace torrent_frontend {
    struct Session;
    struct FileEntry;
    struct Torrent;
}

namespace search {
    struct Settings;
}

namespace ui {

struct Settings;
struct FileOpener;
struct Notifier;

#if DOWNOW_APP_INDICATOR
struct AppIndicator;
#endif

// copied from here: http://bazaar.launchpad.net/~phablet-team/content-hub/trunk/view/head:/import/Ubuntu/Content/contenttype.h
// not ideal
enum class ContentType {
    All = -1,
    Unknown = 0,
    Documents = 1,
    Pictures = 2,
    Music = 3,
    Contacts = 4,
    Videos = 5,
    Links = 6
};

#if DOWNOW_FILE_OPENER_CONTENT_HUB
namespace cuc = com::ubuntu::content;
#endif

struct App : QGuiApplication
#if DOWNOW_FILE_OPENER_CONTENT_HUB
        , ContentHubPeerProvider
#endif
{
private:
    Q_OBJECT
    Q_PROPERTY(QString torrentsSavePath READ getTorrentsSavePath NOTIFY torrentsSavePathChanged)
    Q_PROPERTY(int maxDownloadSpeedBPS READ getMaxDownloadSpeedBPS WRITE setMaxDownloadSpeedBPS NOTIFY maxDownloadSpeedBPSChanged)
    Q_PROPERTY(int maxUploadSpeedBPS READ getMaxUploadSpeedBPS WRITE setMaxUploadSpeedBPS NOTIFY maxUploadSpeedBPSChanged)
    Q_PROPERTY(bool limitedBandwidth READ getLimitedBandwidth WRITE setLimitedBandwidth NOTIFY limitedBandwidthChanged)
    Q_PROPERTY(bool showInFileManagerEnabled READ showInFileManagerEnabled CONSTANT)
    Q_PROPERTY(bool isMainUIVisible READ isMainUIVisible NOTIFY isMainUIVisibleChanged)
    Q_PROPERTY(bool supportsRunningInBackground READ getSupportsRunningInBackground CONSTANT)

public:
    using base_type = QGuiApplication;
    using path = boost::filesystem::path;

    App(int & argc, char ** argv);
    ~App();

#if DOWNOW_FILE_OPENER_CONTENT_HUB
    QFuture<com::ubuntu::content::Peer>
    getPeerForType(com::ubuntu::content::Type const&) override;
#endif

    Q_INVOKABLE void openFileEntry(downow::torrent_frontend::FileEntry *);
    Q_INVOKABLE void openFilePaths(QStringList filePaths);
    Q_INVOKABLE bool canOpenFile(QString filePath) const;

    QString getTorrentsSavePath() const;

    int getMaxDownloadSpeedBPS() const;
    int getMaxUploadSpeedBPS() const;

    void setMaxDownloadSpeedBPS(int);
    void setMaxUploadSpeedBPS(int);

    bool getLimitedBandwidth() const;
    void setLimitedBandwidth(bool);

    bool showInFileManagerEnabled() const;

    bool isMainUIVisible() const;
    bool isExitting() const;
    bool getSupportsRunningInBackground() const;

    bool canOpen(QString) const;

public Q_SLOTS:
    void openMagnetUrl(QUrl);

    /**
     * Opens either a torrent file path, torrent file url, magnet url
     */
    void open(QString);

    void showView();
    void hideView();
    void windowClosed();
#if DOWNOW_FILE_OPENER_CONTENT_HUB
    void onSelectedPeerWithAppId(QString appId);
    void onSelectPeerCanceled();
#endif

Q_SIGNALS:
    void maxDownloadSpeedBPSChanged();
    void maxUploadSpeedBPSChanged();
    void showNoDestinationForType();
    void torrentsSavePathChanged();
    void limitedBandwidthChanged();
    void isMainUIVisibleChanged();
    void preparingToExit();

#if DOWNOW_FILE_OPENER_CONTENT_HUB
    void selectPeerForContentType(int contentType);
#endif

private Q_SLOTS:
    void updateMaxDownloadSpeedBPS();
    void updateMaxUploadSpeedBPS();
    void prepareToExit();

private:
    void initDirs();
    void setupFileLogging();
    void registerTypes();
    void initView(QString const& mainQmlPath);

#if DOWNOW_FILE_OPENER_CONTENT_HUB
    static ContentType contentTypeForMimeType(QMimeType const&);
    static cuc::Type typeForMimeType(QMimeType const&);
    static QMimeType mimeTypeForType(cuc::Type const&);
    static ContentType contentTypeForType(cuc::Type const&);
#endif

private:
    path _cacheDir;
    path _dataDir;
    path _configDir;
    path _downloadDir;
    unique_qobject_ptr<torrent_frontend::Session> _session;
    unique_qobject_ptr<Settings> _settings;
    unique_qobject_ptr<search::Settings> _searchSettings;
    unique_qobject_ptr<QQuickView> _view;
    unique_qobject_ptr<QQmlEngine> _engine;
    unique_qobject_ptr<Notifier> _notifier;
    std::unique_ptr<FileOpener> _fileOpener;
    QString _mainQmlPath;

    bool _isExitting = false;

#if DOWNOW_FILE_OPENER_CONTENT_HUB
    std::unique_ptr<QFutureInterface<com::ubuntu::content::Peer>> _selectPeerFutureInterface;
#endif

#if DOWNOW_APP_INDICATOR
    unique_qobject_ptr<AppIndicator> _appIndicator;
#endif

#if DOWNOW_SINGLE_APPLICATION
    SingleApplicationManager _singleApplicationManager;
#endif

    int _lastMaxDownloadSpeedBPS = 0;
    int _lastMaxUploadSpeedBPS = 0;
};

} }

#endif
