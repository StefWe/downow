#include "Notifier.hpp"

#include <downow/torrent-frontend/Torrent.hpp>
#include <libintl.h>

#include <boost/format.hpp>

namespace downow { namespace ui {

Notifier::Notifier(QObject * parent) :
    QObject(parent) {}

Notifier::~Notifier() {}

void
Notifier::onTorrentComplete(torrent_frontend::Torrent * torrent) {
    std::string notificationText =
            boost::str(
                boost::format(gettext("The torrent \"%s\" is complete"))
                    % torrent->getName().toStdString()
            );

    notify(gettext("Torrent complete"), QString::fromStdString(notificationText));
}

} }
