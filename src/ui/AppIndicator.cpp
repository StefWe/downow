
#include <downow/config.hpp>

#if DOWNOW_APP_INDICATOR

#include "AppIndicator.hpp"

#include <downow/config.hpp>
#include <downow/torrent-frontend/Session.hpp>

#include <libappindicator/app-indicator.h>
#include <boost/log/trivial.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <libintl.h>

#include "App.hpp"
#include "UnitFormatter.hpp"

namespace downow { namespace ui {

static void onShowAppClicked(GtkWidget * /*sender*/, AppIndicator * self) {
    Q_EMIT self->showApp();
}

static void onQuitClicked(GtkWidget * /*sender*/, AppIndicator * self) {
    Q_EMIT self->quitApp();
}

static void onPauseAllToggleClicked(GtkWidget * /*sender*/, AppIndicator * self) {
    if (self->getSession()->isPaused()) {
        self->getSession()->resume();
    }
    else {
        self->getSession()->pause();
    }
}

static void onMaxDownloadSpeedMenuItemToggled(GtkRadioMenuItem * sender, AppIndicator * self) {
    if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(sender))) {
        for (auto iter = self->_maxDownloadSpeedOptionToMenuItem.begin(); iter != self->_maxDownloadSpeedOptionToMenuItem.end(); iter++) {
            if (iter.value() == sender) {
                self->_app->setMaxDownloadSpeedBPS(iter.key());
            }
        }
    }
}

static void onMaxUploadSpeedMenuItemToggled(GtkRadioMenuItem * sender, AppIndicator * self) {
    if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(sender))) {
        for (auto iter = self->_maxUploadSpeedOptionToMenuItem.begin(); iter != self->_maxUploadSpeedOptionToMenuItem.end(); iter++) {
            if (iter.value() == sender) {
                self->_app->setMaxUploadSpeedBPS(iter.key());
            }
        }
    }
}

AppIndicator::AppIndicator(App * app, torrent_frontend::Session * session, QObject * parent) :
    QObject(parent),
    _session(session),
    _app(app) {

    _baseMenu = GTK_MENU(gtk_menu_new());
    auto addMenuItem = [] (GtkMenu * menu, GtkMenuItem * item) {
        gtk_menu_shell_append(GTK_MENU_SHELL(GTK_WIDGET(menu)), GTK_WIDGET(item));
        gtk_widget_show(GTK_WIDGET(item));
    };

    _showMainUIMenuItem = GTK_MENU_ITEM(gtk_menu_item_new_with_label(gettext("Show application")));
    addMenuItem(_baseMenu, _showMainUIMenuItem);
    g_signal_connect(GTK_WIDGET(_showMainUIMenuItem), "activate", G_CALLBACK(onShowAppClicked), this);

    onMainUIVisibilityChanged();

    _pauseAllToggleMenuItem = GTK_MENU_ITEM(gtk_menu_item_new());
    onPausedChanged();
    addMenuItem(_baseMenu, _pauseAllToggleMenuItem);
    g_signal_connect(GTK_WIDGET(_pauseAllToggleMenuItem), "activate", G_CALLBACK(onPauseAllToggleClicked), this);

    _downloadSpeedMenuItem = GTK_MENU_ITEM(gtk_menu_item_new());
    onDownloadSpeedChanged();
    addMenuItem(_baseMenu, _downloadSpeedMenuItem);

    _uploadSpeedMenuItem = GTK_MENU_ITEM(gtk_menu_item_new());
    onUploadSpeedChanged();
    addMenuItem(_baseMenu, _uploadSpeedMenuItem);

    {
        GtkMenu * downloadSpeedSubMenu = GTK_MENU(gtk_menu_new());
        GSList * group = nullptr;

        GtkMenuItem * titleMenuItem = GTK_MENU_ITEM(gtk_menu_item_new_with_label(gettext("Max download speed")));
        gtk_widget_set_sensitive(GTK_WIDGET(titleMenuItem), false);
        addMenuItem(downloadSpeedSubMenu, titleMenuItem);

        for (Bytes option : possibleSpeedValues) {
            GtkRadioMenuItem * item = GTK_RADIO_MENU_ITEM(gtk_radio_menu_item_new(group));
            if (option > 0) {
                gtk_menu_item_set_label(GTK_MENU_ITEM(item), formatBytes(option).toStdString().c_str());
            }
            else {
                gtk_menu_item_set_label(GTK_MENU_ITEM(item), gettext("Unlimited"));
            }

            group = gtk_radio_menu_item_get_group(item);

            g_signal_connect(GTK_WIDGET(item), "toggled", G_CALLBACK(onMaxDownloadSpeedMenuItemToggled), this);

            addMenuItem(downloadSpeedSubMenu, GTK_MENU_ITEM(item));
            _maxDownloadSpeedOptionToMenuItem[option] = item;
        }

        gtk_menu_item_set_submenu(_downloadSpeedMenuItem, GTK_WIDGET(downloadSpeedSubMenu));
    }

    {
        GtkMenu * uploadSpeedSubMenu = GTK_MENU(gtk_menu_new());
        GSList * group = nullptr;

        GtkMenuItem * titleMenuItem = GTK_MENU_ITEM(gtk_menu_item_new_with_label(gettext("Max upload speed")));
        gtk_widget_set_sensitive(GTK_WIDGET(titleMenuItem), false);
        addMenuItem(uploadSpeedSubMenu, titleMenuItem);

        for (Bytes option : possibleSpeedValues) {
            GtkRadioMenuItem * item = GTK_RADIO_MENU_ITEM(gtk_radio_menu_item_new(group));
            if (option > 0) {
                gtk_menu_item_set_label(GTK_MENU_ITEM(item), formatBytes(option).toStdString().c_str());
            }
            else {
                gtk_menu_item_set_label(GTK_MENU_ITEM(item), gettext("Unlimited"));
            }

            group = gtk_radio_menu_item_get_group(item);

            g_signal_connect(GTK_WIDGET(item), "toggled", G_CALLBACK(onMaxUploadSpeedMenuItemToggled), this);

            addMenuItem(uploadSpeedSubMenu, GTK_MENU_ITEM(item));
            _maxUploadSpeedOptionToMenuItem[option] = item;
        }

        gtk_menu_item_set_submenu(_uploadSpeedMenuItem, GTK_WIDGET(uploadSpeedSubMenu));
    }

    onMaxDownloadSpeedChanged();
    onMaxUploadSpeedChanged();

    GtkMenuItem * quitItem = GTK_MENU_ITEM(gtk_menu_item_new_with_label(gettext("Quit")));
    addMenuItem(_baseMenu, quitItem);
    g_signal_connect(GTK_WIDGET(quitItem), "activate", G_CALLBACK(onQuitClicked), this);

    _indicator = decltype(_indicator) {
        app_indicator_new(DOWNOW_REVERSE_DOMAIN, "downow", APP_INDICATOR_CATEGORY_APPLICATION_STATUS)
    };

    app_indicator_set_status(_indicator.get(), APP_INDICATOR_STATUS_ACTIVE);
    app_indicator_set_menu(_indicator.get(), _baseMenu);

    connect(_session.data(), &torrent_frontend::Session::downloadSpeedBPSChanged, this, &AppIndicator::onDownloadSpeedChanged);
    connect(_session.data(), &torrent_frontend::Session::uploadSpeedBPSChanged, this, &AppIndicator::onUploadSpeedChanged);
    connect(_app.data(), &App::maxDownloadSpeedBPSChanged, this, &AppIndicator::onMaxDownloadSpeedChanged);
    connect(_app.data(), &App::maxUploadSpeedBPSChanged, this, &AppIndicator::onMaxUploadSpeedChanged);
    connect(_app.data(), &App::isMainUIVisibleChanged, this, &AppIndicator::onMainUIVisibilityChanged);
    connect(_app.data(), &App::preparingToExit, this, &AppIndicator::onPreparingToExit);
    connect(_session.data(), &torrent_frontend::Session::isPausedChanged, this, &AppIndicator::onPausedChanged);
}

AppIndicator::~AppIndicator() {}

torrent_frontend::Session *AppIndicator::getSession() {
    return _session.data();
}

void AppIndicator::onDownloadSpeedChanged() {
    updateDownloadSpeedMenuItem();
}

void AppIndicator::onUploadSpeedChanged() {
    updateUploadSpeedMenuItem();
}

void AppIndicator::onMaxDownloadSpeedChanged() {
    auto iter = _maxDownloadSpeedOptionToMenuItem.find(_app->getMaxDownloadSpeedBPS());
    if (iter != _maxDownloadSpeedOptionToMenuItem.end()) {
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(*iter), true);
    }
    else {
        BOOST_LOG_TRIVIAL(warning)
                << "Max download speed " << _app->getMaxDownloadSpeedBPS() << "B/s "
                << "is not a supported option in the UI";
    }

    updateDownloadSpeedMenuItem();
}

void AppIndicator::onMaxUploadSpeedChanged() {
    auto iter = _maxUploadSpeedOptionToMenuItem.find(_app->getMaxUploadSpeedBPS());
    if (iter != _maxUploadSpeedOptionToMenuItem.end()) {
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(*iter), true);
    }
    else {
        BOOST_LOG_TRIVIAL(warning)
                << "Max download speed " << _app->getMaxUploadSpeedBPS() << "B/s "
                << "is not a supported option in the UI";
    }

    updateUploadSpeedMenuItem();
}

void AppIndicator::onPausedChanged() {
    gtk_menu_item_set_label(_pauseAllToggleMenuItem, _session->isPaused() ? gettext("Resume") : gettext("Pause"));
}

void AppIndicator::onMainUIVisibilityChanged() {
    if (_app->isMainUIVisible() || _app->isExitting()) {
        gtk_widget_hide(GTK_WIDGET(_showMainUIMenuItem));
    }
    else {
        gtk_widget_show(GTK_WIDGET(_showMainUIMenuItem));
    }
}

void AppIndicator::onPreparingToExit() {
    GList * list = gtk_container_get_children(GTK_CONTAINER(_baseMenu));
    for (guint i = 0; i < g_list_length(list); i++) {
        gtk_widget_hide(GTK_WIDGET(g_list_nth_data(list, i)));
    }

    GtkMenuItem * exittingMenuItem = GTK_MENU_ITEM(gtk_menu_item_new_with_label(gettext("Exitting..")));
    gtk_widget_set_sensitive(GTK_WIDGET(exittingMenuItem), false);
    gtk_menu_shell_append(GTK_MENU_SHELL(_baseMenu), GTK_WIDGET(exittingMenuItem));
    gtk_widget_show(GTK_WIDGET(exittingMenuItem));
}

void AppIndicator::updateDownloadSpeedMenuItem() {
    QString text = gettext("Download speed: ") + formatBytes(_session->getDownloadSpeedBPS()) + "/s";
    if (_app->getMaxDownloadSpeedBPS() > 0) {
        text += "   ≤   " + formatBytes(_app->getMaxDownloadSpeedBPS()) + "/s";
    }
    gtk_menu_item_set_label(_downloadSpeedMenuItem, text.toStdString().c_str());
}

void AppIndicator::updateUploadSpeedMenuItem() {
    QString text = gettext("Upload speed: ") + formatBytes(_session->getUploadSpeedBPS()) + "/s";
    if (_app->getMaxUploadSpeedBPS() > 0) {
        text += "   ≤   " + formatBytes(_app->getMaxUploadSpeedBPS()) + "/s";
    }
    gtk_menu_item_set_label(_uploadSpeedMenuItem, text.toStdString().c_str());
}

} }

#endif
