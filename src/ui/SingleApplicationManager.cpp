
#include <downow/config.hpp>

#if DOWNOW_SINGLE_APPLICATION

#include "SingleApplicationManager.hpp"

#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/posix_time_duration.hpp>

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/log/trivial.hpp>

#include <boost/thread.hpp>
#include <downow/common/Utils.hpp>

#include <array>

#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>

namespace downow { namespace ui {

namespace ipc = boost::interprocess;
namespace fs = boost::filesystem;

static char const* const messageQueueName = DOWNOW_REVERSE_DOMAIN "_message_queue";
static char const* const lockFileName = DOWNOW_REVERSE_DOMAIN "_lock";
static std::size_t constexpr maxMessageSize = 1024;

#define DOWNOW_MESSAGE_TYPE_OPEN "Open"
static char const* const messageTypeOpen = DOWNOW_MESSAGE_TYPE_OPEN;
static char const* const messageTypeOpenUrl = "OpenUrl";

SingleApplicationManager::SingleApplicationManager(QObject * parent) :
    QObject(parent),
    _mq(
        ipc::open_or_create,
        messageQueueName,
        /*max messages count*/ 10,
        /*max message size*/ maxMessageSize
    ) {
    fs::path lockFile = fs::temp_directory_path() / lockFileName;
    if (!fs::exists(lockFile)) {
        fs::ofstream of(lockFile, std::ios_base::out);
        of.close();
    }

    _mainAppFileLock = ipc::file_lock(lockFile.c_str());
    _alreadyExists = !_mainAppFileLock.try_lock();

    if (!_alreadyExists) {
        _listeningThread = boost::thread([this] {
             while (true) {
                 std::array<char, maxMessageSize> messageBuffer;
                 ipc::message_queue::size_type receivedMessageSize;
                 unsigned int priority;
                 _mq.receive(messageBuffer.data(), messageBuffer.size(), /*out*/ receivedMessageSize, /*out*/ priority);

                 boost::this_thread::interruption_point();

                 if (messageBuffer.size() < receivedMessageSize) {
                    BOOST_LOG_TRIVIAL(error)
                            << "Received message is too big. "
                            << "Message size: " << receivedMessageSize << ", "
                            << "buffer size: " << messageBuffer.size() << ", "
                            << "message: " << messageBuffer.data();
                    continue;
                 }

                 std::string message(messageBuffer.data(), receivedMessageSize);

                 // append null terminator
                 BOOST_LOG_TRIVIAL(debug) << "Received IPC message: " << message;

                 QJsonDocument messageJson = QJsonDocument::fromJson(QByteArray::fromRawData(message.data(), message.size()));

                 QString messageType = messageJson.object()["type"].toString();
                 if (messageType == messageTypeOpen) {
                     Q_EMIT anotherInstanceTriedToLaunch();
                 }
                 else if (messageType == messageTypeOpenUrl) {
                     Q_EMIT openLink(messageJson.object()["link"].toString());
                 }
             }
        });
    }
    else {
        // includes null terminator
        char message[] = "{\"type\": \"" DOWNOW_MESSAGE_TYPE_OPEN "\"}";

        // remove null terminator
        _mq.send(message, sizeof(message), /*priority*/ 0);
    }
}

SingleApplicationManager::~SingleApplicationManager() {
    if (!_alreadyExists) {
        _listeningThread.interrupt();
        {
            // send a message to wake up the listening thread
            _mq.send("", 0, 0);
        }
        _listeningThread.join();
        _mainAppFileLock.unlock();
        ipc::message_queue::remove(messageQueueName);
    }
}

bool SingleApplicationManager::alreadyExists() const {
    return _alreadyExists;
}

void SingleApplicationManager::sendLinkToMaster(QString const& link) {
    QJsonObject object;
    object["type"] = messageTypeOpenUrl;
    object["link"] = link;

    QByteArray json = QJsonDocument(object).toJson(QJsonDocument::Compact);
    _mq.send(json.data(), json.size(), /* priority */ 0);
}

} }

#endif
