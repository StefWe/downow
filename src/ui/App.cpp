#include "App.hpp"

#include "../config.hpp.in"

#if DOWNOW_FILE_OPENER_CONTENT_HUB
#   include "ContentHubOpener.hpp"
#endif

#if DOWNOW_FILE_OPENER_URL_DISPATCHER
#   include "DesktopServicesFileOpener.hpp"
#endif

#if DOWNOW_NOTIFICATION_BACKEND_LIBNOTIFY
#   include "NotifierLibNotify.hpp"
#endif

#if DOWNOW_APP_INDICATOR
#   include "AppIndicator.hpp"
#endif

#include "Notifier.hpp"
#include <QDesktopServices>

#include "Settings.hpp"

#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>

#include <boost/exception/all.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/sinks/async_frontend.hpp>
#include <boost/format.hpp>

#include <qt5/QtQuick/QQuickView>
#include <qt5/QtQml/QQmlEngine>
#include <qt5/QtQml/QtQml>
#include <qt5/QtCore/QUrl>
#include <qt5/QtCore/QTimer>
#include <qt5/QtCore/QCommandLineParser>

#include "../common/Utils.hpp"
#include "../backend-libtorrent/Session.hpp"
#include "../backend-mock/SessionMock.hpp"

#include "../torrent-frontend/TorrentListModel.hpp"
#include "../torrent-frontend/FileEntriesModel.hpp"
#include "../torrent-frontend/FileEntry.hpp"

#include "../search/Result.hpp"
#include "../search/YifySource.hpp"
#include "../search/SearchResultsListModel.hpp"
#include "../search/Settings.hpp"

#include <boost/preprocessor/stringize.hpp>

#include <memory>
#include <functional>

namespace downow { namespace ui {

namespace fs = boost::filesystem;
namespace logging = boost::log;

static void crashMessageOutput(QtMsgType type, QMessageLogContext const&, QString const& str) {
    switch (type) {
    case QtDebugMsg:
        BOOST_LOG_TRIVIAL(debug) << str;
        break;
    case QtWarningMsg:
        BOOST_LOG_TRIVIAL(warning) << str;
        break;
    case QtFatalMsg:
    case QtCriticalMsg:
        BOOST_LOG_TRIVIAL(fatal) << str;
        break;

    default: break;
    }
}

static void formatLogMessage(logging::record_view const& rec, logging::formatting_ostream & strm) {
    // Get the LineID attribute value and put it into the stream
//    strm << logging::extract< unsigned int >("LineID", rec) << ": ";

    // The same for the severity level.
    // The simplified syntax is possible if attribute keywords are used.
    strm << "<" << rec[logging::trivial::severity] << "> ";

    // Finally, put the record message to the stream
    strm << rec[logging::expressions::smessage];
}

App::App(int & argc, char ** argv) :
    base_type(argc, argv) {

    setOrganizationDomain(DOWNOW_REVERSE_DOMAIN);
    if (applicationName().isEmpty()) {
        setApplicationName("downow");
    }

    setApplicationVersion(
                BOOST_PP_STRINGIZE(DOWNOW_VERSION_MAJOR) "."
                BOOST_PP_STRINGIZE(DOWNOW_VERSION_MINOR) "."
                BOOST_PP_STRINGIZE(DOWNOW_VERSION_REVISION));

    QCommandLineParser parser;
    parser.setApplicationDescription(DOWNOW_DESCRIPTION);
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("links", "Magnet URLs/file URLs/file paths to open", "[links...]");

#ifndef DOWNOW_MAIN_QML_FILE
    QCommandLineOption mainQmlFileOption {
        "main-qml-file",
        "The path to the main QML file used for displaying the UI",
        "qml-file-path"
    };
    parser.addOption(mainQmlFileOption);
#endif

    char const* const torrentBackendLibtorrent = "libtorrent";
    char const* const torrentBackendMock = "mock";

    QStringList torrentBackendOptions;
    {
        #if DOWNOW_TORRENT_BACKEND_LIBTORRENT
            torrentBackendOptions.append(torrentBackendLibtorrent);
        #endif
        #if DOWNOW_TORRENT_BACKEND_MOCK
            torrentBackendOptions.append(torrentBackendMock);
        #endif
    }

    std::function<QString()> getTorrentBackend;
    {
        QHash<int, QString> const torrentBackendOptionToString {
            {DOWNOW_TORRENT_BACKEND_OPTION_LIBTORRENT, torrentBackendLibtorrent},
            {DOWNOW_TORRENT_BACKEND_OPTION_MOCK, torrentBackendMock}
        };

        QString defaultValue = torrentBackendOptionToString[DOWNOW_TORRENT_BACKEND_DEFAULT];

        if (torrentBackendOptions.size() > 1) {
            QCommandLineOption torrentBackendOption {
                "torrent-backend",
                "The backend to use for sharing torrents. Default is " + defaultValue,
                torrentBackendOptions.join("|")
            };
            torrentBackendOption.setDefaultValue(defaultValue);

            parser.addOption(torrentBackendOption);
            getTorrentBackend = [&parser] {
                return parser.value("torrent-backend");
            };
        }
        else {
            getTorrentBackend = [defaultValue] {
                return defaultValue;
            };
        }
    }

    char const* const notifierBackendLibnotify = "libnotify";
    char const* const notifierBackendNone = "none";

    QStringList notifierBackendOptions;
    {
        #if DOWNOW_NOTIFICATION_BACKEND_LIBNOTIFY
            notifierBackendOptions.append(notifierBackendLibnotify);
        #endif
        notifierBackendOptions.append(notifierBackendNone);
    }

    std::function<QString()> getNotifierBackend;
    {
        QHash<int, QString> const notifierBackendOptionToString {
            {DOWNOW_NOTIFICATION_BACKEND_OPTION_LIBNOTIFY, notifierBackendLibnotify},
            {DOWNOW_NOTIFICATION_BACKEND_OPTION_NONE, notifierBackendNone}
        };

        QString defaultValue = notifierBackendOptionToString[DOWNOW_NOTIFICATION_BACKEND_DEFAULT];

        if (notifierBackendOptions.size() > 1) {
            QCommandLineOption notifierBackendOption {
                "notification-backend",
                "The notification backend to use. Default is " + defaultValue,
                notifierBackendOptions.join("|")
            };
            notifierBackendOption.setDefaultValue(defaultValue);

            parser.addOption(notifierBackendOption);
            getNotifierBackend = [&parser] {
                return parser.value("notification-backend");
            };
        }
        else {
            getNotifierBackend = [defaultValue] {
                return defaultValue;
            };
        }
    }

    char const* const fileOpenerBackendOptionContentHub = "content-hub";
    char const* const fileOpenerBackendOptionUrlDispatcher = "url-dispatcher";

    QStringList fileOpenerBackendOptions;
    {
        #if DOWNOW_FILE_OPENER_CONTENT_HUB
            fileOpenerBackendOptions.append(fileOpenerBackendOptionContentHub);
        #endif
        #if DOWNOW_FILE_OPENER_URL_DISPATCHER
            fileOpenerBackendOptions.append(fileOpenerBackendOptionUrlDispatcher);
        #endif
    }

    std::function<QString()> getFileOpenerBackend;
    {
        QHash<int, QString> const fileOpenerBackendOptionToString {
            {DOWNOW_FILE_OPENER_OPTION_CONTENT_HUB, fileOpenerBackendOptionContentHub},
            {DOWNOW_FILE_OPENER_OPTION_URL_DISPATCHER, fileOpenerBackendOptionUrlDispatcher}
        };

        QString defaultValue = fileOpenerBackendOptionToString[DOWNOW_FILE_OPENER_DEFAULT];

        if (fileOpenerBackendOptions.size() > 1) {
            QCommandLineOption fileOpenerBackendOption {
                "file-opener-backend",
                "The backend used to open files externally. Default is " + defaultValue,
                fileOpenerBackendOptions.join("|")
            };
            fileOpenerBackendOption.setDefaultValue(defaultValue);

            parser.addOption(fileOpenerBackendOption);
            getFileOpenerBackend = [&parser] {
                return parser.value("file-opener-backend");
            };
        }
        else {
            getFileOpenerBackend = [defaultValue] {
                return defaultValue;
            };
        }
    }

#if DOWNOW_APP_INDICATOR
    QCommandLineOption startInBackgroundOption {
        "start-in-background",
        "Whether or not the app should present its main UI on start up"
    };
    parser.addOption(startInBackgroundOption);
#endif

    parser.process(arguments());

#ifndef DOWNOW_MAIN_QML_FILE
    _mainQmlPath = parser.value(mainQmlFileOption);
#else
    _mainQmlPath = DOWNOW_MAIN_QML_FILE;
#endif

    // show logs in console
    logging::add_console_log();

#if DOWNOW_SINGLE_APPLICATION
    if (_singleApplicationManager.alreadyExists()) {
        BOOST_LOG_TRIVIAL(debug) << "Application instance already exists";

        // start downloading positional arguments in the master process
        for (QString link : parser.positionalArguments()) {
            if (canOpen(link)) {
                _singleApplicationManager.sendLinkToMaster(link);
            }
            else {
                BOOST_LOG_TRIVIAL(error) << "Can't open link: " << link;
            }
        }

        ::exit(0);
    }
    connect(&_singleApplicationManager, &SingleApplicationManager::anotherInstanceTriedToLaunch, this, &App::showView);
    connect(&_singleApplicationManager, &SingleApplicationManager::openLink, this, &App::open);
#endif

    initDirs();

    BOOST_LOG_TRIVIAL(debug) << "Cache home: " << fs::absolute(_cacheDir);

    setupFileLogging();

    BOOST_LOG_TRIVIAL(debug) << "****************** Session started ******************";
    BOOST_LOG_TRIVIAL(debug) << "APP_ID: " << getenv("APP_ID");
    BOOST_LOG_TRIVIAL(debug) << "Config home: " << fs::absolute(_configDir);
    BOOST_LOG_TRIVIAL(debug) << "Data home: " << fs::absolute(_dataDir);

    auto const configFile = _configDir / "settings";
    std::shared_ptr<QSettings> qsettings = shared_qobject<QSettings>(QString::fromStdString(configFile.native()), QSettings::defaultFormat());

#if DOWNOW_CLEAR_SETTINGS
    qsettings.clear();
#endif

    _settings = make_unique_qobject<Settings>(qsettings);
    _searchSettings = make_unique_qobject<search::Settings>(qsettings);

    {
        using namespace fs;
        _downloadDir = _settings->getDownloadDir().toStdString();
        if (_downloadDir.empty()) {
            _downloadDir = _dataDir / "download";
        }
    }

    BOOST_LOG_TRIVIAL(debug)
            << "Version: "
            << _settings->getVersionMajor() << "."
            << _settings->getVersionMinor() << "."
            << _settings->getVersionRevision();

    if (_settings->getLastVersion()) {
        BOOST_LOG_TRIVIAL(debug)
                << "Last version launched: "
                << _settings->getLastVersion()->major << "."
                << _settings->getLastVersion()->minor << "."
                << _settings->getLastVersion()->revision;
    }
    else {
        BOOST_LOG_TRIVIAL(debug) << "First launch";
    }

    BOOST_LOG_TRIVIAL(debug)
            << "Settings:\n"
            << "\tDownload dir: " << fs::absolute(_downloadDir) << "\n"
            << "\tmax wifi download speed: " << _settings->getMaxWifiDownloadSpeedBPS() << "b/s\n"
            << "\tmax cellular download speed: " << _settings->getMaxCellularDownloadSpeedBPS() << "b/s\n"
            << "\tmax upload download speed: " << _settings->getMaxWifiUploadSpeedBPS() << "b/s\n"
            << "\tmax cellular upload speed: " << _settings->getMaxCellularUploadSpeedBPS() << "b/s";

    BOOST_LOG_TRIVIAL(debug) << "Torrent backend: " << getTorrentBackend();
    BOOST_LOG_TRIVIAL(debug) << "File opener backend: " << getFileOpenerBackend();
    BOOST_LOG_TRIVIAL(debug) << "Notification backend: " << getNotifierBackend();

    BOOST_LOG_TRIVIAL(debug) << "*****************************************************";

#if DOWNOW_TORRENT_BACKEND_LIBTORRENT
    if (getTorrentBackend() == torrentBackendLibtorrent) {
        boost::optional<int> lastRevision;
        if (_settings->getLastVersion()) {
            lastRevision = _settings->getLastVersion()->revision;
        }

        _session = make_unique_qobject<backend_libtorrent::Session>(
            _downloadDir,
            _dataDir / "libtorrent",
            lastRevision
        );
    }
#endif
#if DOWNOW_TORRENT_BACKEND_MOCK
    if (getTorrentBackend() == torrentBackendMock) {
        _session = make_unique_qobject<backend_mock::SessionMock>();
    }
#endif

    if (!_session) {
        BOOST_LOG_TRIVIAL(error)
                << "Torrent backend \"" << getTorrentBackend() << "\" is not valid. "
                << "Choose one of the options: " << torrentBackendOptions.join(", ");
        ::exit(1);
    }

#if DOWNOW_FILE_OPENER_CONTENT_HUB
    if (getFileOpenerBackend() == fileOpenerBackendOptionContentHub) {
        _fileOpener = std::make_unique<ContentHubOpener>(this);
    }
#endif

#if DOWNOW_FILE_OPENER_URL_DISPATCHER
    if (getFileOpenerBackend() == fileOpenerBackendOptionUrlDispatcher) {
        _fileOpener = std::make_unique<DesktopServicesFileOpener>();
    }
#endif

    if (!_fileOpener) {
        BOOST_LOG_TRIVIAL(error)
                << "File opener backend \"" << getFileOpenerBackend() << "\" is not valid. "
                << "Choose one of the options: " << fileOpenerBackendOptions.join(", ");
        ::exit(1);
    }

    // set session speed limits when we change from wifi to cellular or vice-versa
    {
        updateMaxDownloadSpeedBPS();
        updateMaxUploadSpeedBPS();

        connect(_settings.get(), &Settings::maxWifiDownloadSpeedBPSChanged, this, &App::updateMaxDownloadSpeedBPS);
        connect(_settings.get(), &Settings::maxCellularDownloadSpeedBPSChanged, this, &App::updateMaxDownloadSpeedBPS);
        connect(this, &App::limitedBandwidthChanged, this, &App::updateMaxDownloadSpeedBPS);

        connect(_settings.get(), &Settings::maxWifiUploadSpeedBPSChanged, this, &App::updateMaxUploadSpeedBPS);
        connect(_settings.get(), &Settings::maxCellularUploadSpeedBPSChanged, this, &App::updateMaxUploadSpeedBPS);
        connect(this, &App::limitedBandwidthChanged, this, &App::updateMaxUploadSpeedBPS);

        connect(this, &App::maxDownloadSpeedBPSChanged, [this] {
            _session->setMaxDownloadSpeedBPS(getMaxDownloadSpeedBPS());
        });
        connect(this, &App::maxUploadSpeedBPSChanged, [this] {
            _session->setMaxUploadSpeedBPS(getMaxUploadSpeedBPS());
        });

        _session->setMaxDownloadSpeedBPS(getMaxDownloadSpeedBPS());
        _session->setMaxUploadSpeedBPS(getMaxUploadSpeedBPS());
    }

    registerTypes();

#if DOWNOW_APP_INDICATOR
    if (!parser.isSet(startInBackgroundOption)) {
        showView();
    }
#else
    showView();
#endif

#if DOWNOW_APP_INDICATOR
    _appIndicator = make_unique_qobject<AppIndicator>(this, _session.get());
    connect(_appIndicator.get(), &AppIndicator::quitApp, this, &App::prepareToExit);
    connect(_appIndicator.get(), &AppIndicator::showApp, this, &App::showView);
    connect(_appIndicator.get(), &AppIndicator::hideApp, this, &App::hideView);
#endif

    connect(this, &App::preparingToExit, [this] {
        hideView();
    });
    connect(this, &App::preparingToExit, _session.get(), &torrent_frontend::Session::prepareToExit);

    connect(_session.get(), &torrent_frontend::Session::allowedToExit, this, [this] {
        BOOST_LOG_TRIVIAL(debug) << "Allowed to exit. Exiting";
        quit();
    });
    setQuitOnLastWindowClosed(false);

    QDesktopServices::setUrlHandler("magnet", this, SLOT(openMagnetUrl(QUrl)));

#if DOWNOW_NOTIFICATION_BACKEND_LIBNOTIFY
    if (getNotifierBackend() == notifierBackendLibnotify) {
        _notifier = make_unique_qobject<NotifierLibNotify>();
    }
#endif

    if (_notifier) {
        connect(_session.get(), &torrent_frontend::Session::torrentCompleted, _notifier.get(), &Notifier::onTorrentComplete);
    }

    // start downloading positional arguments
    for (QString link : parser.positionalArguments()) {
        if (canOpen(link)) {
            open(link);
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "Not a magnet URL or file URL or file path: " << link;
        }
    }
}

App::~App() {}

void App::openFileEntry(torrent_frontend::FileEntry * fileEntry) {
    BOOST_LOG_TRIVIAL(debug) << "Opening file " << fileEntry->getFileInfo().absoluteFilePath();
    try {
        _fileOpener->openFile(fileEntry->getFileInfo());
    }
    catch (FileOpener::CouldntOpenFile const&) {
        Q_EMIT showNoDestinationForType();
    }
}

void App::openFilePaths(QStringList filePaths) {
    if (filePaths.size() == 0) {
        return;
    }

    using torrent_frontend::FileType;

    boost::optional<FileType> fileType;
    try {
        fileType = FileType { filePaths[0] };
    }
    catch (FileType::CouldntFindFileTypeForMimeType const&) {}

    if (fileType) {
        BOOST_LOG_TRIVIAL(debug) << "Opening " << filePaths.size() << " files of type " << fileType->id();
    }
    else {
        BOOST_LOG_TRIVIAL(debug) << "Opening " << filePaths.size() << " files of unknown type";
    }

    try {
        _fileOpener->openFiles(filePaths);
    }
    catch (FileOpener::CouldntOpenFile const&) {
        Q_EMIT showNoDestinationForType();
    }
}

#if DOWNOW_FILE_OPENER_CONTENT_HUB
QFuture<com::ubuntu::content::Peer>
App::getPeerForType(com::ubuntu::content::Type const& type) {
    _selectPeerFutureInterface = std::make_unique<QFutureInterface<cuc::Peer>>();
    Q_EMIT selectPeerForContentType((int)contentTypeForType(type));
    return _selectPeerFutureInterface->future();
}

ContentType App::contentTypeForType(com::ubuntu::content::Type const& type) {
    using namespace cuc;
    if (type == Type::Known::music()) {
        return ContentType::Music;
    }
    else if (type == Type::Known::pictures()) {
        return ContentType::Pictures;
    }
    else if (type == Type::Known::videos()) {
        return ContentType::Videos;
    }
    else {
        return ContentType::Unknown;
    }
}

void App::onSelectedPeerWithAppId(QString appId) {
    if (_selectPeerFutureInterface) {
        _selectPeerFutureInterface->reportResult(cuc::Peer(appId));
        _selectPeerFutureInterface->reportFinished();
        _selectPeerFutureInterface.reset();
    }
}

void App::onSelectPeerCanceled() {
    if (_selectPeerFutureInterface) {
        _selectPeerFutureInterface->cancel();
        _selectPeerFutureInterface.reset();
    }
}
#endif

void App::initDirs() {
    using namespace fs;
    _configDir = [] {
        path configHome;
        if (getenv("XDG_CONFIG_HOME")) {
            configHome = path { getenv("XDG_CONFIG_HOME") };
        }
        else {
            configHome = path { getenv("HOME") } / ".config";
        }

        return configHome / DOWNOW_CLICK_PACKAGE_NAME;
    }();

    _dataDir = [] {
        path dataHome;
        if (getenv("XDG_DATA_HOME")) {
            dataHome = path { getenv("XDG_DATA_HOME") };
        }
        else {
            dataHome = path { getenv("HOME") } / ".local/share";
        }

        return dataHome / DOWNOW_CLICK_PACKAGE_NAME;
    }();

    _cacheDir = [] {
        path cacheHome;
        if (getenv("XDG_CACHE_HOME")) {
            cacheHome = path { getenv("XDG_CACHE_HOME") };
        }
        else {
            cacheHome = path { getenv("HOME") } / ".cache";
        }

        return cacheHome / DOWNOW_CLICK_PACKAGE_NAME;
    }();
}

void
App::setupFileLogging() {
    using Sink = logging::sinks::asynchronous_sink<logging::sinks::text_ostream_backend>;
    auto sink = boost::make_shared<Sink>();
    fs::create_directories(_cacheDir);
    sink->locked_backend()->add_stream(boost::make_shared<fs::ofstream>(_cacheDir / "log"));
    sink->set_formatter(&formatLogMessage);
    sink->locked_backend()->auto_flush(true);
    logging::core::get()->add_sink(sink);

    qInstallMessageHandler(&crashMessageOutput);
}

void App::registerTypes() {
    char const* const uri = "com.nogzatalz.Downow";
    // @uri com.nogzatalz.Downow
    qmlRegisterUncreatableType<Settings>(uri, 1, 0, "DowNowSettings", "Get a reference through a DowNowBackend instance");
    qmlRegisterUncreatableType<torrent_frontend::Session>(uri, 1, 0, "Session", "Session is uncreatable from QML. Use the globalSession injected property to get a hold of a Session instance.");
    qmlRegisterUncreatableType<torrent_frontend::Torrent>(uri, 1, 0, "Torrent", "Torrent is uncreatable from QML. Use TorrentListModel to get a list of torrents from a Session.");
    qmlRegisterUncreatableType<torrent_frontend::FileEntry>(uri, 1, 0, "FileEntry", "FileEntry is uncreatable from QML. Use FileEntriesModel to get a list of file entries from a Torrent.");
    qmlRegisterType<torrent_frontend::TorrentListModel>(uri, 1, 0, "TorrentListModel");
    qmlRegisterType<search::Result>(uri, 1, 0, "SearchResult");
    qmlRegisterType<search::VideoResult>(uri, 1, 0, "VideoResult");
    qmlRegisterType<search::SearchResultsListModel>(uri, 1, 0, "SearchResultsListModel");
    qmlRegisterUncreatableType<search::Source>(uri, 1, 0, "SearchSource", "");
    qmlRegisterUncreatableType<search::Settings>(uri, 1, 0, "SearchSettings", "Singleton. Use globalSearchSettings to get a reference.");
    qmlRegisterType<torrent_frontend::FileEntriesModel>(uri, 1, 0, "FileEntriesModel");
}

void
App::showView() {
    if (isMainUIVisible()) {
        return;
    }

    if (!_engine) {
        _engine = make_unique_qobject<QQmlEngine>();
    }

    if (!_view) {
        _view = make_unique_qobject<QQuickView>(_engine.get(), /*parent*/ nullptr);
        _view->rootContext()->setContextProperty("globalSettings", _settings.get());
        _view->rootContext()->setContextProperty("globalSession", _session.get());
        _view->rootContext()->setContextProperty("globalSearchSettings", _searchSettings.get());
        _view->rootContext()->setContextProperty("app", this);
        _view->setSource(QUrl::fromLocalFile(_mainQmlPath));
        _view->setResizeMode(QQuickView::ResizeMode::SizeRootObjectToView);
        connect(_view.get(), SIGNAL(closing(QQuickCloseEvent*)), this, SLOT(windowClosed()));
    }

    _view->show();
    Q_EMIT isMainUIVisibleChanged();
}

void
App::hideView() {
    if (isMainUIVisible()) {
        _view->hide();
        Q_EMIT isMainUIVisibleChanged();
    }
}

void App::windowClosed() {
#if DOWNOW_APP_INDICATOR
    if (_settings->getRunInBackground()) {
        hideView();
    }
    else {
        prepareToExit();
    }
#else
    prepareToExit();
#endif
}

QString
App::getTorrentsSavePath() const {
    return _session->getTorrentsSaveDirectory().absolutePath();
}

void
App::updateMaxDownloadSpeedBPS() {
    int newVal = getLimitedBandwidth()
            ? _settings->getMaxCellularDownloadSpeedBPS()
            : _settings->getMaxWifiDownloadSpeedBPS();
    if (_lastMaxDownloadSpeedBPS == newVal) {
        return;
    }

    _lastMaxDownloadSpeedBPS = newVal;
    Q_EMIT maxDownloadSpeedBPSChanged();
}

void
App::updateMaxUploadSpeedBPS() {
    int newVal = getLimitedBandwidth()
            ? _settings->getMaxCellularUploadSpeedBPS()
            : _settings->getMaxWifiUploadSpeedBPS();
    if (_lastMaxUploadSpeedBPS == newVal) {
        return;
    }

    _lastMaxUploadSpeedBPS = newVal;
    Q_EMIT maxUploadSpeedBPSChanged();
}

void App::prepareToExit() {
    _isExitting = true;
    Q_EMIT preparingToExit();
}

int
App::getMaxDownloadSpeedBPS() const {
    return _lastMaxDownloadSpeedBPS;
}

int
App::getMaxUploadSpeedBPS() const {
    return _lastMaxUploadSpeedBPS;
}

void
App::setMaxDownloadSpeedBPS(int maxDownloadSpeedBPS) {
    _settings->setMaxCellularDownloadSpeedBPS(maxDownloadSpeedBPS);
    _settings->setMaxWifiDownloadSpeedBPS(maxDownloadSpeedBPS);
}

void
App::setMaxUploadSpeedBPS(int maxUploadSpeedBPS) {
    _settings->setMaxCellularUploadSpeedBPS(maxUploadSpeedBPS);
    _settings->setMaxWifiUploadSpeedBPS(maxUploadSpeedBPS);
}

bool
App::getLimitedBandwidth() const {
    return false;
//    if (_limitedBandwidth) {
//        return true;
//    }
//    else {
//        // if indeterminate also return false
//        return false;
//    }
}

void
App::setLimitedBandwidth(bool limitedBandwidth) {
//    if (_limitedBandwidth == limitedBandwidth) {
//        return;
//    }

//    _limitedBandwidth = limitedBandwidth;
    //    Q_EMIT limitedBandwidthChanged();
}

bool
App::showInFileManagerEnabled() const {
    return DOWNOW_SHOW_IN_FILE_MANAGER;
}

bool App::isMainUIVisible() const {
    return _view && _view->isVisible();
}

bool App::isExitting() const {
    return _isExitting;
}

bool App::getSupportsRunningInBackground() const {
    return DOWNOW_APP_INDICATOR;
}

void App::openMagnetUrl(QUrl url) {
    _session->addTorrentByMagnetLink(url);
}

void App::open(QString str) {
    QFileInfo fileInfo {str};
    if (fileInfo.exists() && fileInfo.suffix() == "torrent") {
        _session->addTorrentByFile(str);
    }
    else {
        QUrl url(str);
        if (url.isValid()) {
            if (url.scheme() == "file") {
                _session->addTorrentByFile(url.toLocalFile());
            }
            else if (url.scheme() == "magnet") {
                _session->addTorrentByMagnetLink(url);
            }
        }
    }
}

bool App::canOpen(QString str) const {
    QFileInfo fileInfo {str};
    if (fileInfo.exists() && fileInfo.suffix() == "torrent") {
        return true;
    }
    else {
        QUrl url {str, QUrl::StrictMode};
        if (url.isValid()) {
            if (url.scheme() == "file" || url.scheme() == "magnet") {
                return true;
            }
        }
    }

    return false;
}

bool App::canOpenFile(QString filePath) const {
    return _fileOpener->canOpenFile(QFileInfo(filePath));
}

} }
