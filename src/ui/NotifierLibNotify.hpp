
#ifndef DOWNOW_UI_NOTIFIER_LIBNOTIFY
#define DOWNOW_UI_NOTIFIER_LIBNOTIFY

#include <downow/config.hpp>

#if !DOWNOW_NOTIFICATION_BACKEND_LIBNOTIFY
#   error libnotify backend not selected for compilation
#else

#include "Notifier.hpp"

namespace downow { namespace ui {

struct NotifierLibNotify : Notifier {
private:
    Q_OBJECT

public:
    NotifierLibNotify(QObject * parent = nullptr);
    ~NotifierLibNotify() override;

protected:
    void notify(QString title, QString message) override;
};

} }

#endif
#endif
