#ifndef DOWNOW_UI_NOTIFIER
#define DOWNOW_UI_NOTIFIER

#include <QtCore/QObject>
#include <QtCore/QString>

namespace downow {

namespace torrent_frontend {
    struct Torrent;
}

namespace ui {

struct Notifier : QObject {
private:
    Q_OBJECT

public:
    Notifier(QObject * parent = nullptr);
    virtual ~Notifier();

public Q_SLOTS:
    virtual void onTorrentComplete(torrent_frontend::Torrent *);

protected:
    virtual void notify(QString title, QString message) = 0;
};

} }

#endif // NOTIFIER_HPP
