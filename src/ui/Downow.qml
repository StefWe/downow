import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.0
import Ubuntu.Content 1.1
import com.nogzatalz.Downow 1.0
import "components"
import "components/torrent-list-page"
import "components/common"
import "components/torrent-page"
import "components/common/Utils.js" as Utils
import QtMultimedia 5.0

MainView {
    id: root

    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "com.nogzatalz.downow"

    automaticOrientation: true
    anchorToKeyboard: true

    width: Utils.gu(90)
    height: Utils.gu(65)

    property DowNowTheme downowTheme: whiteTheme

    DowNowTheme {
        id: darkTheme
        backgroundColor: "#3f3f3f"
        textColor: "#fff"
        progressIndicatorDownloadingColor: "#20a800"
        progressIndicatorPausedColor: "#ddd"
        progressIndicatorDoneColor: "#20a800"
    }

    DowNowTheme {
        id: whiteTheme
        backgroundColor: "#eee"
        textColor: "#666"
        torrentTitleColor: "#555"
        progressIndicatorDownloadingColor: "#20a800"
        progressIndicatorPausedColor: "#ddd"
        progressIndicatorDoneColor: progressIndicatorDownloadingColor
        torrentDownloadSpeedTextColor: Qt.darker(textColor, 1.1)
        torrentUploadSpeedTextColor: torrentDownloadSpeedTextColor
    }

    backgroundColor: downowTheme.backgroundColor

    Action {
        id: addAction
        text: "Add"
        iconName: "add"
        onTriggered: {
            pageStack.push(searchPageComponent)
        }
    }

    Action {
        id: settingsAction
        iconName: "settings"
        text: "Settings"
        onTriggered: {
            tabs.selectedTabIndex = settingsTab.index
        }
    }

    Action {
        id: pauseAction
        iconName: "media-playback-pause"
        text: "Pause"
        visible: globalSession.torrentsCount > 0 && !globalSession.isPaused
        onTriggered: {
            globalSession.pause()
        }
    }

    Action {
        id: resumeAction
        iconName: "media-playback-start"
        text: "Resume"
        visible: globalSession.torrentsCount > 0 && globalSession.isPaused
        onTriggered: {
            globalSession.resume()
        }
    }

    Audio {
        id: doneSound
        source: "sounds/done.ogg"
    }

    Connections {
        target: globalSession
        onTorrentCompleted: {
            doneSound.play()
        }
        onDuplicateTorrentError: {
            PopupUtils.open(duplicateTorrentDialogComponent, root, {torrentName: name})
        }
    }

    Connections {
        target: app
        onSelectPeerForContentType: {
            PopupUtils.open(peerPickerPopupComponent, root, {contentType: contentType})
        }

        onShowNoDestinationForType: {
            PopupUtils.open(cantOpenFileComponent, root)
        }
    }

    Connections {
        target: UriHandler
        onOpened: {
            console.log('Received URIs from URL dispatcher');
            for (var i = 0; i < uris.length; i++) {
                globalSession.addTorrentByMagnetLink(uris[i]);
            }
        }
    }

    PageStack {
        id: pageStack
        Component.onCompleted: push(tabs)

        Tabs {
            id: tabs
            visible: false

            Tab {
                title: page.title
                page: TorrentListPage {
                    id: torrentListPage
                    downowTheme: root.downowTheme
                    addAction: addAction
                    settingsAction: settingsAction
                    resumeAction: resumeAction
                    pauseAction: pauseAction

                    onOpenTorrentDetailsPage: {
                        pageStack.push(torrentPageComponent, {
                            torrent: torrent
                        });
                    }
                }
            }

            Tab {
                id: settingsTab
                title: "Settings"
                page: Loader {
                    parent: settingsTab
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                    source: (tabs.selectedTab === settingsTab)
                            ? Qt.resolvedUrl("components/settings-page/SettingsPage.qml")
                            : ""
                }
            }

            Tab {
                id: faqTab
                title: "FAQ"
                page: Loader {
                    parent: faqTab
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                    source: (tabs.selectedTab === faqTab)
                            ? Qt.resolvedUrl("components/faq-page/FaqPage.qml")
                            : ""
                }
            }

            Tab {
                id: aboutTab
                title: "About"
                page: Loader {
                    parent: aboutTab
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                    source: (tabs.selectedTab === aboutTab)
                            ? Qt.resolvedUrl("components/about-page/AboutPage.qml")
                            : ""
                }
            }
        }
    }

    Component {
        id: searchPageComponent

        SearchPage {
            id: searchPage

            onTorrentLinkSelected: {
                globalSession.addTorrentByMagnetLink(magnetLink)
            }
            onTorrentFileSelected: {
                globalSession.addTorrentByFile(torrentFilePath)
            }
            onSelectedTorrent: {
                pageStack.pop()
            }
        }
    }

    Component {
        id: torrentPageComponent
        TorrentPage {
            downowTheme: root.downowTheme
            onDone: pageStack.pop()
        }
    }

    Component {
        id: peerPickerPopupComponent
        PopupBase {
            id: popup
            property alias contentType: peerPicker.contentType

            ContentPeerPicker {
                id: peerPicker
                handler: ContentHandler.Destination

                onPeerSelected: {
                    app.onSelectedPeerWithAppId(peer.appId)
                    PopupUtils.close(popup)
                }

                onCancelPressed: {
                    app.onSelectPeerCanceled()
                    PopupUtils.close(popup)
                }
            }
        }
    }

    Component {
        id: cantOpenFileComponent

        Dialog {
            id: cantOpenFileDialog

            title: "I can't open that file"
            text: "Files are stored in " + app.torrentsSavePath

            Button {
                text: "OK"
                onClicked: PopupUtils.close(cantOpenFileDialog)
            }
        }
    }

    Component {
        id: duplicateTorrentDialogComponent

        Dialog {
            id: duplicateTorrentDialog

            property string torrentName

            title: "Duplicate torrent"
            text: "The torrent \"" + torrentName + "\" already exists"

            Button {
                text: "OK"
                onClicked: PopupUtils.close(duplicateTorrentDialog)
            }
        }
    }
}
