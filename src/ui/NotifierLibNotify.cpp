
#include <downow/config.hpp>

#if DOWNOW_NOTIFICATION_BACKEND_LIBNOTIFY

#include "NotifierLibNotify.hpp"

#include <libnotify/notify.h>
#include <boost/log/trivial.hpp>

namespace downow { namespace ui {

NotifierLibNotify::NotifierLibNotify(QObject * parent) :
    Notifier(parent) {
    notify_init(DOWNOW_REVERSE_DOMAIN);
}

NotifierLibNotify::~NotifierLibNotify() {
    notify_uninit();
}

void NotifierLibNotify::notify(QString title, QString message) {
    NotifyNotification * notification = notify_notification_new(
                title.toStdString().c_str(),
                message.toStdString().c_str(),
                /*icon*/ "");

    GError * error = nullptr;
    bool success = notify_notification_show(notification, &error);
    if (!success) {
        BOOST_LOG_TRIVIAL(error) << "Error while trying to send notification: " << error->message;
        g_object_unref(G_OBJECT(error));
    }

    g_object_unref(G_OBJECT(notification));
}

} }

#endif
