#ifndef DOWNOW_UI_APP_INDICATOR
#define DOWNOW_UI_APP_INDICATOR

#include <downow/config.hpp>

#if !DOWNOW_APP_INDICATOR
#   error App indicator not selected for compilation
#else

#include <QtCore/QObject>
#include <QtCore/QPointer>

#include <libappindicator/app-indicator.h>
#include <memory>

#include <QHash>

#include "UnitFormatter.hpp"

namespace downow {

namespace torrent_frontend {
    struct Session;
}

namespace ui {

struct App;

template <typename T>
struct GObjectDeleter {
    void operator() (T * obj) const {
        g_object_unref(G_OBJECT(obj));
    }
};

template <typename T>
using gobject_ptr = std::unique_ptr<T, GObjectDeleter<T>>;

struct AppIndicator : QObject {
private:
    Q_OBJECT

public:
    AppIndicator(App *, torrent_frontend::Session *, QObject * parent = nullptr);
    ~AppIndicator();

    torrent_frontend::Session * getSession();

Q_SIGNALS:
    void showApp();
    void hideApp();
    void quitApp();

public Q_SLOTS:
    void onDownloadSpeedChanged();
    void onUploadSpeedChanged();
    void onMaxDownloadSpeedChanged();
    void onMaxUploadSpeedChanged();
    void onPausedChanged();
    void onMainUIVisibilityChanged();
    void onPreparingToExit();

private:
    void updateDownloadSpeedMenuItem();
    void updateUploadSpeedMenuItem();

public:
    QPointer<torrent_frontend::Session> _session;
    QPointer<App> _app;
    gobject_ptr< ::AppIndicator> _indicator;

    GtkMenu * _baseMenu;
    GtkMenuItem * _downloadSpeedMenuItem;
    GtkMenuItem * _uploadSpeedMenuItem;
    GtkMenuItem * _pauseAllToggleMenuItem;
    GtkMenuItem * _showMainUIMenuItem;

    QHash<Bytes, GtkRadioMenuItem *> _maxDownloadSpeedOptionToMenuItem;
    QHash<Bytes, GtkRadioMenuItem *> _maxUploadSpeedOptionToMenuItem;
};

} }

#endif
#endif
