#ifndef DOWNOW_UI_SETTINGS
#define DOWNOW_UI_SETTINGS

#include <downow/config.hpp>

#include <qt5/QtCore/QObject>

#include <memory>

#include <boost/filesystem/path.hpp>
#include <boost/optional.hpp>

struct QSettings;

namespace downow { namespace ui {

struct Settings : QObject {
private:
    Q_OBJECT
    Q_PROPERTY(int maxWifiDownloadSpeedBPS READ getMaxWifiDownloadSpeedBPS WRITE setMaxWifiDownloadSpeedBPS NOTIFY maxWifiDownloadSpeedBPSChanged)
    Q_PROPERTY(int maxCellularDownloadSpeedBPS READ getMaxCellularDownloadSpeedBPS WRITE setMaxCellularDownloadSpeedBPS NOTIFY maxCellularDownloadSpeedBPSChanged)
    Q_PROPERTY(int maxWifiUploadSpeedBPS READ getMaxWifiUploadSpeedBPS WRITE setMaxWifiUploadSpeedBPS NOTIFY maxWifiUploadSpeedBPSChanged)
    Q_PROPERTY(int maxCellularUploadSpeedBPS READ getMaxCellularUploadSpeedBPS WRITE setMaxCellularUploadSpeedBPS NOTIFY maxCellularUploadSpeedBPSChanged)

#if DOWNOW_APP_INDICATOR
    Q_PROPERTY(bool runInBackground READ getRunInBackground WRITE setRunInBackground NOTIFY runInBackgroundChanged)
#endif

    Q_PROPERTY(QString downloadDir READ getDownloadDir WRITE setDownloadDir NOTIFY downloadDirChanged)

    Q_PROPERTY(int versionMajor READ getVersionMajor NOTIFY versionMajorChanged)
    Q_PROPERTY(int versionMinor READ getVersionMinor NOTIFY versionMinorChanged)
    Q_PROPERTY(int versionRevision READ getVersionRevision NOTIFY versionRevisionChanged)
public:
    Settings(std::shared_ptr<QSettings> settings, QObject * parent = nullptr);
    static Settings & instance();

    int getMaxWifiDownloadSpeedBPS() const;
    int getMaxCellularDownloadSpeedBPS() const;
    int getMaxWifiUploadSpeedBPS() const;
    int getMaxCellularUploadSpeedBPS() const;

    void setMaxWifiDownloadSpeedBPS(int);
    void setMaxCellularDownloadSpeedBPS(int);
    void setMaxWifiUploadSpeedBPS(int);
    void setMaxCellularUploadSpeedBPS(int);

    QString getDownloadDir() const;
    void setDownloadDir(QString);

#if DOWNOW_APP_INDICATOR
    bool getRunInBackground() const;
    void setRunInBackground(bool);
#endif

    int getVersionMajor() const;
    int getVersionMinor() const;
    int getVersionRevision() const;

    struct Version {
        int major;
        int minor;
        int revision;
    };

    boost::optional<Version> getLastVersion() const {
        return _lastVersion;
    }

Q_SIGNALS:
    void maxWifiDownloadSpeedBPSChanged();
    void maxCellularDownloadSpeedBPSChanged();
    void maxWifiUploadSpeedBPSChanged();
    void maxCellularUploadSpeedBPSChanged();
    void versionMajorChanged();
    void versionMinorChanged();
    void versionRevisionChanged();
    void downloadDirChanged();
    void runInBackgroundChanged();

private:
    std::shared_ptr<QSettings> _settings;
    boost::optional<Version> _lastVersion;
};

} }

#endif
