
#include <downow/config.hpp>

#if DOWNOW_FILE_OPENER_URL_DISPATCHER

#include "DesktopServicesFileOpener.hpp"

#include <qt5/QtGui/QDesktopServices>
#include <qt5/QtCore/QFileInfo>
#include <qt5/QtCore/QUrl>

namespace downow { namespace ui {

void DesktopServicesFileOpener::openFiles(FileOpener::FileInfoInputRange const& fileInfoRange) {
    for (QFileInfo const& fileInfo : fileInfoRange) {
        QDesktopServices::openUrl(QUrl::fromLocalFile(fileInfo.absoluteFilePath()));
    }
}

bool DesktopServicesFileOpener::canOpenFile(QFileInfo const& fileInfo) {
    return true;
}

} }

#endif
