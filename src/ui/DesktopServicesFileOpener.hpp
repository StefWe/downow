#ifndef DOWNOW_UI_DESKTOP_SERVICES_FILE_OPENER
#define DOWNOW_UI_DESKTOP_SERVICES_FILE_OPENER

#include <downow/config.hpp>

#if !DOWNOW_FILE_OPENER_URL_DISPATCHER
#   error url dispatcher not selected for compilation
#else

#include "FileOpener.hpp"

namespace downow { namespace ui {

struct DesktopServicesFileOpener : FileOpener {
    void openFiles(FileInfoInputRange const&) override;
    bool canOpenFile(QFileInfo const&) override;
};

} }

#endif
#endif
