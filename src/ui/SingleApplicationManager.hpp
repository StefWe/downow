#ifndef DOWNOW_UI_SINGLE_APPLICATION_MANAGER
#define DOWNOW_UI_SINGLE_APPLICATION_MANAGER

#include <downow/config.hpp>

#if DOWNOW_SINGLE_APPLICATION

#include <QtCore/QObject>

#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/interprocess/sync/file_lock.hpp>
#include <boost/thread/thread.hpp>

namespace downow { namespace ui {

struct SingleApplicationManager : QObject {
private:
    Q_OBJECT

public:
    SingleApplicationManager(QObject * parent = nullptr);
    ~SingleApplicationManager();

    bool alreadyExists() const;
    void sendLinkToMaster(QString const&);

Q_SIGNALS:
    void anotherInstanceTriedToLaunch();
    void openLink(QString);

private:
    boost::interprocess::message_queue _mq;
    boost::interprocess::file_lock _mainAppFileLock;
    boost::thread _listeningThread;
    bool _alreadyExists;
};

} }

#endif
#endif
