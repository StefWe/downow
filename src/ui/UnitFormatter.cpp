#include "UnitFormatter.hpp"

#include <math.h>
#include <utility> //pair
#include <array>
#include <initializer_list>

namespace downow { namespace ui {

std::vector<Bytes> const possibleSpeedValues = {0, 5 * 1024, 50 * 1024, 100 * 1024, 250 * 1024, 500 * 1024, 750 * 1024, 1024 * 1024};

template <typename Num, typename UnitsRange>
std::pair<Num, char const*> formatUnit(Num num, UnitsRange unitsRange) {
    char const* name;
    for (auto const& unit : unitsRange) {
        if (std::abs(num) / unit.getStep() < 1 || unit.getStep() == 0) {
            name = unit.getName();
            break;
        }
        num /= unit.getStep();
    }

    return {num, name};
}

QString formatBytes(Bytes const& bytes) {
    struct MetricUnit {
        MetricUnit(char const* name) : _name(name) {}
        char const* _name;

        char const* getName() const {
            return _name;
        }
        int getStep() const {
            return 1024;
        }
    };

    std::initializer_list<MetricUnit> units {
        "B", "KB", "MB", "GB", "TB"
    };

    auto result = formatUnit(bytes, units);
    return QString::number(result.first) + result.second;
}

QString formatDuration(std::chrono::seconds const& seconds) {
    struct Unit {
        char const* name;
        int step;

        char const* getName() const {
            return name;
        }
        int getStep() const {
            return step;
        }
    };

    std::initializer_list<Unit> units {
        {"second", 60}, {"minute", 60}, {"hour", 24}, {"day", 30}, {"month", 12}, {"year", 0}
    };

    auto result = formatUnit(seconds.count(), units);
    QString str = QString::number(result.first) + " " + result.second;
    if (result.first > 1) {
        str += "s";
    }

    return str;
}

} }
