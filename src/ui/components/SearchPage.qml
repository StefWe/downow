import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0
import com.nogzatalz.Downow 1.0
import "search-page"
import "common/Format.js" as Format

Page {
    id: root
    title: "Add torrent"

    signal torrentLinkSelected(string magnetLink)
    signal torrentFileSelected(url torrentFilePath)
    signal selectedTorrent

    onTorrentFileSelected: selectedTorrent()
    onTorrentLinkSelected: selectedTorrent()

    property bool openedInBrowser: false

    // close this page if the user found something on the web
    Connections {
        target: UriHandler
        onOpened: {
            if (openedInBrowser) {
                openedInBrowser = false;
                root.selectedTorrent();
            }
        }
    }

    head {
        actions: [
            Action {
                iconName: "stock_website"
                text: "Search in browser"
                visible: searchField.text !== ""
                onTriggered: {
                    PopupUtils.open(searchInBrowserDialog)
                }
            }
        ]

        contents: TextField {
            id: searchField
            width: parent ? parent.width - units.gu(2) : implicitWidth
            placeholderText: "Search"
        }
    }

    SearchResultsListModel {
        id: yifyResultsModel
        query: searchField.text
        type: "Yify"
    }

//    SearchResultsListModel {
//        id: openBayResultsModel
//        query: searchField.text
//        type: "OpenPirateBay"
//    }

    SearchResultsListModel {
        id: kickassResultsModel
        query: searchField.text
        type: "KickassTorrents"
    }

    property var sources: [yifyResultsModel, kickassResultsModel]
    property var sourceContainerViews: [yifySourceContainerView, kickassSourceContainerView]

    Label {
        anchors {
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }

        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        text: "You can use this page to search " + sources.map(function(s) { return s.type; }).join(", ") + " from within the app, or you can open the browser and tap on a magnet link to start downloading the torrent in DowNow."
        visible: sourceContainerViews.every(function(s) { return !s.visible; });
    }

    Flickable {
        anchors.fill: parent
        contentWidth: width
        contentHeight: column.height

        Column {
            id: column
            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: units.gu(1)

            SourceContainerView {
                id: yifySourceContainerView
                model: yifyResultsModel
                resultsComponent: Component {
                    MediaCoverflowSourceView {
                        model: yifyResultsModel
                        onTorrentLinkSelected: root.torrentLinkSelected(magnetLink)
                    }
                }
            }

            SourceContainerView {
                id: kickassSourceContainerView
                model: kickassResultsModel
                resultsComponent: Component {
                    GenericListSourceView {
                        model: kickassResultsModel
                        onTorrentLinkSelected: root.torrentLinkSelected(magnetLink)
                    }
                }
            }

            Column {
                anchors {
                    left: parent.left
                    right: parent.right
                }

                visible: sources.every(function(s) {
                    return !s.isLoading && s.lastLoadedQuery !== ""
                })

                spacing: units.gu(1)

                ListItem.ThinDivider {}

                Label {
                    anchors {
                        left: parent.left
                        right: parent.right
                        leftMargin: units.gu(1)
                        rightMargin: units.gu(1)
                    }

                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    text: "If you didn't find what you were looking for, you can always search the web for the torrent and tap on a magnet link to start downloading it in DowNow."
                }

                Button {
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                    }

                    text: "Search the web"
                    onClicked: PopupUtils.open(searchInBrowserDialog)
                }

                // an empty item to add padding at the bottom
                Item {
                    height: units.gu(1)
                    width: units.gu(1)
                }
            }
        }
    }

    Component {
        id: searchInBrowserDialog

        Dialog {
            id: dialog
            title: i18n.tr("Search for \"" + searchField.text + "\"in the browser")

            Button {
                text: i18n.tr("Google")
                color: UbuntuColors.orange
                onClicked: {
                    var success = Qt.openUrlExternally("https://www.google.co.il/search?q=" + searchField.text + " torrent")
                    PopupUtils.close(dialog)
                    root.openedInBrowser = success;
                }
            }
            Button {
                text: i18n.tr("DuckDuckGo")
                color: UbuntuColors.orange
                onClicked: {
                    var success = Qt.openUrlExternally("https://duckduckgo.com/?q=" + searchField.text + " torrent")
                    PopupUtils.close(dialog)
                    root.openedInBrowser = success;
                }
            }

            Button {
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialog)
            }
        }
    }
}
