import QtQuick 2.0
import Ubuntu.Components 1.3
import "../common/Format.js" as Format
import "../common/Utils.js" as Utils
import "../common"

Row {
    id: root

    property url icon
    property int speedBytesPerSec
    property color textColor
    property DowNowTheme downowTheme

    spacing: Utils.gu(0.5)

    Label {
        id: label
        text: Format.formatBytes(speedBytesPerSec) + "/s"
        fontSize: "small"
        color: root.textColor
    }

    Image {
        anchors {
            verticalCenter: parent.verticalCenter
        }

        source: icon
        sourceSize.width: Utils.gu(1.5)
    }
}
