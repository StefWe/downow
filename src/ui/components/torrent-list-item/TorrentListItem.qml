import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0
import "../common/Format.js" as Format
import "../common/Utils.js" as Utils
import "../common"

ListItem.Empty {
    id: root
    height: Utils.gu(9.5)
    property DowNowTheme downowTheme
    property /*Torrent*/ var torrent

//    removable: true
//    confirmRemoval: true

    ProgressIndicator {
        id: progressIndicator

        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            margins: Utils.gu(1)
        }

        torrent: root.torrent
        downowTheme: root.downowTheme

        onResume: {
            torrent.resume()
        }
        onPause: {
            torrent.pause()
        }
    }

    Item {
        id: rightColumn

        anchors {
            left: progressIndicator.right
            top: parent.top
            bottom: parent.bottom
            right: parent.right
            leftMargin: Utils.gu(1)
            bottomMargin: Utils.gu(1)
            topMargin: Utils.gu(0.8)
        }

        Text {
            id: name
            anchors {
                left: parent.left
                right: parent.right
                rightMargin: Utils.gu(1)
                top: parent.top
            }
            font.family: "ubuntu"
            font.pixelSize: Utils.gu(2.3)

            elide: Text.ElideRight
            color: downowTheme.torrentTitleColor

            text: {
                if (torrent.state === Torrent.Preparing && torrent.name === '') {
                    return 'Preparing';
                }
                else {
                    return torrent.name;
                }
            }
        }

        Item {
            anchors {
                left: parent.left
                right: parent.right
                top: name.bottom
                bottom: parent.bottom
            }

            Component {
                id: preparingComponent

                Label {
                    id: downloadingMetadata
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: "Downloading metadata"
                }
            }

            Component {
                id: downloadingComponent

                Column {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    spacing: units.gu(0.3)

                    Label {
                        id: timeLeft
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        fontSize: "medium"
                        elide: Text.ElideRight
                        color: downowTheme.textColor

                        text: torrent.timeLeftSec >= 0
                              ? Format.formatDuration(torrent.timeLeftSec) + " left"
                              : ""
                    }

                    Label {
                        id: percentsLabel
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        fontSize: "medium"
                        elide: Text.ElideRight
                        color: downowTheme.textColor

                        text: {
                            var percents = 100 * torrent.progress;
                            var downloadedBytesStr = Format.formatKB(torrent.downloadedKB);
                            var totalBytesStr = Format.formatKB(torrent.totalKB);
                            return percents.toFixed(0) + "% - " + downloadedBytesStr + "/" + totalBytesStr
                        }
                    }
                }
            }

            Component {
                id: seedingComponent

                Column {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    spacing: units.gu(0.3)

                    Label {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        fontSize: "medium"
                        elide: Text.ElideRight
                        color: downowTheme.textColor

                        text: 'Size: ' + Format.formatKB(torrent.downloadedKB)
                    }

                    Label {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        fontSize: "medium"
                        elide: Text.ElideRight
                        color: downowTheme.textColor

                        text: 'Uploaded: ' + Format.formatKB(torrent.uploadedKB) + ' (Ratio: ' + Format.truncate(torrent.ratio, 2) + ')'
                    }
                }
            }

            Loader {
                id: text
                anchors {
                    left: parent.left
                    right: speedIndicators.left
                    bottom: parent.bottom
                }

                sourceComponent: {
                    switch (torrent.state) {
                    case Torrent.Preparing:
                        return preparingComponent;
                    case Torrent.Downloading:
                    case Torrent.Paused:
                        return downloadingComponent;
                    case Torrent.Seeding:
                        return seedingComponent;
                    }
                }
            }

            Column {
                id: speedIndicators
                anchors {
                    right: parent.right
                    bottom: parent.bottom
                    rightMargin: Utils.gu(0.5)
                }
                spacing: Utils.gu(0.5)

                SpeedIndicator {
                    anchors.right: parent.right
                    speedBytesPerSec: torrent.uploadSpeedBPS
                    icon: "../../images/upload-icon.svg"
                    textColor: root.downowTheme.torrentUploadSpeedTextColor
                }
                SpeedIndicator {
                    anchors.right: parent.right
                    speedBytesPerSec: torrent.downloadSpeedBPS
                    icon: "../../images/download-icon.svg"
                    textColor: root.downowTheme.torrentUploadSpeedTextColor
                    visible: torrent.state != Torrent.Seeding
                }
            }
        }
    }
}
