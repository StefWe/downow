import QtQuick 2.2
import Ubuntu.Components 1.3
import com.nogzatalz.Downow 1.0
import "../common/Utils.js" as Utils
import "../common"

Item {
    id: root
    implicitHeight: Utils.gu(10)
    width: height

    property /*Torrent*/ var torrent
    property real percents: torrent ? torrent.progress : 0

    property DowNowTheme downowTheme
    property bool pulseWhenDownloading: false
    property color antiprogressCircleColor: UbuntuColors.warmGrey
    property color innerCircleColor: "#fff"

    signal pause
    signal resume

    property color progressCircleColor: {
        switch (state) {
        case "downloading": return downowTheme.progressIndicatorDownloadingColor
        case "paused": return downowTheme.progressIndicatorPausedColor
        case "done": return downowTheme.progressIndicatorDoneColor
        default: return downowTheme.progressIndicatorDownloadingColor
        }
    }

    states: [
        State {
            name: "preparing"
            when: torrent.state === Torrent.Preparing
        },
        State {
            name: "downloading"
            when: torrent.state === Torrent.Downloading
        },
        State {
            name: "paused"
            when: torrent.state === Torrent.Paused
        },
        State {
            name: "done"
            when: torrent.state === Torrent.Seeding
        }
    ]

    Rectangle {
        id: antiprogressCircle
        anchors.fill: parent
        color: root.antiprogressCircleColor
        radius: width / 2
    }

    Pie {
        id: progressCircle
        anchors.fill: parent

        color: root.progressCircleColor
        percentsFilled: root.percents
    }

    FinishedIndicator {
        id: innerCircle
        anchors.fill: parent

        isFinished: root.state == "done"

        front: Rectangle {
            color: root.innerCircleColor
            width: parent.width * 0.6
            height: width
            radius: width / 2

            anchors.centerIn: parent

            Image {
                id: pauseResumeImage
                anchors.centerIn: parent
                height: sourceSize.height
                sourceSize.height: 0.6 * parent.height
                source: {
                    if (root.state == "downloading") {
                        return "../../images/pause-icon.svg";
                    }
                    else if (root.state == "paused" || root.state == "preparing") {
                        return "../../images/resume-icon.svg";
                    }
                    else {
                        return "";
                    }
                }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (root.state == "paused") {
                root.resume();
            }
            else if (root.state == "downloading") {
                root.pause();
            }
        }
    }
}
