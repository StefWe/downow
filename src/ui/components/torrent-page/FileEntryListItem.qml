import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0
import "../common"
import "../common/Format.js" as Format
import "../common/Utils.js" as Utils
import "../common/FileTypeProperties.js" as FileTypeProperties
import ".."

ListItem.Base {
    id: root
    height: Utils.gu(7)
    property DowNowTheme downowTheme
    property /*FileEntry*/ var fileEntry

    property bool selectionMode: false

    progression: fileEntry.isCompleted && !selectionMode

    Item {
        id: progressIndicator
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            topMargin: units.gu(1)
            bottomMargin: units.gu(1)
        }
        width: height

        Rectangle {
            id: antiprogressCircle
            anchors.fill: parent
            color: UbuntuColors.warmGrey
            radius: width / 2
        }

        Pie {
            anchors.fill: parent
            color: {
                switch (fileEntry.state) {
                case FileEntry.Downloading: return downowTheme.progressIndicatorDownloadingColor
                case FileEntry.Paused: return downowTheme.progressIndicatorPausedColor
                case FileEntry.Done: return downowTheme.progressIndicatorDoneColor
                default: return downowTheme.progressIndicatorDownloadingColor
                }
            }
            percentsFilled: fileEntry.progress
        }

        FinishedIndicator {
            anchors.fill: parent
            isFinished: fileEntry.isCompleted

            front: Rectangle {
                id: whiteCircle
                anchors.centerIn: parent
                height: parent.height * 0.85
                width: height

                color: "white"
                radius: width / 2

                Label {
                    id: percents
                    text: (fileEntry.progress * 100).toFixed(0)
                    anchors.centerIn: parent
                    fontSize: "large"
                }
            }
        }
    }

    Column {
        anchors {
            right: typeIcon.left
            rightMargin: units.gu(0.5)
            left: progressIndicator.right
            verticalCenter: parent.verticalCenter
            leftMargin: units.gu(1)
        }
        spacing: units.gu(0.5)

        Text {
            id: name
            anchors {
                left: parent.left
                right: parent.right
            }
            font.family: "ubuntu"
            font.pixelSize: Utils.gu(2)

            elide: Text.ElideRight
            color: root.selected ? UbuntuColors.orange : downowTheme.torrentTitleColor

            text: fileEntry.name
        }

        Label {
            anchors {
                left: parent.left
                right: parent.right
            }
            fontSize: "medium"
            elide: Text.ElideRight
            color: downowTheme.textColor

            text: {
                var timeLeft = fileEntry.timeLeftSec >= 0
                  ? Format.formatDuration(fileEntry.timeLeftSec) + " left"
                  : "";
                var downloadedBytesStr = Format.formatKB(fileEntry.downloadedKB);
                var totalBytesStr = Format.formatKB(fileEntry.totalKB);

                return (timeLeft !== "" ? timeLeft + " • " : "") +
                        downloadedBytesStr + "/" + totalBytesStr
            }
        }
    }

    Icon {
        id: typeIcon
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        height: visible ? units.gu(2) : 0
        width: height

        color: root.selected ? UbuntuColors.orange : keyColor

        visible: typeIcon.name !== ""

        name: {
            try {
                return FileTypeProperties.get(fileEntry.typeId).iconName;
            }
            catch (exp) {
                return "";
            }
        }
    }
}
