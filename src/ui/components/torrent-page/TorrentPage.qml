import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Content 1.1
import Ubuntu.Components.Popups 1.0
import com.nogzatalz.Downow 1.0
import "../common/FileTypeProperties.js" as FileTypeProperties
import "../common"
import ".."
import "../torrent-list-item"

Page {
    id: root
    property /*Torrent*/ var torrent
    property DowNowTheme downowTheme
    property alias selectedFiles: files.selectedFiles

    title: "Details"

    signal done

    state: "normal"

    Connections {
        target: files
        onSelectedFile: {
            root.state = "selection"
        }
        onDeselectedFile: {
            if (files.selectedFiles.length === 0) {
                root.state = "normal"
            }
        }
    }

    states: [
        PageHeadState {
            name: "normal"
            head: root.head
            actions: [
                removeAction,
                selectAction,
                openAllOfTypeAction
            ]
        },
        PageHeadState {
            name: "selection"
            head: root.head
            actions: [
                cancelSelectionAction,
                openSelectedAction
            ]
        }
    ]

    Action {
        id: removeAction
        text: "Remove"
        iconName: "delete"
        onTriggered: {
            PopupUtils.open(deleteDialogComponent)
        }
    }

    Action {
        id: selectAction
        text: "Select"
        iconName: "select"
        onTriggered: {
            root.state = "selection"
        }
    }

    function openInFileManager() {
        Qt.openUrlExternally("file://" + torrent.savePath);
    }

    function openDialogToChooseTypeOfFilesToOpen(fileEntries) {
        var dialog = PopupUtils.open(openDialogComponent);
        dialog.filesSelected.connect(function(fileEntries) {
            app.openFilePaths(filePathsOfFileEntries(fileEntries))
        });
    }

    Action {
        id: openAllOfTypeAction
        iconName: "external-link"
        text: "Open"
        onTriggered: {
            openDialogToChooseTypeOfFilesToOpen(torrent.fileEntries)
        }
    }

    Action {
        id: openSelectedAction
        iconName: "external-link"
        text: "Open selected"
        onTriggered: {
            openDialogToChooseTypeOfFilesToOpen(files.selectedFiles)
        }
    }

    function filePathsOfFileEntries(fileEntries) {
        var filePaths = [];
        for (var i = 0; i < fileEntries.length; i++) {
            filePaths.push(fileEntries[i].savePath);
        }
        return filePaths;
    }

    Action {
        id: cancelSelectionAction
        iconName: "cancel"
        text: "Cancel selection"
        onTriggered: {
            files.cancelSelection()
        }
    }

    Connections {
        target: torrent
        onRemoved: {
            root.done()
        }
    }

    flickable: null

    TorrentListItem {
        id: torrentListItem
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        downowTheme: root.downowTheme
        torrent: root.torrent
    }

    TorrentFiles {
        id: files
        anchors {
            top: torrentListItem.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        torrent: root.torrent
        downowTheme: root.downowTheme
        state: root.state
    }

    Component {
        id: openDialogComponent

        Dialog {
            id: openDialog

            property var completeFiles: {
                var fileEntries = root.state === "normal" ? root.torrent.fileEntries : root.selectedFiles

                var completeFiles = []
                for (var i = 0; i < fileEntries.length; i++) {
                    if (fileEntries[i].isCompleted) {
                        completeFiles.push(fileEntries[i])
                    }
                }

                return completeFiles
            }
            property bool hasCompleteFiles: completeFiles.length > 0

            signal filesSelected(var fileEntries)

            title: "Open"
            text: {
                if (hasCompleteFiles) {
                    if (app.showInFileManagerEnabled) {
                        return "Select type of files to open or open the torrent's directory in the file manager";
                    }
                    else {
                        return "Select type of files to open";
                    }
                }
                else {
                    "No complete files to open"
                }
            }

            property var typesOfCompleteFiles: {
                var types = {};
                for (var i = 0; i < completeFiles.length; i++) {
                    var typeInfo = types[completeFiles[i].typeId];
                    if (!typeInfo) {
                        typeInfo = []
                        types[completeFiles[i].typeId] = typeInfo;
                    }
                    typeInfo.push(completeFiles[i]);
                }
                return types
            }

            Column {
                spacing: units.gu(1)

                Repeater {
                    model: {
                        var typeToFiles = openDialog.typesOfCompleteFiles;
                        var typeIds = Object.keys(typeToFiles);

                        if (typeIds.length === 0) {
                            return [];
                        }

                        // sort by count
                        typeIds.sort(function(val1, val2) {
                            if (typeToFiles[val1].length < typeToFiles[val2].length) {
                                return 1;
                            }
                            else if (typeToFiles[val1].length > typeToFiles[val2].length) {
                                return -1;
                            }
                            else {
                                return 0;
                            }
                        });

                        return typeIds;
                    }

                    Button {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        color: UbuntuColors.orange
                        text: (modelData !== "" ? modelData : "Unknown") +
                              " (" + openDialog.typesOfCompleteFiles[modelData].length + ")"

                        onClicked: {
                            openDialog.filesSelected(openDialog.typesOfCompleteFiles[modelData])
                            PopupUtils.close(openDialog);
                        }
                    }
                }

                Loader {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    sourceComponent: app.showInFileManagerEnabled
                                     ? openInFileManagerButtonComponent
                                     : null
                }

                Component {
                    id: openInFileManagerButtonComponent
                    Button {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        color: UbuntuColors.orange
                        text: "Open in file browser"

                        onClicked: {
                            openInFileManager()
                            PopupUtils.close(openDialog);
                        }
                    }
                }

                Button {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    text: "Cancel"
                    onClicked: {
                        PopupUtils.close(openDialog);
                    }
                }
            }
        }
    }

    // FIXME: No idea why these timers are needed.
    // If I remove them and just call torrent.removeAndDeleteFiles() from within the dialog,
    // I get the following error and the app freezes.
    // error: QQmlExpression: Attempted to evaluate an expression in an invalid context
    Timer {
        id: removeAndDeleteFilesTimer
        interval: 100
        repeat: false
        running: false
        onTriggered: {
            torrent.removeAndDeleteFiles()
        }
    }
    Timer {
        id: removeTimer
        interval: 100
        repeat: false
        running: false
        onTriggered: {
            torrent.remove()
        }
    }

    Component {
        id: deleteDialogComponent
        Dialog {
            id: deleteDialog
            title: "Delete Torrent"
            text: "Are you sure you want to delete this torrent?"

            Button {
                text: "Delete and remove files"
                onClicked: {
                    removeAndDeleteFilesTimer.restart()
                    PopupUtils.close(deleteDialog)
                }
                color: UbuntuColors.red
            }

            Button {
                text: "Delete"
                onClicked: {
                    removeTimer.restart()
                    PopupUtils.close(deleteDialog)
                }
                color: UbuntuColors.red
            }

            Button {
                text: "Cancel"
                onClicked: PopupUtils.close(deleteDialog)
            }
        }
    }
}
