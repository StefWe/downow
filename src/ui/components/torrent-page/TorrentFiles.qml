import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0
import com.nogzatalz.Downow 1.0
import "../common"

Item {
    id: root

    signal selectedFile
    signal deselectedFile

    property var /* Torrent */ torrent
    property DowNowTheme downowTheme

    property var __selectedFiles: ({})
    property var selectedFiles: Object.keys(__selectedFiles).map(function(fileName) {
        return __selectedFiles[fileName]
    })

    function selectFile(file) {
        __selectedFiles[file.name] = file;
        __selectedFilesChanged()
        root.selectedFile()
    }

    function deselectFile(file) {
        delete __selectedFiles[file.name]
        __selectedFilesChanged()
        root.deselectedFile()
    }

    function isFileSelected(file) {
        return __selectedFiles[file.name];
    }

    function cancelSelection() {
        __selectedFiles = {}
        __selectedFilesChanged()
        root.deselectedFile()
    }

    states: [
        State {
            name: "selection"
        },
        State {
            name: "normal"
        }
    ]

    ListItem.Header {
        id: filesHeader
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        text: "Files"
    }

    ActivityIndicator {
        id: loadingFilesIndicator
        anchors {
            topMargin: units.gu(1)
            top: filesHeader.bottom
            horizontalCenter: parent.horizontalCenter
        }
        running: torrent.state === Torrent.Preparing
        visible: running
    }

    ListView {
        id: filesColumn
        anchors {
            top: filesHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        clip: true
        visible: !loadingFilesIndicator.running

        model: FileEntriesModel {
            torrent: root.torrent
        }

        delegate: FileEntryListItem {
            fileEntry: model.fileEntry
            downowTheme: root.downowTheme

            selected: root.isFileSelected(model.fileEntry) ? true : false
            selectionMode: root.state === "selection"

            onClicked: {
                if (root.state === "selection") {
                    toggleSelection()
                }
                else {
                    if (model.fileEntry.isCompleted) {
                        app.openFileEntry(model.fileEntry);
                    }
                    else {
                        PopupUtils.open(cantOpenIncompleteFileDialogComponent)
                    }
                }
            }

            onPressAndHold: toggleSelection()

            function toggleSelection() {
                if (isFileSelected(model.fileEntry)) {
                    deselectFile(model.fileEntry)
                }
                else {
                    selectFile(model.fileEntry)
                }
            }
        }

        footer: Item {
            anchors {
                left: parent.left
                right: parent.right
            }

            height: openFilesInstruction.height + units.gu(2)

            Label {
                id: openFilesInstruction
                anchors {
                    left: parent.left
                    right: parent.right
                    rightMargin: units.gu(1)
                    leftMargin: units.gu(1)
                    verticalCenter: parent.verticalCenter
                }

                text: "You can open files by tapping them once they are done downloading"
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
        }
    }

    Component {
        id: cantOpenIncompleteFileDialogComponent

        Dialog {
            id: cantOpenIncompleteFileDialog

            title: "I can't open incomplete files"
            text: "You can open files by tapping them once they are done downloading"

            Button {
                text: "OK"
                onTriggered: {
                    PopupUtils.close(cantOpenIncompleteFileDialog)
                }
            }
        }
    }
}
