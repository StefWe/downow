import QtQuick 2.2

Canvas {
    id: root

    property color color
    property real percentsFilled: 0
    property real startRadians: Math.PI / 2
    property bool isClockwise: true

    Behavior on percentsFilled {
        SpringAnimation {
            spring: 5
            damping: 1
        }
    }

    Behavior on color {
        ColorAnimation {
            duration: 500
        }
    }

    onColorChanged: requestPaint()
    onWidthChanged: requestPaint()
    onPercentsFilledChanged: requestPaint()
    onStartRadiansChanged: requestPaint()
    onIsClockwiseChanged: requestPaint()

    onPaint: {
        var ctx = getContext("2d");
        ctx.clearRect(0, 0, width, height);
        ctx.resetTransform();

        var middleX = width / 2;
        var middleY = height / 2;
        var startAngle = -root.startRadians;
        var endAngle = (root.percentsFilled * Math.PI * 2) - root.startRadians;

        // green circle
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.arc(middleX, middleY, width / 2, startAngle, endAngle, false);
        ctx.lineTo(middleX, middleY);
        ctx.lineTo(middleX, 0);

        ctx.fill();
    }
}
