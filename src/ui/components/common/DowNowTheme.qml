import QtQuick 2.0

QtObject {
    property color backgroundColor
    property color textColor
    property color torrentTitleColor: textColor
    property color torrentDownloadSpeedTextColor: textColor
    property color torrentUploadSpeedTextColor: torrentDownloadSpeedTextColor

    property color progressIndicatorDownloadingColor
    property color progressIndicatorPausedColor
    property color progressIndicatorDoneColor
}
