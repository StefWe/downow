.pragma library

/**
  * @param getUnitStop
  *     a function that receives an index and
  *     returns the multiplier for the next unit
  *
  * @return
  *     A map with num being the new number after we divided it
  *     and unitIndex being the unit index in which we stopped
  */
function calculateUnitValueAndIndex(num, getUnitStop, maxUnitIndex) {
    if (!maxUnitIndex) maxUnitIndex = Number.MAX_VALUE;

    var flipSigns = num < 0
    if (flipSigns) {
        num = -num
    }

    var unitIndex = 0;
    while (num / getUnitStop(unitIndex) >= 1 && unitIndex < maxUnitIndex) {
        num = num / getUnitStop(unitIndex);
        unitIndex++;
    }

    return {
        num: flipSigns ? -num : num,
        unitIndex: unitIndex
    };
}

function formatBytes(bytes) {
    return formatKB(bytes / 1024);
}

function formatKB(kb) {
    var units = ["KB", "MB", "GB", "TB"];
    var stops = function() { return 1024 };
    var f = calculateUnitValueAndIndex(kb, stops, units.length-1);
    return truncate(f.num, 1) + units[f.unitIndex];
}

function formatDuration(seconds) {
    var stops = function(i) {
        switch (i) {
        case 0: return 60;
        case 1: return 60;
        case 2: return 24;
        case 3: return 7;
        case 4: return 30;
        case 5: return 12;
        }
    };
    var units = ["second", "minute", "hour", "day", "week", "month", "year"];
    var f = calculateUnitValueAndIndex(seconds, stops, units.length-1);
    f.num = Math.floor(f.num);
    return f.num + " " + units[f.unitIndex] + (f.num > 1 ? "s" : "");
}

function formatSeeds(seeds) {
    var stops = function() {
        return 1000;
    };
    var units = ["", "K"];
    var f = calculateUnitValueAndIndex(seeds, stops, units.length-1);
    f.num = Math.floor(f.num);
    return f.num + units[f.unitIndex] + " S";
}

function truncate(number, decimalPoints) {
    function isInteger(num) {
        return num === Math.floor(num);
    }

    if (isInteger(number)) {
        return number.toString();
    }
    else {
        var hasLessDecimalPlaces = isInteger(number * Math.pow(10, decimalPoints));
        if (hasLessDecimalPlaces) {
            return number.toString();
        }
        else {
            return number.toFixed(decimalPoints);
        }
    }
}
