import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem

ListItem.Empty {
    height: label.height + units.gu(2)

    property alias text: label.text

    highlightWhenPressed: false

    Label {
        id: label
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right

            topMargin: units.gu(1)
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }

        wrapMode: Text.WordWrap
        onLinkActivated: Qt.openUrlExternally(link)
        linkColor: UbuntuColors.orange
    }
}
