import QtQuick 2.0

Flipable {
    id: root
    property bool isFinished: false

    back: Image {
        anchors {
            centerIn: parent
        }

        source: "../../images/done-icon.svg"
        sourceSize.width: parent.width * 0.7
    }

    transform: Rotation {
        origin {
            x: root.width / 2
            y: root.height / 2
        }
        axis { x: 1; y: 0; z: 0 }

        angle: root.isFinished ? 180 : 0

        Behavior on angle {
            NumberAnimation {
                duration: 500
                easing.type: Easing.OutBack
            }
        }
    }
}
