.pragma library

function get(type) {
    var properties = {
        "Music": {
            iconName: "stock_music"
        },
        "Picture": {
            iconName: "stock_image"
        },
        "Video": {
            iconName: "stock_video"
        },
        "Document": {
            iconName: "stock_note"
        }
    };
    return properties[type];
}
