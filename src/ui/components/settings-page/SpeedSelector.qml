import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import "../common/Format.js" as Format
import "../common/PossibleSpeedValues.js" as PossibleSpeedValues

Column {
    id: root
    spacing: units.gu(1)

    property string type: ""

    property var availableSpeedsBPSModel: PossibleSpeedValues.get()

    property int cellularBPS: 0
    property int wifiBPS: 0

    property bool enableDifferentCellularWifi: false

    QtObject {
        id: internal
        property int cellularIndex: availableSpeedsBPSModel.indexOf(cellularBPS)
        property int wifiIndex: availableSpeedsBPSModel.indexOf(wifiBPS)
        property bool isCurrentlyExpanded: false
    }

    Connections {
        target: sameForBothCheckBox
        onCheckedChanged: {
            if (sameForBothCheckBox.checked) {
                root.cellularBPS = root.wifiBPS
            }
        }
    }

    Item {
        id: top
        anchors {
            left: parent.left
            right: parent.right
        }
        height: title.height

        Label {
            id: title
            anchors {
                left: parent.left
                verticalCenter: parent.verticalCenter
            }
            text: "Max " + type + " speed"
        }

        Row {
            anchors {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }
            spacing: units.gu(0.5)
            visible: root.enableDifferentCellularWifi

            Label {
                anchors {
                    verticalCenter: parent.verticalCenter
                }

                text: "Same for WiFi/cellular"
                fontSize: "x-small"
            }
            CheckBox {
                id: sameForBothCheckBox
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                checked: root.cellularBPS === root.wifiBPS
            }
        }
    }

    function bpsToString(bps) {
        if (bps === 0) {
            return "Unlimited"
        }
        else {
            return Format.formatBytes(bps) + "/s"
        }
    }

    property var availableSpeedsModel: availableSpeedsBPSModel.map(bpsToString)

    Item {
        height: wifiOrBothColumn.height
        anchors {
            left: parent.left
            right: parent.right
        }

        Column {
            id: wifiOrBothColumn

            anchors {
                left: parent.left
            }
            spacing: units.gu(0.3)
            width: sameForBothCheckBox.checked ? parent.width : parent.width / 2 - units.gu(1)

            OptionSelector {
                id: wifiOrBothSpeedSelector
                anchors {
                    left: parent.left
                    right: parent.right
                }
                selectedIndex: internal.wifiIndex

                onSelectedIndexChanged: {
                    if (sameForBothCheckBox.checked) {
                        root.wifiBPS = availableSpeedsBPSModel[selectedIndex]
                        root.cellularBPS = availableSpeedsBPSModel[selectedIndex]
                    }
                    else {
                        root.wifiBPS = availableSpeedsBPSModel[selectedIndex]
                    }
                }

                Connections {
                    target: internal
                    onIsCurrentlyExpandedChanged: {
                        wifiOrBothSpeedSelector.currentlyExpanded = internal.isCurrentlyExpanded
                    }
                    onWifiIndexChanged: {
                        wifiOrBothSpeedSelector.selectedIndex = internal.wifiIndex
                    }
                }

                onCurrentlyExpandedChanged: {
                    internal.isCurrentlyExpanded = currentlyExpanded
                }

                expanded: false
                model: availableSpeedsModel
                currentlyExpanded: internal.isCurrentlyExpanded
            }

            //FIXME: no idea why this item is needed, but without it the OptionSelector doesn't expand properly
            Item {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: units.gu(0.1)
                visible: sameForBothCheckBox.checked
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "WiFi"
                fontSize: "small"
                visible: !sameForBothCheckBox.checked
            }
        }

        Loader {
            anchors {
                right: parent.right
            }
            width: parent.width / 2 - units.gu(1)

            sourceComponent: sameForBothCheckBox.checked ? null : cellularColumnComponent
        }

        Component {
            id: cellularColumnComponent

            Column {
                id: cellularColumn
                spacing: units.gu(0.3)

                OptionSelector {
                    id: cellularSpeedSelector
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    selectedIndex: internal.cellularIndex

                    onSelectedIndexChanged: {
                        root.cellularBPS = availableSpeedsBPSModel[selectedIndex]
                    }

                    Connections {
                        target: internal
                        onIsCurrentlyExpandedChanged: {
                            cellularSpeedSelector.currentlyExpanded = internal.isCurrentlyExpanded
                        }
                        onCellularIndexChanged: {
                            cellularSpeedSelector.selectedIndex = internal.cellularIndex
                        }
                    }

                    onCurrentlyExpandedChanged: {
                        internal.isCurrentlyExpanded = currentlyExpanded
                    }

                    expanded: false
                    model: availableSpeedsModel
                    currentlyExpanded: internal.isCurrentlyExpanded
                }
                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Cellular"
                    fontSize: "small"
                    visible: !sameForBothCheckBox.checked
                }
            }
        }
    }
}
