import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0

Page {
    title: "Settings"

    property var /*DowNowSettings*/ settings: globalSettings
    property var /*SearchSettings*/ searchSettings: globalSearchSettings

    flickable: flick
    Flickable {
        id: flick
        anchors {
            fill: parent
        }

        contentHeight: column.height + column.anchors.topMargin + column.anchors.bottomMargin
        contentWidth: width

        Column {
            id: column
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                leftMargin: units.gu(1)
                rightMargin: units.gu(1)
                topMargin: units.gu(2)
                bottomMargin: units.gu(2)
            }

            spacing: units.gu(2)
            SpeedSelector {
                id: downloadSpeedSelector
                anchors {
                    left: parent.left
                    right: parent.right
                }

                type: "download"
                cellularBPS: settings.maxCellularDownloadSpeedBPS
                wifiBPS: settings.maxWifiDownloadSpeedBPS

                onCellularBPSChanged: settings.maxCellularDownloadSpeedBPS = cellularBPS
                onWifiBPSChanged: settings.maxWifiDownloadSpeedBPS = wifiBPS
            }

            SpeedSelector {
                id: uploadSpeedSelector
                anchors {
                    left: parent.left
                    right: parent.right
                }

                type: "upload"
                cellularBPS: settings.maxCellularUploadSpeedBPS
                wifiBPS: settings.maxWifiUploadSpeedBPS

                onCellularBPSChanged: settings.maxCellularUploadSpeedBPS = cellularBPS
                onWifiBPSChanged: settings.maxWifiUploadSpeedBPS = wifiBPS
            }

            Loader {
                anchors {
                    left: parent.left
                    right: parent.right
                }

                sourceComponent: app.supportsRunningInBackground ? this.comp : null

                property Component comp: Component {
                    id: comp
                    Item {
                        height: runInBackgroundCheckbox.height

                        Label {
                            id: runInBackgroundTitle
                            anchors {
                                left: parent.left
                                verticalCenter: parent.verticalCenter
                            }
                            text: i18n.tr("Run in background")
                        }

                        Switch {
                            id: runInBackgroundCheckbox
                            anchors {
                                right: parent.right
                                verticalCenter: parent.verticalCenter
                            }

                            checked: globalSettings.runInBackground
                            onCheckedChanged: globalSettings.runInBackground = checked
                        }
                    }
                }
            }
        }
    }
}
