import QtQuick 2.0
import Ubuntu.Components 1.3

Image {
    id: root
    property bool lessThan: true

    anchors {
        verticalCenter: parent.verticalCenter
    }
    source: "../../images/" + (root.lessThan ? "less" : "more") + "-than-or-equal-to-sign.svg"
    sourceSize.height: parent.height - units.gu(4.5)
}
