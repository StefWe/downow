import QtQuick 2.0
import QtQuick.Layouts 1.0
import Ubuntu.Components 1.3
import com.nogzatalz.Downow 1.0

Rectangle {
    id: root

    height: units.gu(6)
    color: "white"

    property real downLimitSpeedBPS: app.maxDownloadSpeedBPS
    property real upLimitSpeedBPS: app.maxUploadSpeedBPS

    property real downSpeedBPS: globalSession.downloadSpeedBPS
    property real upSpeedBPS: globalSession.uploadSpeedBPS

    Item {
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            top: parent.top
        }

        readonly property int recommenedWidth: units.gu(49)
        width: root.width >= recommenedWidth ? recommenedWidth : root.width

        CurrentSpeedIndicator {
            id: downloadSpeedIndicator
            anchors {
                left: parent.left
                leftMargin: units.gu(1)
                top: parent.top
                bottom: parent.bottom
            }
            layoutDirection: Qt.LeftToRight
            icon: "../../images/download-icon.svg"
            bytesPerSecond: root.downSpeedBPS
        }

        LessMoreThan {
            id: lessThan
            anchors {
                left: downloadSpeedIndicator.right
                leftMargin: units.gu(1)
                verticalCenter: parent.verticalCenter
            }

            lessThan: true
        }

        Row {
            anchors {
                left: lessThan.right
                right: moreThan.left
                top: parent.top
                bottom: parent.bottom
                leftMargin: units.gu(0.5)
                rightMargin: units.gu(0.5)
            }
            spacing: units.gu(0.5)

            SpeedLimitSelector {
                id: downSpeedLimitSelector
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                width: (parent.width - parent.spacing) / 2

                limitBPS: root.downLimitSpeedBPS

                onSelectedSpeed: {
                    app.maxDownloadSpeedBPS = speedBPS
                }
            }

            SpeedLimitSelector {
                id: upSpeedLimitSelector
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                width: (parent.width - parent.spacing) / 2

                limitBPS: root.upLimitSpeedBPS

                onSelectedSpeed: {
                    app.maxUploadSpeedBPS = speedBPS
                }
            }
        }

        LessMoreThan {
            id: moreThan
            anchors {
                right: uploadSpeedIndicator.left
                rightMargin: units.gu(1)
                verticalCenter: parent.verticalCenter
            }

            lessThan: false
        }

        CurrentSpeedIndicator {
            id: uploadSpeedIndicator
            anchors {
                right: parent.right
                rightMargin: units.gu(1)
                top: parent.top
                bottom: parent.bottom
            }
            layoutDirection: Qt.RightToLeft
            icon: "../../images/upload-icon.svg"
            bytesPerSecond: root.upSpeedBPS
        }
    }
}
