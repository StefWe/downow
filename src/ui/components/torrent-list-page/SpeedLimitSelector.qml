import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0
import "../common/Format.js" as Format
import "../common/PossibleSpeedValues.js" as PossibleSpeedValues

UbuntuShape {
    id: root
    color: mouseArea.pressed ? "#999" : "#bbb"

    height: units.gu(3)

    Behavior on color {
        PropertyAnimation {
            duration: UbuntuAnimation.SnapDuration
            easing: UbuntuAnimation.StandardEasing
        }
    }

    signal selectedSpeed(int speedBPS)

    property int limitBPS: 0
    readonly property bool unlimited: limitBPS <= 0

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: PopupUtils.open(popoverComponent, root)
    }

    Image {
        id: pullDownIndicator
        anchors {
            left: parent.left
            leftMargin: units.gu(0.5)
            verticalCenter: parent.verticalCenter
        }

        source: "../../images/pull-down-icon.svg"
        sourceSize.height: units.gu(0.7)
    }

    Text {
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: pullDownIndicator.right
            right: parent.right
            leftMargin: units.gu(0.3)
            rightMargin: units.gu(0.5)
            topMargin: units.gu(0.6)
            bottomMargin: units.gu(0.6)
        }
        visible: !root.unlimited
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: Format.formatBytes(root.limitBPS) + "/s"
        font.family: "ubuntu"
        fontSizeMode: Text.Fit
        font.pixelSize: units.gu(1.5)
        minimumPixelSize: 1
    }

    Image {
        anchors {
            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }

        visible: root.unlimited
        source: "../../images/infinity.svg"
        sourceSize.height: units.gu(1)
    }

    Component {
        id: popoverComponent

        Popover {
            id: popover

            ListView {
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                height: units.gu(40)

                currentIndex: PossibleSpeedValues.get().indexOf(limitBPS)
                highlightFollowsCurrentItem: true

                highlight: Component {
                    Rectangle {
                        color: UbuntuColors.orange
                    }
                }

                model: PossibleSpeedValues.get()
                delegate: ListItem.Empty {
                    id: listItem
                    property bool selected: limitBPS === modelData

                    Label {
                        anchors.centerIn: parent
                        fontSize: "medium"
                        text: modelData === 0 ? "Unlimited" : Format.formatBytes(modelData) + "/s"
                        color: listItem.selected ? "white" : "#5c5c5c"
                    }

                    onClicked: {
                        root.selectedSpeed(modelData);
                        PopupUtils.close(popover);
                    }
                }
            }
        }
    }
}
