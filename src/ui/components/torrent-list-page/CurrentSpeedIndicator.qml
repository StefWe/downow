import QtQuick 2.0
import Ubuntu.Components 1.3
import "../common/Format.js" as Format

Row {
    id: root
    spacing: units.gu(0.5)

    property url icon
    property int bytesPerSecond

    Image {
        id: sign
        anchors {
            verticalCenter: parent.verticalCenter
        }
        sourceSize.height: units.gu(2)
        source: root.icon
    }
    Text {
        anchors {
            verticalCenter: parent.verticalCenter
        }
        width: units.gu(8)
        horizontalAlignment: root.layoutDirection === Qt.LeftToRight ? Text.AlignLeft : Text.AlignRight
        verticalAlignment: Qt.AlignVCenter
        text: Format.formatBytes(root.bytesPerSecond) + "/s"
        font.family: "ubuntu"
        fontSizeMode: Text.Fit
        color: "#636363"
        font.pixelSize: units.gu(1.8)
    }
}
