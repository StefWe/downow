import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0
import Ubuntu.Components.Popups 1.0
import QtGraphicalEffects 1.0
import "../common"
import "../torrent-list-item"
import ".."

Page {
    id: root
    title: i18n.tr("DowNow")

    signal openTorrentDetailsPage(Torrent torrent)

    property DowNowTheme downowTheme
    property Action addAction
    property Action settingsAction
    property Action resumeAction
    property Action pauseAction

    property /*Torrent*/ var currentlyFocusedTorrent

    TorrentListModel {
        id: torrentsModel
        session: globalSession
        filter: {
            var indexToFilter = [TorrentListModel.All, TorrentListModel.OnlyDownloading, TorrentListModel.OnlyCompleted];
            return indexToFilter[head.sections.selectedIndex];
        }
    }

    head {
        actions: [resumeAction, pauseAction, addAction]
        sections {
            model: [
                i18n.tr("All"),
                i18n.tr("Downloading"),
                i18n.tr("Complete")
            ]
        }
    }

    Action {
        id: showInFileManagerAction
        text: "Show in file manager"
        visible: app.showInFileManagerEnabled
        onTriggered: root.openTorrentInFileManager(currentlyFocusedTorrent)
    }

    Action {
        id: deleteFilesAndRemoveAction
        text: "Remove and delete files"
        onTriggered: currentlyFocusedTorrent.removeAndDeleteFiles()
    }

    Action {
        id: removeAction
        text: "Remove"
        onTriggered: currentlyFocusedTorrent.remove()
    }

    function openTorrentMenu(torrentListItem, torrent) {
        root.currentlyFocusedTorrent = torrent
        PopupUtils.open(longTapOptionsComponent, torrentListItem)
    }

    function openTorrentInFileManager(torrent) {
        Qt.openUrlExternally("file://" + torrent.savePath);
    }

    function openTorrent(torrent) {
        Qt.openUrlExternally("file://" + torrent.savePath + "/" + torrent.name)
    }

    flickable: listView

    ListView {
        id: listView
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        height: parent.height - speedPanel.height

        model: torrentsModel

        delegate: TorrentListItem {
            id: torrentListItem
            downowTheme: root.downowTheme
            torrent: model.torrent

            onClicked: root.openTorrentDetailsPage(torrent)
            onPressAndHold: root.openTorrentMenu(torrentListItem, torrent)

            /*
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton

                onPressAndHold: root.openTorrentMenu(torrentListItem, torrent)
                onClicked: {
                    if (mouse.button === Qt.RightButton) {
                        openTorrentMenu(torrentListItem, torrent)
                    }
                    else {
                        mouse.accepted = false
                    }
                }

                preventStealing: true
                propagateComposedEvents: true
            }
            */
        }
    }

    DropShadow {
        anchors.fill: speedPanelContainer
        horizontalOffset: 0
        verticalOffset: -units.gu(0.5)
        samples: 24
        radius: 30
        spread: 0
        fast: false
        cached: true
        color: Qt.rgba(0, 0, 0, 0.2)
        source: speedPanelContainer
    }

    Item {
        id: speedPanelContainer
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: speedPanel.height + units.gu(2)

        SpeedPanel {
            id: speedPanel
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
        }
    }

    Column {
        visible: torrentsModel.count === 0
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            right: parent.right
        }
        spacing: units.gu(1)

        Label {
            anchors {
                left: parent.left
                right: parent.right
            }

            text: {
                switch (torrentsModel.filter) {
                case TorrentListModel.OnlyDownloading:
                    return i18n.tr("No downloading torrents");
                case TorrentListModel.OnlyCompleted:
                    return i18n.tr("No complete torrents");
                default:
                    return i18n.tr("No torrents");
                }
            }

            fontSize: "large"
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }
        Label {
            anchors {
                left: parent.left
                right: parent.right
            }
            visible: torrentsModel.filter === TorrentListModel.All || torrentsModel.filter === TorrentListModel.OnlyDownloading

            text: i18n.tr("You can add torrents using the '+' button")
            fontSize: "medium"
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Component {
        id: longTapOptionsComponent

        ActionSelectionPopover {
            id: longTapOptionsPopover
            actions: [
                showInFileManagerAction,
                deleteFilesAndRemoveAction,
                removeAction
            ].filter(function(action) {
                return action.visible;
            })
        }
    }
}
