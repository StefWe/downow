import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0
import "../common"

Page {
    id: root
    title: "About"
    flickable: null

    property var /*DowNowSettings*/ settings: globalSettings

    Flickable {
        anchors.fill: parent

        contentWidth: width
        contentHeight: content.height

        clip: true

        Item {
            id: content
            width: parent.width
            height: childrenRect.height

            UbuntuShape {
                id: appIcon
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: units.gu(1)
                }

                radius: "medium"
                width: units.gu(10)
                height: width

                image: Image {
                    source: "../../images/downow-icon.svg"
                }
            }

            Label {
                id: downowLabel
                anchors {
                    top: appIcon.bottom
                    horizontalCenter: parent.horizontalCenter
                    topMargin: units.gu(1)
                }

                text: "DowNow " + root.settings.versionMajor + "." + root.settings.versionMinor + "." + root.settings.versionRevision
            }

            Column {
                anchors {
                    top: downowLabel.bottom
                    left: parent.left
                    right: parent.right
                    topMargin: units.gu(1)
                }

                ListItem.ThinDivider {}

                ListItem.Header {
                    text: "Developed by"
                }

                Item {
                    width: parent.width
                    height: units.gu(1)
                }

                Column {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    spacing: units.gu(1)

                    ByPerson {
                        name: "Tal Zion"
                        email: "talzion12@gmail.com"
                        imageUrl: "../../images/tal.jpg"
                    }
                    ByPerson {
                        name: "Noga Dikerman"
                        email: "nogadi@me.com"
                        imageUrl: "../../images/noga.jpg"
                    }
                }

                Item {
                    width: parent.width
                    height: units.gu(1)
                }

                ListItem.ThinDivider {}

                ListItem.Header {
                    text: "Open source licenses"
                }
                CaptionListItem {
                    text: "DowNow uses the excellent libtorrent by rasterbar which can be found <a href=\"http://sourceforge.net/projects/libtorrent\">here</a>."
                }

                ListItem.Header {
                    text: "Contributions"
                }
                CaptionListItem {
                    text: "DowNow is open source software and is provided under the GNU GPL v3 license.<br />Code contributions and bug reports are very welcome and can be managed on DowNow's <a href=\"https://launchpad.net/downow\">Launchpad page</a>."
                }
            }
        }
    }
}
