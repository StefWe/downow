import QtQuick 2.0
import Ubuntu.Components 1.3

Item {
    id: root
    implicitWidth: parent.width

    height: image.height

    property string name: ""
    property string email: ""
    property url imageUrl

    UbuntuShape {
        id: image
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: units.gu(1)
        }

        height: units.gu(6)
        width: height

        image: Image {
            source: root.imageUrl
            fillMode: Image.PreserveAspectCrop
            sourceSize.width: image.width
            sourceSize.height: image.height
        }
    }

    Column {
        anchors {
            left: image.right
            right: parent.right
            verticalCenter: parent.verticalCenter
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }
        spacing: units.gu(0.5)

        Label {
            anchors {
                left: parent.left
                right: parent.right
            }
            text: root.name
            fontSize: "medium"
        }
        Label {
            anchors {
                left: parent.left
                right: parent.right
            }
            text: root.email
            fontSize: "small"
        }
    }
}
