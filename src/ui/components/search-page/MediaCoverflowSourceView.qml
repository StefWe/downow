import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0
import "../common/Format.js" as Format

Item {
    id: root
    property SearchResultsListModel model
    signal torrentLinkSelected(string magnetLink)

    clip: true

    height: model.count > 0 ? resultsCarousel.height : units.gu(0)
//    opacity: model.count > 0 ? 1 : 0
//    visible: opacity !== 0

//    Behavior on height {
//        NumberAnimation {
//            duration: UbuntuAnimation.BriskDuration
//            easing.type: Easing.InOutQuad
//        }
//    }
//    Behavior on opacity {
//        NumberAnimation {
//            duration: UbuntuAnimation.BriskDuration
//            easing.type: Easing.InOutQuad
//        }
//    }

    ListView {
        id: resultsCarousel
        anchors {
            left: parent.left
            right: parent.right
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }

        height: units.gu(35)
        orientation: ListView.Horizontal

        model: root.model
        spacing: units.gu(2)

        delegate: Item {
            width: units.gu(25)
            height: resultsCarousel.height

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                onClicked: root.torrentLinkSelected(model.result.magnetLink)
            }

            CrossFadeImage {
                id: coverImage
                anchors.fill: parent

                source: model.result.coverImageUrl
                fillMode: Image.PreserveAspectCrop
            }

            Loader {
                anchors {
                    top: coverImage.top
                    left: coverImage.left
                    right: coverImage.right
                    bottom: bottomInfo.top
                }

                sourceComponent: coverImage.status !== Image.Ready ? comp : null
                Component {
                    id: comp

                    Rectangle {
                        gradient: Gradient {
                            GradientStop {
                                position: 0
                                color: Qt.rgba(0.9, 0.9, 0.9, 1)
                            }
                            GradientStop {
                                position: 1
                                color: Qt.rgba(0.8, 0.8, 0.8, 1)
                            }
                        }

                        Icon {
                            anchors.centerIn: parent
                            name: "stock_video"
                            width: units.gu(10)
                            height: width
                        }
                    }
                }
            }

            Rectangle {
                id: bottomInfo
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
                height: units.gu(9)
                color: Qt.rgba(0, 0, 0, 0.8)

                Label {
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                        margins: units.gu(1)
                    }

                    text: model.result.name
                    fontSize: "medium"
                    color: "white"
                    elide: Text.ElideRight
                    maximumLineCount: 2
                    wrapMode: Text.WordWrap
                }

                Row {
                    anchors {
                        bottom: parent.bottom
                        right: parent.right
                        margins: units.gu(1)
                    }

                    spacing: units.gu(1)

                    Label {
                        text: Format.formatKB(model.result.sizeKB)
                        fontSize: "small"
                        color: "white"
                    }

                    Label {
                        text: Format.formatSeeds(model.result.seeders)
                        fontSize: "small"
                        color: "white"
                    }

                    Label {
                        text: model.result.genres[0]
                        fontSize: "small"
                        color: "white"
                    }

                    Label {
                        text: model.result.quality
                        fontSize: "small"
                        color: "white"
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                color: mouseArea.pressed ? Qt.rgba(0, 0, 0, 0.2) : Qt.rgba(0, 0, 0, 0)
                Behavior on color {
                    ColorAnimation { duration: UbuntuAnimation.SnapDuration }
                }
            }
        }
    }
}
