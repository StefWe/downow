
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0
import "../common/Format.js" as Format

Column {
    id: root
    clip: true

    property SearchResultsListModel model
    signal torrentLinkSelected(string magnetLink)

    Repeater {
        model: root.model

        delegate: ListItem.Subtitled {
            text: model.result.name
            subText: Format.formatKB(model.result.sizeKB) + " • " + model.result.seeders + " seeders";

            onClicked: {
                root.torrentLinkSelected(model.result.magnetLink);
            }
        }
    }
}
