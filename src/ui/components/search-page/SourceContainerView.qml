import QtQuick 2.0

import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0
import "../common/Format.js" as Format

Column {
    id: root
    property SearchResultsListModel model
    property Component resultsComponent

    anchors {
        left: parent.left
        right: parent.right
    }

    visible: {
        if (model.query === "") {
            return false;
        }
        else {
            return model.lastLoadedQuery !== "" || model.isLoading;
        }
    }

    spacing: units.gu(1)

    ListItem.Header {
        text: model.source.type + " results:"
    }

    Loader {
        anchors{
            left: parent.left
            right: parent.right
        }

        sourceComponent: {
            if (model.isLoading) {
                return activityIndicatorComponent;
            }
            else {
                if (model.source.error !== SearchSource.None) {
                    return errorLabelComponent;
                }
                else if (model.count === 0) {
                    return noResultsLabelComponent;
                }
                else {
                    return resultsComponent;
                }
            }
        }
    }

    Component {
        id: activityIndicatorComponent

        Column {
            ActivityIndicator {
                anchors.horizontalCenter: parent.horizontalCenter
                running: model.isLoading
                visible: running
            }
        }
    }

    Component {
        id: noResultsLabelComponent

        Label {
            text: "No results"
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Component {
        id: errorLabelComponent

        Label {
            text: "I couldn't connect to " + model.type + ". Please check your internet connection."
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
        }
    }
}
