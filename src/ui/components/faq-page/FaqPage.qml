import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.0 as ListItem
import com.nogzatalz.Downow 1.0

Page {
    id: root
    title: "FAQ"

    ListModel {
        id: questionAnswersModel

        Component.onCompleted: {
            append({
                questionText: "Where are downloaded files located?",
                answerText: "You can find all files downloaded by DowNow in this directory: <a href=\"file://" + app.torrentsSavePath + "\">" + app.torrentsSavePath + "</a>"
            });
            append({
                questionText: "Can I move downloaded files to other directories?",
                answerText: "Yes, but in most cases this is not needed because you can always tap on a file in the torrent page to open the file in another app using content hub. But if for some reason you still want to move a file you'll have to download the <a href=\"scope://com.canonical.scopes.clickstore?q=File Manager\">file manager app</a> and move it yourself. DowNow is a confined application and therefore it can only write to a specific set of directories."
            });
            append({
                questionText: "Why doesn't DowNow download in the background?",
                answerText: "This is currently not possible because of the <a href=\"https://samohtv.wordpress.com/2013/08/15/application-lifecycle-model-policies/\">application lifecycle model</a>. A high importance <a href=\"https://bugs.launchpad.net/downow/+bug/1361823\">bug report</a> describing this issue has been reported to DowNow's Launchpad page. We will add this feature when it becomes possible."
            });
        }
    }

    UbuntuListView {
        id: list
        anchors.fill: parent
        model: questionAnswersModel

        delegate: ListItem.Empty {
            height: question.height + units.gu(2)

            highlightWhenPressed: false

            Column {
                id: question
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    leftMargin: units.gu(1)
                    rightMargin: units.gu(1)
                }
                spacing: units.gu(1)

                Label {
                    id: questionLabel
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: "<b>" + questionText + "</b>"
                    wrapMode: Text.WordWrap
                }
                Label {
                    id: answerLabel
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: answerText
                    wrapMode: Text.WordWrap
                    onLinkActivated: Qt.openUrlExternally(link)
                    linkColor: UbuntuColors.orange
                }


            }
        }
    }
}
