
#include <downow/config.hpp>

#if DOWNOW_FILE_OPENER_CONTENT_HUB

#include "ContentHubOpener.hpp"

#include "ContentHubPeerProvider.hpp"

#include <com/ubuntu/content/hub.h>
#include <com/ubuntu/content/item.h>
#include <com/ubuntu/content/transfer.h>

#include <boost/log/trivial.hpp>
#include <unordered_map>

#include <downow/common/Utils.hpp>
#include <downow/torrent-frontend/FileType.hpp>

namespace cuc = com::ubuntu::content;

namespace downow { namespace ui {

static cuc::Type contentHubTypeForFileType(torrent_frontend::FileType const& fileType) {
    using namespace torrent_frontend;
    static cuc::Type contentHubTypeForFileType[FileType::count()] = {
        cuc::Type::Known::documents(),
        cuc::Type::Known::pictures(),
        cuc::Type::Known::music(),
        cuc::Type::Known::contacts(),
        cuc::Type::Known::videos(),
        cuc::Type::Known::links()
    };
    return contentHubTypeForFileType[fileType.enumValue()];
}

ContentHubOpener::ContentHubOpener(ContentHubPeerProvider * peerProvider) :
    _peerProvider(peerProvider) {}

void ContentHubOpener::openFiles(FileOpener::FileInfoInputRange const& fileInfoRange) {
    if (fileInfoRange.begin() == fileInfoRange.end()) {
        return;
    }

    // check that the files exist
    for (QFileInfo const& fileInfo : fileInfoRange) {
        if (!fileInfo.exists()) {
            BOOST_THROW_EXCEPTION(FileDoesntExist());
        }
    }

    cuc::Type type = [&] {
        try {
            return contentHubTypeForFileType(torrent_frontend::FileType {*fileInfoRange.begin()});
        }
        catch (torrent_frontend::FileType::CouldntFindFileTypeForMimeType const&) {
            return cuc::Type::unknown();
        }
    }();

    QPointer<cuc::Hub> hub = cuc::Hub::Client::instance();

    QVector<cuc::Peer> destinationPeers = hub->known_destinations_for_type(type);
    if (destinationPeers.size() == 0) {
        BOOST_LOG_TRIVIAL(debug) << "Couldn't find any destinations for type: " << type.id();
        BOOST_THROW_EXCEPTION(CouldntOpenFile());
    }
    else {
        QVector<cuc::Item> items;
        for (QFileInfo const& fileInfo : fileInfoRange) {
            items.push_back({
                QUrl::fromLocalFile(fileInfo.absoluteFilePath())
            });
        }

        auto onHasPeer = [items, type, hub] (cuc::Peer peer) {
            BOOST_LOG_TRIVIAL(debug) << "Opening with peer " << peer.id();
            cuc::Transfer * transfer = hub->create_export_to_peer_for_type(peer, type);
            QObject::connect(transfer, &cuc::Transfer::stateChanged, [transfer] {
                BOOST_LOG_TRIVIAL(debug) << "transfer state changed to " << transfer->state();
            });
            transfer->setSelectionType(items.size() == 1 ? cuc::Transfer::single : cuc::Transfer::multiple);
            transfer->charge(items);
        };

        if (destinationPeers.size() == 1) {
            onHasPeer(destinationPeers[0]);
        }
        else if (destinationPeers.size() > 1) {
            QFuture<cuc::Peer> futurePeer = _peerProvider->getPeerForType(type);

            onFutureFinished(futurePeer, this, [onHasPeer] (QFuture<cuc::Peer> future) {
                onHasPeer(future.result());
            });
        }
    }
}

bool ContentHubOpener::canOpenFile(QFileInfo const& fileInfo) {
    if (!fileInfo.exists()) {
        return false;
    }

    cuc::Type type = contentHubTypeForFileType({fileInfo});
    QPointer<cuc::Hub> hub = cuc::Hub::Client::instance();
    QVector<cuc::Peer> destinationPeers = hub->known_destinations_for_type(type);
    return destinationPeers.size() > 0;
}

} }

#endif
