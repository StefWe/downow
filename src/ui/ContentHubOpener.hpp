#ifndef DOWNOW_UI_CONTENT_HUB_OPENER
#define DOWNOW_UI_CONTENT_HUB_OPENER

#include <downow/config.hpp>

#if !DOWNOW_FILE_OPENER_CONTENT_HUB
#   error content hub backend not selected for compilation
#else

#include "FileOpener.hpp"

#include <qt5/QtCore/QObject>

namespace downow { namespace ui {

struct ContentHubPeerProvider;

struct ContentHubOpener : QObject, FileOpener {
private:
    Q_OBJECT

public:
    ContentHubOpener(ContentHubPeerProvider *);
    void openFiles(FileInfoInputRange const&) override;
    bool canOpenFile(QFileInfo const&) override;

private:
    ContentHubPeerProvider * _peerProvider;    
};

} }

#endif
#endif
