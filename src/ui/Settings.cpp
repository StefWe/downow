#include "Settings.hpp"

#include <qt5/QtCore/QSettings>
#include <qt5/QtCore/QPointer>
#include <boost/log/trivial.hpp>

#include <downow/config.hpp>
#include <downow/common/Utils.hpp>

namespace downow { namespace ui {

static char const* const lastVersionMajorKey = "lastVersion/major";
static char const* const lastVersionMinorKey = "lastVersion/minor";
static char const* const lastVersionRevisionKey = "lastVersion/revision";
static char const* const maxWifiDownloadSpeedBPSKey = "maxWifiDownloadSpeedBPS";
static char const* const maxCellularDownloadSpeedBPSKey = "maxCellularDownloadSpeedBPS";
static char const* const maxWifiUploadSpeedBPSKey = "maxWifiUploadSpeedBPS";
static char const* const maxCellularUploadSpeedBPSKey = "maxCellularUploadSpeedBPS";
static char const* const downloadDirKey = "downloadDir";
static char const* const runInBackgroundKey = "runInBackground";

static QPointer<Settings> _instance = nullptr;

Settings &Settings::instance() {
    if (!_instance) {
        BOOST_THROW_EXCEPTION(InstanceDoesntExistException());
    }

    return *_instance;
}

Settings::Settings(std::shared_ptr<QSettings> settings, QObject * parent) :
    QObject(parent),
    _settings(settings) {

    if (_instance) {
        BOOST_THROW_EXCEPTION(InstanceAlreadyExistsException());
    }

    if (_settings->contains(lastVersionMajorKey)) {
        _lastVersion = Version {
            _settings->value(lastVersionMajorKey).toInt(),
            _settings->value(lastVersionMinorKey).toInt(),
            _settings->value(lastVersionRevisionKey).toInt()
        };
    }

    _settings->setValue(lastVersionMajorKey, getVersionMajor());
    _settings->setValue(lastVersionMinorKey, getVersionMinor());
    _settings->setValue(lastVersionRevisionKey, getVersionRevision());

    // Before revision 57 we used -1 as an "Unlimited" option
    // for max download/upload speeds.
    // We now use 0 in order to be able to use unsigned types
    // to store speed
    if (!_lastVersion || _lastVersion->revision <= 57) {
        if (_settings->value(maxWifiDownloadSpeedBPSKey, 0) < 0) {
            _settings->remove(maxWifiDownloadSpeedBPSKey);
        }
        if (_settings->value(maxWifiUploadSpeedBPSKey, 0) < 0) {
            _settings->remove(maxWifiUploadSpeedBPSKey);
        }
        if (_settings->value(maxCellularDownloadSpeedBPSKey, 0) < 0) {
            _settings->remove(maxCellularDownloadSpeedBPSKey);
        }
        if (_settings->value(maxCellularUploadSpeedBPSKey, 0) < 0) {
            _settings->remove(maxCellularUploadSpeedBPSKey);
        }
    }

    _instance = this;
}

int Settings::getMaxWifiDownloadSpeedBPS() const {
    return _settings->value(maxWifiDownloadSpeedBPSKey, 0).toInt();
}

int Settings::getMaxCellularDownloadSpeedBPS() const {
    return _settings->value(maxCellularDownloadSpeedBPSKey, 0).toInt();
}

int Settings::getMaxWifiUploadSpeedBPS() const {
    return _settings->value(maxWifiUploadSpeedBPSKey, 0).toInt();
}

int Settings::getMaxCellularUploadSpeedBPS() const {
    return _settings->value(maxCellularUploadSpeedBPSKey, 0).toInt();
}

void Settings::setMaxWifiDownloadSpeedBPS(int limitBPS) {
    if (getMaxWifiDownloadSpeedBPS() == limitBPS) {
        return;
    }

    BOOST_LOG_TRIVIAL(debug) << "Settings: set max wifi download speed to " << limitBPS;

    if (limitBPS > 0) {
        _settings->setValue(maxWifiDownloadSpeedBPSKey, limitBPS);
    }
    else {
        _settings->remove(maxWifiDownloadSpeedBPSKey);
    }

    Q_EMIT maxWifiDownloadSpeedBPSChanged();
}

void Settings::setMaxCellularDownloadSpeedBPS(int limitBPS) {
    if (getMaxCellularDownloadSpeedBPS() == limitBPS) {
        return;
    }

    BOOST_LOG_TRIVIAL(debug) << "Settings: set max cellular download speed to " << limitBPS;

    if (limitBPS > 0) {
        _settings->setValue(maxCellularDownloadSpeedBPSKey, limitBPS);
    }
    else {
        _settings->remove(maxCellularDownloadSpeedBPSKey);
    }

    Q_EMIT maxCellularDownloadSpeedBPSChanged();
}

void Settings::setMaxWifiUploadSpeedBPS(int limitBPS) {
    if (getMaxWifiUploadSpeedBPS() == limitBPS) {
        return;
    }

    BOOST_LOG_TRIVIAL(debug) << "Settings: set max wifi upload speed to " << limitBPS;

    if (limitBPS > 0) {
        _settings->setValue(maxWifiUploadSpeedBPSKey, limitBPS);
    }
    else {
        _settings->remove(maxWifiUploadSpeedBPSKey);
    }
    Q_EMIT maxWifiUploadSpeedBPSChanged();
}

void Settings::setMaxCellularUploadSpeedBPS(int limitBPS) {
    if (getMaxCellularUploadSpeedBPS() == limitBPS) {
        return;
    }

    BOOST_LOG_TRIVIAL(debug) << "Settings: set max cellular upload speed to " << limitBPS;

    if (limitBPS > 0) {
        _settings->setValue(maxCellularUploadSpeedBPSKey, limitBPS);
    }
    else {
        _settings->remove(maxCellularUploadSpeedBPSKey);
    }
    Q_EMIT maxCellularUploadSpeedBPSChanged();
}

QString Settings::getDownloadDir() const {
    return _settings->value(downloadDirKey, QString()).toString();
}

void Settings::setDownloadDir(QString downloadDir) {
    if (downloadDir == getDownloadDir()) {
        return;
    }

    _settings->setValue(downloadDirKey, downloadDir);
    Q_EMIT downloadDirChanged();
}

#if DOWNOW_APP_INDICATOR
bool Settings::getRunInBackground() const {
    return _settings->value(runInBackgroundKey, true).toBool();
}
void Settings::setRunInBackground(bool runInBackground) {
    bool old = getRunInBackground();
    _settings->setValue(runInBackgroundKey, runInBackground);

    if (old != runInBackground) {
        Q_EMIT runInBackgroundChanged();
    }
}
#endif

int Settings::getVersionMajor() const {
    return DOWNOW_VERSION_MAJOR;
}

int Settings::getVersionMinor() const {
    return DOWNOW_VERSION_MINOR;
}

int Settings::getVersionRevision() const {
    return DOWNOW_VERSION_REVISION;
}

} }
