#ifndef DOWNOW_UI_FILE_OPENER
#define DOWNOW_UI_FILE_OPENER

#include <qt5/QtCore/QFileInfo>
#include <boost/exception/all.hpp>
#include <boost/range/any_range.hpp>
#include <downow/torrent-frontend/FileType.hpp>
#include <array>

namespace downow { namespace ui {

struct FileOpener {
    struct CouldntOpenFile : virtual boost::exception, virtual std::exception {};
    struct FileDoesntExist : virtual CouldntOpenFile {};
    virtual void openFile(QFileInfo const& fileInfo) {
        std::array<QFileInfo, 1> range { fileInfo };
        openFiles(range);
    }

    virtual bool canOpenFile(QFileInfo const&) = 0;

    using FileInfoInputRange = boost::any_range<
        QFileInfo,
        boost::forward_traversal_tag,
        QFileInfo,
        std::ptrdiff_t
    >;
    virtual void openFiles(FileInfoInputRange const&) = 0;
    virtual ~FileOpener() {}
};

} }

#endif
