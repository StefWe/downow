#ifndef DOWNOW_UI_UNITFORMATTER
#define DOWNOW_UI_UNITFORMATTER

#include <QtCore/QObject>
#include <QtCore/QString>
#include <boost/integer.hpp>
#include <chrono>

namespace downow { namespace ui {

using Bytes = boost::uint64_t;
using Seconds = int;

extern std::vector<Bytes> const possibleSpeedValues;

QString formatBytes(Bytes const&);
QString formatDuration(std::chrono::seconds const&);

} }

#endif
