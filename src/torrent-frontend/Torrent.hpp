#ifndef DOWNOW_TORRENT_FRONTEND_TORRENT
#define DOWNOW_TORRENT_FRONTEND_TORRENT

#include "Shareable.hpp"
#include <boost/range/any_range.hpp>
#include <qt5/QtQml/QQmlListProperty>

namespace downow { namespace torrent_frontend {

struct FileEntry;
struct Session;

struct Torrent : Shareable {
private:
    Q_OBJECT
    Q_PROPERTY(int downloadLimitBPS READ getDownloadLimitBPS WRITE setDownloadLimitBPS NOTIFY downloadLimitBPSChanged)
    Q_PROPERTY(int uploadLimitBPS READ getUploadLimitBPS WRITE setUploadLimitBPS NOTIFY uploadLimitBPSChanged)
    Q_PROPERTY(Priority priority READ getPriority WRITE setPriority NOTIFY priorityChanged)
    Q_PROPERTY(QQmlListProperty<downow::torrent_frontend::FileEntry> fileEntries READ getFileEntriesQml NOTIFY fileEntriesChanged)
    Q_PROPERTY(downow::torrent_frontend::Session * session READ getSession)

    Q_ENUMS(Priority)
    Q_ENUMS(State)

public:
    using base_type = Shareable;
    enum class Priority {
        Low,
        Normal,
        High,
        Count,
        Invalid=-1
    };

    Torrent(Session *, QObject * parent = nullptr);

    virtual int getDownloadLimitBPS() const = 0;
    virtual void setDownloadLimitBPS(int downloadLimit) = 0;

    virtual int getUploadLimitBPS() const = 0;
    virtual void setUploadLimitBPS(int uploadLimit) = 0;

    virtual Priority getPriority() const = 0;
    virtual void setPriority(Priority priorty) = 0;

    using FileEntriesRandomAccessRange = boost::any_range<
        FileEntry *,
        boost::random_access_traversal_tag,
        FileEntry *,
        std::ptrdiff_t
    >;

    virtual FileEntriesRandomAccessRange getFileEntries() const = 0;

    QQmlListProperty<FileEntry> getFileEntriesQml();

    Q_INVOKABLE virtual Session * getSession() const = 0;

    Q_INVOKABLE virtual void resume() = 0;
    Q_INVOKABLE virtual void pause() = 0;

    Q_INVOKABLE void remove();
    Q_INVOKABLE void removeAndDeleteFiles();

Q_SIGNALS:
    void started(Torrent * torrent);
    void stopped(Torrent * torrent);
    void downloadLimitBPSChanged();
    void uploadLimitBPSChanged();
    void priorityChanged();
    void fileEntriesWillChange();
    void fileEntriesChanged();
    void removed();
};

std::ostream & operator<< (std::ostream & out, Torrent::State state);

} }

Q_DECLARE_METATYPE(downow::torrent_frontend::Torrent *)

#endif
