
#include "Torrent.hpp"
#include "Session.hpp"

#include <qt5/QtCore/QTimer>

#include <array>
#include <boost/log/trivial.hpp>
#include "../common/Utils.hpp"

namespace downow { namespace torrent_frontend {

Torrent::Torrent(Session * session, QObject * parent) :
    base_type(parent) {
    connect(session, &Session::torrentRemoved, this, [this] (Torrent * torrent) {
        if (this == torrent) {
            Q_EMIT removed();
        }
    });
    connect(this, &Torrent::stateChanged, [this] {
        BOOST_LOG_TRIVIAL(debug) << "State changed to " << getState() << " for torrent " << getName();
    });
}

static int countFunction(QQmlListProperty<FileEntry> * list) {
    return reinterpret_cast<Torrent *>(list->data)->getFileEntries().size();
}

static FileEntry * atFunction(QQmlListProperty<FileEntry> * list, int index) {
    return reinterpret_cast<Torrent *>(list->data)->getFileEntries()[index];
}

QQmlListProperty<FileEntry> Torrent::getFileEntriesQml() {
    return {
        /*object*/ this,
        /*data*/ this,
        countFunction,
        atFunction
    };
}

void Torrent::remove() {
    getSession()->removeTorrent(this);
}

void Torrent::removeAndDeleteFiles() {
    getSession()->removeTorrentAndDeleteFiles(this);
}

std::ostream & operator<< (std::ostream & out, Torrent::State state) {
    static std::array<char const*, (int)Torrent::State::Count> strs {{
        "Preparing",
        "Downloading",
        "Paused",
        "Seeding"
    }};

    if ((std::size_t)state < strs.size()) {
        out << strs[(int)state];
    }

    return out;
}

} }
