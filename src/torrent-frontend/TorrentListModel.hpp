#ifndef DOWNOW_TORRENT_FRONTEND_TORRENT_LIST_MODEL
#define DOWNOW_TORRENT_FRONTEND_TORRENT_LIST_MODEL

#include <QObject>
#include <QAbstractListModel>
#include "Torrent.hpp"

#include <vector>

namespace downow { namespace torrent_frontend {

struct Session;

struct TorrentListModel : QAbstractListModel {
private:
    Q_OBJECT
    Q_PROPERTY(downow::torrent_frontend::Session * session READ getSession WRITE setSession NOTIFY sessionChanged)

    Q_ENUMS(SortBy)
    Q_ENUMS(Filter)
    Q_PROPERTY(SortBy sortBy READ getSortBy WRITE setSortBy NOTIFY sortByChanged)
    Q_PROPERTY(int count READ getCount NOTIFY countChanged)
    Q_PROPERTY(Filter filter READ getFilter WRITE setFilter NOTIFY filterChanged)
public:
    enum class SortBy {
        Name, State, DownSpeed, AddedDate, Count
    };

    enum class Filter {
        All,
        OnlyDownloading,
        OnlyCompleted
    };

    explicit TorrentListModel(QObject * parent = nullptr);

    enum class Role {
        Torrent
    };

    Session * getSession() {
        return _pSession;
    }
    Session const* getSession() const {
        return _pSession;
    }

    SortBy getSortBy() const {
        return _sortBy;
    }
    void setSortBy(SortBy sortBy);

    int getCount() const {
        return _torrents.size();
    }

    Filter getFilter() const {
        return _filter;
    }

    void setFilter(Filter);

    void setSession(Session *);
    int rowCount(QModelIndex const& parent) const override;
    QVariant data(QModelIndex const& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void sessionChanged(Session *);
    void sortByChanged();
    void countChanged();
    void filterChanged();

private:
    void update();
    void updateBySort();
    void updateByFilter();
    std::function<bool(Torrent *, Torrent *)> getCompareFunction() const;

private Q_SLOTS:
    void onTorrentCompleted();

private:
    Session * _pSession = nullptr;

    std::vector<Torrent *> _torrents;
    SortBy _sortBy;
    Filter _filter;
};

} }

#endif
