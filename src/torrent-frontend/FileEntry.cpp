#include "FileEntry.hpp"
#include "Torrent.hpp"

#include <boost/log/trivial.hpp>
#include <qt5/QtCore/QMimeDatabase>

#include "../common/Utils.hpp"

namespace downow { namespace torrent_frontend {

FileEntry::FileEntry(Torrent * torrent, QObject * parent) :
    base_type(parent) {
    connect(this, &FileEntry::stateChanged, [this] {
        if (isCompleted()) {
            isCompletedChanged();
        }
    });
    connect(this, &FileEntry::savePathChanged, this, &FileEntry::nameChanged);
    connect(this, &FileEntry::savePathChanged, this, &FileEntry::savePathUrlChanged);
    connect(torrent, &Torrent::stateChanged, this, &FileEntry::updateState);
    connect(this, &FileEntry::downloadedBytesChanged, this, &FileEntry::updateState);
}

QDateTime FileEntry::getDateAdded() const {
    return getTorrent()->getDateAdded();
}

void FileEntry::updateState() {
    State newState = [this]() -> State {
        // if the torrent is downloading, the file entry may be
        // finished or still downloading so we need to check
        State torrentState = getTorrent()->getState();
        if (torrentState != State::Downloading) {
            return torrentState;
        }
        else {
            return getDownloadedBytes() == getTotalBytes()
                    ? State::Seeding
                    : State::Downloading;
        }
    }();

    if (newState != _lastState) {
        _lastState = newState;
        Q_EMIT stateChanged(_lastState, newState);
    }
}

Shareable::State FileEntry::getState() const {
    return _lastState;
}

FileType FileEntry::getFileType() const {
    return FileType(getMimeType());
}

QMimeType FileEntry::getMimeType() const {
    QMimeDatabase database;
    return database.mimeTypeForFile(getFileInfo());
}

} }
