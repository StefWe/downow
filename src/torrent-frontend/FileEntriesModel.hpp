#ifndef DOWNOW_TORRENT_FRONTEND_FILE_ENTRIES_MODEL
#define DOWNOW_TORRENT_FRONTEND_FILE_ENTRIES_MODEL

#include <qt5/QtCore/QAbstractListModel>
#include <qt5/QtCore/QPointer>

namespace downow { namespace torrent_frontend {

struct Torrent;
struct FileEntry;

struct FileEntriesModel : QAbstractListModel {
private:
    Q_OBJECT
    Q_PROPERTY(downow::torrent_frontend::Torrent * torrent READ getTorrent WRITE setTorrent NOTIFY torrentChanged)

public:
    explicit FileEntriesModel(QObject * parent = nullptr);

    enum class Role {
        FileEntryRole
    };

    int
    rowCount(const QModelIndex &parent) const override;

    QVariant
    data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray>
    roleNames() const override;

    Torrent * getTorrent() const {
        return _torrent.data();
    }

    void setTorrent(Torrent *);
    Q_SIGNAL void torrentChanged();

private:
    Q_SLOT void onFileEntriesWillChange();
    Q_SLOT void onFileEntriesChanged();

private:
    QPointer<Torrent> _torrent;
};

} }

#endif
