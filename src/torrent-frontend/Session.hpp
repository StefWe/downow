
#ifndef DOWNOW_TORRENT_FRONTEND_SESSION
#define DOWNOW_TORRENT_FRONTEND_SESSION

#include <qt5/QtCore/QObject>
#include <qt5/QtCore/QDir>
#include <qt5/QtCore/QVector>
#include <qt5/QtCore/QUrl>

#include <memory>
#include <functional>
#include <boost/range/any_range.hpp>

#include <qt5/QtCore/QMimeType>

namespace downow { namespace torrent_frontend {

struct Torrent;
struct FileEntry;

struct Session : QObject {
private:
    Q_OBJECT

    friend struct Torrent;
    Q_PROPERTY(int maxDownloadSpeedBPS READ getMaxDownloadSpeedBPS WRITE setMaxDownloadSpeedBPS NOTIFY maxDownloadSpeedBPSChanged)
    Q_PROPERTY(int maxUploadSpeedBPS READ getMaxUploadSpeedBPS WRITE setMaxUploadSpeedBPS NOTIFY maxUploadSpeedBPSChanged)
    Q_PROPERTY(int downloadSpeedBPS READ getDownloadSpeedBPS NOTIFY downloadSpeedBPSChanged)
    Q_PROPERTY(int uploadSpeedBPS READ getUploadSpeedBPS NOTIFY uploadSpeedBPSChanged)
    Q_PROPERTY(bool isPaused READ isPaused NOTIFY isPausedChanged)
    Q_PROPERTY(int torrentsCount READ getTorrentsCount NOTIFY torrentsCountChanged)

public:
    using base_type = QObject;
    Session(QObject * parent = nullptr);
    virtual ~Session() {}

    Q_INVOKABLE virtual void addTorrentByMagnetLink(QUrl magnetLink) = 0;
    Q_INVOKABLE virtual void addTorrentByFile(QString torrentFilePath) = 0;

    Q_INVOKABLE virtual void removeTorrent(Torrent *) = 0;
    Q_INVOKABLE virtual void removeTorrentAndDeleteFiles(Torrent *) = 0;

    Q_INVOKABLE virtual QDir getTorrentsSaveDirectory() const = 0;

    virtual int getDownloadSpeedBPS() const = 0;
    virtual int getUploadSpeedBPS() const = 0;

    Q_INVOKABLE virtual void pause() = 0;
    Q_INVOKABLE virtual void resume() = 0;

    virtual bool isPaused() const = 0;

    virtual int getMaxDownloadSpeedBPS() const = 0;
    virtual int getMaxUploadSpeedBPS() const = 0;

    virtual void setMaxDownloadSpeedBPS(int) = 0;
    virtual void setMaxUploadSpeedBPS(int) = 0;

    Torrent * findTorrentByPredicate(std::function<bool(Torrent *)> pred);

    int getTorrentsCount() const;

    /**
     * Called when the app wants to exit.
     * The session can use this function to do some cleanup before
     * emitting the signal "allowedToExit".
     */
    Q_SLOT virtual void prepareToExit() {
        allowedToExit();
    }

    using TorrentRandomRange = boost::any_range<
        Torrent *,
        boost::random_access_traversal_tag,
        Torrent *,
        std::ptrdiff_t
    >;

    virtual
    TorrentRandomRange
    getTorrents() const = 0;

Q_SIGNALS:
    void allowedToExit();

    void willAddTorrent(Torrent * torrent);
    void torrentAdded(Torrent * torrent);

    void willRemoveTorrent(Torrent * torrent);
    void torrentRemoved(Torrent * torrent);

    void willChangeTorrents();
    void torrentsChanged();

    void torrentMetadataReceived(Torrent * torrent);

    void upDownRatioChanged(float upDownRatio);
    void uploadedBytesChanged(int64_t uploadedBytes);
    void downloadedBytesChanged(int64_t downloadedBytes);
    void secondsActiveChanged(int64_t secondsActive);

    void torrentCompleted(Torrent *);

    void duplicateTorrentError(QString name);
    void maxDownloadSpeedBPSChanged();
    void maxUploadSpeedBPSChanged();
    void isPausedChanged();
    void downloadSpeedBPSChanged();
    void uploadSpeedBPSChanged();

    void torrentsCountChanged();

private:
    Q_SLOT void onTorrentStateChanged(Torrent *);
};

} }

#endif
