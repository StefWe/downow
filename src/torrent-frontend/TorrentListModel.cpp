#include "TorrentListModel.hpp"
#include "Session.hpp"
#include "Torrent.hpp"

#include <boost/range/adaptor/filtered.hpp>

#include <unordered_map>

namespace downow { namespace torrent_frontend {

TorrentListModel::TorrentListModel(QObject * parent) :
    QAbstractListModel(parent),
    _sortBy(SortBy::AddedDate),
    _filter(Filter::All) {}

void
TorrentListModel::setSortBy(TorrentListModel::SortBy sortBy) {
    if (_sortBy == sortBy) {
        return;
    }

    _sortBy = sortBy;
    beginResetModel();
    updateBySort();
    endResetModel();

    Q_EMIT sortByChanged();
}

void TorrentListModel::setFilter(TorrentListModel::Filter filter) {
    if (getFilter() == filter) {
        return;
    }

    _filter = filter;
    update();

    Q_EMIT filterChanged();
}

void
TorrentListModel::setSession(Session * pSession) {
    if (_pSession == pSession) {
        return;
    }

    if (_pSession) {
        _pSession->disconnect(this);
    }

    _pSession = pSession;
    connect(_pSession, &Session::torrentsChanged, this, &TorrentListModel::update);
    connect(_pSession, &Session::torrentCompleted, this, &TorrentListModel::onTorrentCompleted);
    Q_EMIT sessionChanged(_pSession);

    update();
}

int
TorrentListModel::rowCount(QModelIndex const& parent) const {
    return getCount();
}

QVariant
TorrentListModel::data(QModelIndex const& index, int roleIndex) const {
    Torrent & torrent = *_torrents[index.row()];

    Role role = (Role)roleIndex;
    switch (role) {
    case Role::Torrent:
        return QVariant::fromValue(&torrent);
    default: return QVariant {};
    }
}

QHash<int, QByteArray>
TorrentListModel::roleNames() const {
    return {
        {(int)Role::Torrent, "torrent"}
    };
}

void TorrentListModel::update() {
    beginResetModel();
    updateByFilter();
    updateBySort();
    endResetModel();

    Q_EMIT countChanged();
}

void TorrentListModel::updateBySort() {
#if 1
    std::sort(_torrents.begin(), _torrents.end(), getCompareFunction());
#else
    // sort the old torrents
    auto sorted = [&]() -> std::vector<Torrent *> {
        std::vector<Torrent *> res = _torrents;
        std::sort(res.begin(), res.end(), getCompareFunction());
        return res;
    }();

    // create map between a torrent and its old index
    auto torrentToOldIndex = [&]() -> std::unordered_map<Torrent *, std::size_t> {
        std::unordered_map<Torrent *, std::size_t> res;
        for (std::size_t i = 0; i < _torrents.size(); i++) {
            res[_torrents[i]] = i;
        }
        return res;
    }();

    // create map between an old index and a new index
    auto oldIndexToNewIndex = [&]() -> std::unordered_map<std::size_t, std::size_t> {
        std::unordered_map<std::size_t, std::size_t> res;
        for (std::size_t i = 0; i < sorted.size(); i++) {
            std::size_t oldIndex = torrentToOldIndex[sorted[i]];
            res[oldIndex] = i;
        }
        return res;
    }();
#endif
}

void TorrentListModel::updateByFilter() {
    if (_filter == Filter::All) {
        _torrents = std::vector<Torrent *> {
            _pSession->getTorrents().begin(),
            _pSession->getTorrents().end()
        };
    }
    else {
        auto filteredRange = _pSession->getTorrents() | boost::adaptors::filtered([this] (Torrent * torrent) {
            if (_filter == Filter::OnlyCompleted) {
                return torrent->getState() == Torrent::State::Seeding;
            }
            else {
                return torrent->getState() != Torrent::State::Seeding;
            }
        });

        _torrents = std::vector<Torrent *> {
            filteredRange.begin(),
            filteredRange.end()
        };
    }
}

std::function<bool (Torrent *, Torrent *)> TorrentListModel::getCompareFunction() const {
    switch (getSortBy()) {
    case SortBy::Name:
        return [] (Torrent * t1, Torrent * t2) {
            return t1->getName() < t2->getName();
        };
    case SortBy::AddedDate:
        return [] (Torrent * t1, Torrent * t2) {
            return t1->getDateAdded() > t2->getDateAdded();
        };
    case SortBy::DownSpeed:
        return [] (Torrent * t1, Torrent * t2) {
            return t1->getDownloadSpeedBPS() > t2->getDownloadSpeedBPS();
        };
    default:
        return [] (Torrent * t1, Torrent * t2) {
            return true;
        };
    }
}

void TorrentListModel::onTorrentCompleted() {
    // if a torrent has completed and we are showing only downloading or complete torrents,
    // update torrents list
    if (getFilter() != Filter::All) {
        update();
    }
}

} }
