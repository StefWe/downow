#include "DownowPlugin.hpp"

#include "Session.hpp"
#include "Torrent.hpp"
#include "TorrentListModel.hpp"

#include <QtQml/QtQml>

namespace downow {

void
DownowPlugin::registerTypes(const char *uri) {
    qmlRegisterType<Session>(uri, 1, 0, "Session");
    qmlRegisterUncreatableType<Torrent>(uri, 1, 0, "Torrent", "Use TorrentListModel to get a hold of instances of this class");
    qmlRegisterType<TorrentListModel>(uri, 1, 0, "TorrentListModel");
}

}
