#ifndef DOWNOW_MODEL_QML_PLUGIN
#define DOWNOW_MODEL_QML_PLUGIN

#include <QtQml/QQmlExtensionPlugin>

namespace downow {

struct DownowPlugin : QQmlExtensionPlugin {
private:
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri) override;
};

}

#endif
