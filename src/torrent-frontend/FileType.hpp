#ifndef DOWNOW_TORRENT_FRONTEND_FILE_TYPE
#define DOWNOW_TORRENT_FRONTEND_FILE_TYPE

#include <QtCore/QString>
#include <QtCore/QFileInfo>
#include <QtCore/QMimeType>

#include <boost/exception/all.hpp>

namespace downow { namespace torrent_frontend {

struct FileType {
    enum TypeEnum {
        Documents,
        Pictures,
        Music,
        Contacts,
        Videos,
        Links,
        Count
    };

    struct CouldntFindFileTypeForMimeType : virtual boost::exception, virtual std::exception {};

    FileType(TypeEnum type);
    FileType(QFileInfo const&);
    FileType(QMimeType const&);
    QString id() const;
    TypeEnum enumValue() const {
        return _enum;
    }
    static constexpr int count() {
        return Count;
    }

private:
    TypeEnum _enum;
};

} }

#endif
