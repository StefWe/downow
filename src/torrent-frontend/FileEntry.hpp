#ifndef DOWNOW_TORRENT_FRONTEND_FILE_ENTRY
#define DOWNOW_TORRENT_FRONTEND_FILE_ENTRY

#include <qt5/QtCore/QObject>
#include <qt5/QtCore/QString>
#include <qt5/QtCore/QPointer>
#include <qt5/QtCore/QFileInfo>
#include <qt5/QtCore/QUrl>
#include <qt5/QtCore/QMimeType>

#include <boost/log/trivial.hpp>

#include "../common/Utils.hpp"

#include "Shareable.hpp"
#include "FileType.hpp"

namespace downow { namespace torrent_frontend {

struct Torrent;

struct FileEntry : Shareable {
private:
    Q_OBJECT
    Q_PROPERTY(QUrl savePathUrl READ getSavePathUrl NOTIFY savePathUrlChanged)
    Q_PROPERTY(bool isCompleted READ isCompleted NOTIFY isCompletedChanged)
    Q_PROPERTY(downow::torrent_frontend::Torrent * torrent READ getTorrent NOTIFY torrentChanged)
    Q_PROPERTY(QString typeId READ getTypeId NOTIFY typeIdChanged)

public:
    using base_type = Shareable;

    explicit FileEntry(Torrent *, QObject * parent = nullptr);

    virtual Torrent * getTorrent() const = 0;
    virtual QFileInfo getFileInfo() const = 0;

    QString getName() const override {
        return getFileInfo().fileName();
    }

    QString getSavePath() const override {
        return getFileInfo().absoluteFilePath();
    }

    State getState() const override;

    QUrl getSavePathUrl() const {
        return QUrl::fromLocalFile(getSavePath());
    }

    bool isCompleted() const {
        return getState() == State::Seeding;
    }

    QString getTypeId() const {
        try {
            return getFileType().id();
        }
        catch (FileType::CouldntFindFileTypeForMimeType const&) {
            return QString {};
        }
    }

    FileType getFileType() const;
    QMimeType getMimeType() const;

    QDateTime getDateAdded() const override;

    Q_SIGNAL void savePathUrlChanged();
    Q_SIGNAL void isCompletedChanged();
    Q_SIGNAL void torrentChanged();
    Q_SIGNAL void typeIdChanged();

private:
    Q_SLOT void updateState();
    State _lastState = State::Invalid;
};

} }

Q_DECLARE_METATYPE(downow::torrent_frontend::FileEntry *)

#endif
