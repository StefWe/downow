#include "FileType.hpp"

#include <QtCore/QMimeDatabase>
#include <unordered_map>
#include <boost/log/trivial.hpp>
#include "../common/Utils.hpp"

namespace downow { namespace torrent_frontend {

static QMimeType mimeTypeForFileInfo(QFileInfo const& fileInfo) {
    QMimeDatabase database;
    return database.mimeTypeForFile(fileInfo);
}

static FileType fileTypeForMimeType(QMimeType const& mimeType) {
    QString name = mimeType.name();
    QStringList components = name.split("/");
    if (components.size() == 0) {
        BOOST_THROW_EXCEPTION(FileType::CouldntFindFileTypeForMimeType());
    }

    QString type = components[0];

    static std::unordered_map<std::string, FileType> map {
        {"video", FileType::Videos},
        {"audio", FileType::Music},
        {"image", FileType::Pictures},
        {"text", FileType::Documents}
    };

    auto iter = map.find(type.toStdString());
    if (iter == map.end()) {
        BOOST_LOG_TRIVIAL(warning) << "Couldn't find file type for mime type " << mimeType.name();
        BOOST_THROW_EXCEPTION(FileType::CouldntFindFileTypeForMimeType());
    }

    return iter->second;
}

FileType::FileType(FileType::TypeEnum type) : _enum(type) {}

FileType::FileType(const QFileInfo & fileInfo) :
    FileType(mimeTypeForFileInfo(fileInfo)) {}

FileType::FileType(const QMimeType & mimeType) {
    _enum = fileTypeForMimeType(mimeType)._enum;
}

QString FileType::id() const {
    static QString strs[Count] = {
        "Document", "Picture", "Music", "Contact", "Video", "Link"
    };
    return strs[(int)_enum];
}

} }
