#include "Session.hpp"
#include "Torrent.hpp"

#include <qt5/QtCore/QDebug>

#include "FileEntry.hpp"
#include "../common/Utils.hpp"

namespace downow { namespace torrent_frontend {

Session::Session(QObject * parent) :
    base_type(parent) {
    connect(this, &Session::willAddTorrent, this, &Session::willChangeTorrents);
    connect(this, &Session::willRemoveTorrent, this, &Session::willChangeTorrents);
    connect(this, &Session::torrentAdded, this, &Session::torrentsChanged);
    connect(this, &Session::torrentRemoved, this, &Session::torrentsChanged);

    connect(this, &Session::torrentAdded, this, &Session::torrentsCountChanged);
    connect(this, &Session::torrentRemoved, this, &Session::torrentsCountChanged);

    connect(this, &Session::torrentAdded, [this] (Torrent * torrent) {
        connect(torrent, &Torrent::stateChanged, this, [this, torrent] (Torrent::State from, Torrent::State to) {
            onTorrentStateChanged(torrent);
        });
    });

    connect(this, &Session::torrentAdded, [this] {
        if (isPaused()) {
            resume();
        }
    });
}

Torrent *
Session::findTorrentByPredicate(std::function<bool (Torrent *)> pred) {
    auto const& torrents = getTorrents();
    auto iter = std::find_if(torrents.begin(), torrents.end(), pred);

    if (iter != torrents.end()) {
        return *iter;
    }
    else {
        return nullptr;
    }
}

int Session::getTorrentsCount() const {
    return getTorrents().size();
}

void Session::onTorrentStateChanged(Torrent * torrent) {
    if (torrent->getState() == Torrent::State::Seeding) {
        Q_EMIT torrentCompleted(torrent);
    }
}

} }
