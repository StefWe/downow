#include "Shareable.hpp"

#include <limits>
#include <boost/integer_traits.hpp>
#include <boost/log/trivial.hpp>

namespace downow { namespace torrent_frontend {

Shareable::Shareable(QObject * parent) :
    base_type(parent) {
    // update time left property whenever any of these properties change
    connect(this, &Shareable::totalBytesChanged, this, &Shareable::updateTimeLeftSec);
    connect(this, &Shareable::downloadedBytesChanged, this, &Shareable::updateTimeLeftSec);
    connect(this, &Shareable::downloadSpeedBPSChanged, this, &Shareable::updateTimeLeftSec);

    connect(this, &Shareable::uploadedBytesChanged, this, &Shareable::updateRatio);
    connect(this, &Shareable::downloadedBytesChanged, this, &Shareable::updateRatio);

    // update progress
    connect(this, &Shareable::totalBytesChanged, this, &Shareable::progressChanged);
    connect(this, &Shareable::downloadedBytesChanged, this, &Shareable::progressChanged);

    connect(this, &Shareable::downloadedBytesChanged, this, &Shareable::downloadedKBChanged);
    connect(this, &Shareable::uploadedBytesChanged, this, &Shareable::uploadedKBChanged);
    connect(this, &Shareable::totalBytesChanged, this, &Shareable::totalKBChanged);
}

float Shareable::getRatio() const {
    return _lastRatio;
}

void Shareable::updateRatio() {
    float newRatio = (double)getUploadedBytes() / (double)getDownloadedBytes();
    if (newRatio != _lastRatio) {
        _lastRatio = newRatio;
        Q_EMIT ratioChanged();
    }
}

int
Shareable::getTimeLeftSec() const {
    return _lastTimeLeftSec;
}

float Shareable::getProgress() const {
    if (getTotalBytes() == 0) {
        return 0;
    }

    using float_t = float;
    static_assert(std::numeric_limits<float_t>::max() >= std::numeric_limits<Bytes>::max(), "Float type not big enough");

    return (float_t)getDownloadedBytes() / (float_t)getTotalBytes();
}

void
Shareable::updateTimeLeftSec() {
    int newTimeLeftSec;

    if (getDownloadSpeedBPS() != 0) {
        newTimeLeftSec = (getTotalBytes() - getDownloadedBytes()) / getDownloadSpeedBPS();
    }
    else {
        newTimeLeftSec = -1;
    }

    if (_lastTimeLeftSec != newTimeLeftSec) {
        _lastTimeLeftSec = newTimeLeftSec;
        timeLeftSecChanged();
    }
}

} }
