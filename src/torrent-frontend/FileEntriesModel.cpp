#include "FileEntriesModel.hpp"

#include "Torrent.hpp"
#include "FileEntry.hpp"

namespace downow { namespace torrent_frontend {

FileEntriesModel::FileEntriesModel(QObject * parent) :
    QAbstractListModel(parent) {}

int FileEntriesModel::rowCount(const QModelIndex &parent) const {
    if (!_torrent) {
        return 0;
    }

    return _torrent->getFileEntries().size();
}

QVariant FileEntriesModel::data(QModelIndex const& index, int role) const {
    switch ((Role)role) {
    case Role::FileEntryRole:
        return QVariant::fromValue(_torrent->getFileEntries()[index.row()]);

    default:
        return QVariant {};
    }
}

QHash<int, QByteArray> FileEntriesModel::roleNames() const {
    return {
        {(int)Role::FileEntryRole, "fileEntry"}
    };
}

void FileEntriesModel::setTorrent(Torrent * torrent) {
    if (_torrent.data() == torrent) {
        return;
    }

    connect(torrent, &Torrent::fileEntriesWillChange, this, &FileEntriesModel::onFileEntriesWillChange);
    connect(torrent, &Torrent::fileEntriesChanged, this, &FileEntriesModel::onFileEntriesChanged);

    onFileEntriesWillChange();
    _torrent = torrent;
    onFileEntriesChanged();

    Q_EMIT torrentChanged();
}

void FileEntriesModel::onFileEntriesWillChange() {
    beginResetModel();
}

void FileEntriesModel::onFileEntriesChanged() {
    endResetModel();
}

} }
