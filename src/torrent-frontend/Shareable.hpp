#ifndef DOWNOW_TORRENT_FRONTEND_SHAREABLE
#define DOWNOW_TORRENT_FRONTEND_SHAREABLE

#include <qt5/QtCore/QObject>
#include <qt5/QtCore/QString>
#include <qt5/QtCore/QDateTime>

#include <boost/integer.hpp>

#include <cassert>

namespace downow { namespace torrent_frontend {

struct Shareable : QObject {
private:
    Q_OBJECT
    Q_PROPERTY(QString name READ getName NOTIFY nameChanged)

    Q_PROPERTY(unsigned int downloadedKB READ getDownloadedKB NOTIFY downloadedKBChanged)
    Q_PROPERTY(unsigned int uploadedKB READ getUploadedKB NOTIFY uploadedKBChanged)
    Q_PROPERTY(unsigned int totalKB READ getTotalKB NOTIFY totalKBChanged)
    Q_PROPERTY(float ratio READ getRatio NOTIFY ratioChanged)

    Q_PROPERTY(int timeLeftSec READ getTimeLeftSec NOTIFY timeLeftSecChanged)
    Q_PROPERTY(unsigned int downloadSpeedBPS READ getDownloadSpeedBPS NOTIFY downloadSpeedBPSChanged)
    Q_PROPERTY(unsigned int uploadSpeedBPS READ getUploadSpeedBPS NOTIFY uploadSpeedBPSChanged)
    Q_PROPERTY(QString savePath READ getSavePath NOTIFY savePathChanged)
    Q_PROPERTY(QDateTime dateAdded READ getDateAdded NOTIFY dateAddedChanged)
    Q_PROPERTY(float progress READ getProgress NOTIFY progressChanged)
    Q_PROPERTY(State state READ getState NOTIFY stateChanged)

    Q_ENUMS(State)

public:
    using Bytes = boost::uint64_t;
    enum class State {
        Preparing,
        Downloading,
        Paused,
        Seeding,
        Count,
        Invalid=-1
    };

    using base_type = QObject;
    Shareable(QObject * parent = nullptr);
    virtual ~Shareable() {}

    unsigned int getDownloadedKB() const {
        return getDownloadedBytes() / 1024;
    }

    unsigned int getUploadedKB() const {
        return getUploadedBytes() / 1024;
    }

    unsigned int getTotalKB() const {
        return getTotalBytes() / 1024;
    }

    float getRatio() const;

    virtual QString getName() const = 0;

    virtual Bytes getDownloadedBytes() const = 0;
    virtual Bytes getUploadedBytes() const = 0;
    virtual Bytes getTotalBytes() const = 0;

    virtual QString getSavePath() const = 0;
    virtual QDateTime getDateAdded() const = 0;
    virtual int getDownloadSpeedBPS() const = 0;
    virtual int getUploadSpeedBPS() const = 0;
    virtual State getState() const = 0;

    int getTimeLeftSec() const;

    float getProgress() const;

Q_SIGNALS:
    void nameChanged();
    void downloadSpeedBPSChanged();
    void uploadSpeedBPSChanged();
    void downloadedBytesChanged();
    void ratioChanged();
    void uploadedBytesChanged();
    void totalBytesChanged();
    void timeLeftSecChanged();
    void savePathChanged();
    void dateAddedChanged();
    void progressChanged();
    void stateChanged(State from, State to);

    void downloadedKBChanged();
    void uploadedKBChanged();
    void totalKBChanged();

private:
    Q_SLOT void updateRatio();

    int _lastTimeLeftSec = -1;
    float _lastRatio = -1;
    Q_SLOT void updateTimeLeftSec();
};

} }

#endif
